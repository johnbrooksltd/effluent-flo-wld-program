1, Add 2 compiling macro test test_conf
test: if it is set to 1, every Modbus Message sent to vsd will be shown on LCD
test_conf: if it is set to 1, the set up procedure will go through without pressing button

2, Add variable switch_pressure1 which is the boundary for feedback control swithcing between flowrate and pressure

3, Change the setting of vsd, enable its PID function, change S4 of vsd to PID disable switch function

4, Add 2 tim, tim(15) is used for the switching timer

5, During initializing relay 3 output of wld is on to disable PID control of vsd, so the pressure and current measuring can be carried on

6, when the pressure is higher than the switch_pressure and it lasts for a certain period, it turns on the relay 3 of wld linked to S4 of vsd, then the system will go to pressure PID control mode
