
'===============================================================================
'   __________________________________________________________________________
'  |                                                                          |
'  |                      EffluentFlo based on WaterFlo                                    |
'  |                       Written By Rajan Reddy, Amended by John Yuan                            |
'  |                           December 2020                                  |
'  |                        www.johnbrooks.co.nz                              |
'  |__________________________________________________________________________|
'
'===============================================================================

'V1.0.0

Config Watchdog = 2048
Const test = 0
Const test_conf = 0
Const test_disp = 0
Const FM_Rundry = 1
Const PT_PressureHigh = 1
Const FM_BurstPipe = 1

'Fuse bit settings for chip
$prog &HFF  , &HD9 , &HFC       ', &HDF

'This board is based on the Atmel ATmega1284p Chip         +++++++
$regfile = "m1284pdef.dat"

$baud = 9600                                             '9600

'16 Mhz clock rate
$crystal = 16000000
'use 256 for the hardware stack
$hwstack = 255
'use 256 for the SW stack
$swstack = 255
 'use 256 for the frame space
$framesize = 255

'Instruction to load the graphics driver.
'$lib "KCL2_KS108.lib" this is old and tweaked do not use
$lib "glcdks108.lib"  ' this is as supplied by bascom and works

'ascii file which contains the font definitions
$include "font8x8.font"

'Alias Definitions
Statusled Alias Portd.3
Buzzer Alias Portd.4
Jp1_24 Alias Pind.7
Jp1_13 Alias Pind.6
Rly1 Alias Porta.1
Rly2 Alias Porta.0
Rly3 Alias Portb.0
Rly4 Alias Portb.1
Rly5 Alias Portb.2
Rly6 Alias Portb.3
Anaoutl Alias Ocr1al
Anaouth Alias Ocr1ah
Rd165 Alias Portb.4
Sck Alias Portb.7
Miso Alias Pinb.6
Mosi Alias Portb.5

'Constant Definitions
Const Nbrtimers = 17
' Approx number of ticks for 1 sec
Const Nbrticks = 61
Const Channel = 2
Const Beeptime = 6

'===============================================================================
' FUNCTION AND SUBROUTINE DECLARATIONS
'===============================================================================
Declare Sub Initialise()
Declare Sub Poweronlogo()
Declare Sub Read165()
Declare Sub Fault()
Declare Sub Sub_0()                                         ' Main screen display
Declare Sub Sub_01()                                        ' Check if new drive/controller
Declare Sub Sub_02()                                        ' Select function display
Declare Sub Sub_03()                                        ' Modify function display
Declare Sub Sub_04()                                        ' Fault history display
Declare Sub Sub_06()                                        ' Setup new drive
Declare Sub Calc_chksm()
Declare Sub Write_faults()
Declare Sub Clear_buff()
Declare Sub Receive_data()
Declare Sub Digi_val()
Declare Sub Format_value()
Declare Sub Readadc()
Declare Sub Set_value()
Declare Sub Sub_fault_check()
Declare Sub Clear_faults()
Declare Sub mode_select()
Declare Sub Build_write_string()
Declare Sub Build_read_string()
Declare Sub display_fault_var()
Declare Sub ChangeFlowRateSP
Declare  Sub MyAssert(byval number as word)
Declare Function FnGetFlowRate() as Long
Declare Function  ChooseFM_PT(byval a as byte)  as byte                                             'byval aTempByte as Integer

'Timer 0 Overflow interrupt service routine
On Ovf0 Tim0_isr

'===============================================================================
' VARIABLE DECLARATIONS
'===============================================================================
'Allocate memory for the global variables
Dim Ticks As Byte , Beeper As Byte , Tim(17) As Word      'Use for Backgound timers
Dim Ic13 As Byte
Dim Ic14 As Byte
Dim Anaval_l As Long
Dim Anaout As Word
Dim Adcvallo As Word
Dim Adcvalhi As Word
Dim String_to_send As String * 25
Dim Comstring As String * 25
Dim State As Integer
Dim A As String * 1
Dim B As Integer
Dim C As Long
Dim D As String * 4
Dim J As Integer
Dim Actual_amps As String * 5
Dim Actual_rpm As String * 5

#IF FM_BurstPipe
Dim Actual_rpmN As long
#ENDIF

Dim Fault_01 As Byte
Dim Fault_02 As Byte
Dim Fault_03 As Byte
Dim Fault_04 As Byte
Dim Fault_05 As Byte
Dim Fault_06 As Byte
Dim Fault_07 As Byte
Dim Fault_08 As Byte
Dim Fault_09 As Byte
Dim Fault_10 As Byte
Dim Fault_num As Byte
Dim Fault_var As Byte
Dim H As String * 4
Dim D1 As String * 1
Dim D2 As String * 1
Dim D3 As String * 1
Dim D4 As String * 1
Dim Max_val As Integer
Dim Min_val As Integer
Dim Error_reg As Byte
Dim Func_des1 As String * 16
Dim Func_des2 As String * 16
Dim Func_des3 As String * 16
Dim Write_string_reg As String * 20
Dim Csum As Byte
Dim F_num As Byte
Dim Posi As Byte
Dim Configured As Byte
Dim Pb_mode As Byte
Dim Pb_up_arrow As Byte
Dim Pb_dsp_fun As Byte
Dim Pb_run_stop As Byte
Dim Pb_down_arrow As Byte
Dim Pb_reset As Byte
Dim Pb_enter As Byte
Dim Value As Integer
Dim Title_str As String * 20
Dim Password As Word
Dim Volt_out As Integer
Dim Autotune_fail As Bit
Dim Menu As Byte
Dim Framebytes(10) As Byte
Dim Char01 As String * 2
Dim Char02 As String * 2
Dim Char03 As String * 2
Dim Char04 As String * 2
Dim Char05 As String * 2
Dim Char06 As String * 2
Dim Char07 As String * 2
Dim Char08 As String * 2
Dim Char09 As String * 2
Dim Bytecount As Integer
Dim Running As Bit
Dim No_read_rpm As Byte
Dim No_read_current As Byte
Dim Accel_time As Word
Dim Decel_time As Word
Dim Minimum_speed As Word
Dim Min_speed_temp as Word
Dim Maximum_speed As Word
Dim Top_speed As Word
Dim Reset_c As Byte
Dim Temp As Word
Dim Temp_l As Long
Dim Temp_S As Single
Dim Initialised As Bit
Dim Resume_point As Byte
Dim Configured_as As Byte
Dim Motor_volts As Word
Dim Motor_current As Byte
Dim low_suction_current As byte
Dim FlowRateSetpoint As Word
Dim FlowRateSetpoint1 As Word
Dim FlowRateSetpoint2 As Word
Dim test_current as long
Dim Over_current As Word
Dim Motor_power As Word
Dim Motor_rpm As Word
Dim Motor_hz As Word
Dim Error_on As Bit                                        'set in write_faults routine, stops command to drive when error occurs
Dim Level_display As Dword
Dim Null As Byte
Dim Reset_flag As Byte
Dim Number_of_resets As Byte
Dim Passes As Byte
Dim Read_buffer_count As Byte
Dim Cls_temp As Byte
Dim Inst_dac As Integer
Dim Desired_ain As Integer
Dim Error_inst As Integer
Dim Integral As Integer
Dim Integral2 As Integer
Dim P_gain As Integer
Dim I_gain As Integer
Dim 5_i_gain As Integer
Dim 10_i_gain As Integer
Dim 20_i_gain As Integer
Dim 25_i_gain As Integer
Dim 30_i_gain As Integer
Dim Pressure1 As Dword
Dim standby_pressure1 As Dword
'Dim resume_pressure1 As Dword
'Dim switch_pressure1 As Dword
Dim Prime_time as Byte
Dim Transducer_fault as byte
Dim Paused as bit
Dim Run_Flag as bit
Dim error_clear as bit
Dim actual_pressure as word
Dim num_of_poles as string*4

Dim Prime_Loss as byte
Dim Drive as byte ' 1 = CV , 2 = E510 , 3 = E510S
Dim Z as byte
Dim Function_number As String * 4
Dim Function_value As String * 4      ' all values to be in Hex form
Dim RunDry as byte
Dim suggestions as byte
Dim Setpoint as dword
Dim Setpoint_1 as word
Dim Setpoint_2 as word
Dim standby_offset as word
Dim resume_offset as word
Dim max_hours as byte
Dim running_minutes as byte
Dim running_hours as byte
Dim time_exceeded as bit
Dim flowswitch as byte
Dim disp_op as byte
Dim disp_op1 as byte
Dim soft_stop as byte
Dim ext_interlock as byte
Dim run_inhibit as bit
Dim standby_pressure as Word
'Dim resume_pressure as Word
'Dim switch_pressure as Word
Dim standby_time as byte
Dim resume_time as byte
Dim on_standby as bit   'used for pressure too high faulty error
Dim on_switch as bit
Dim LS_pressure as word
Dim no_water_time as byte
Dim going_to_standby as bit   'used for pressure too high faulty error
Dim going_to_switch as bit
Dim resuming as bit
Dim setup_setpoint as byte
';Dim low_suction as byte
Dim real_amps as byte
Dim low_pressure as byte
Dim disp_setpoint_str as string*5
Dim pressure_display_str as string*5
Dim bottom_display_str as string*5
'Dim Reset_timer as bit
Dim operation_mode as byte
'Dim reset_attempts as byte
Dim Set_current As Word
Dim fault_bypass as byte
Dim skip as byte
Dim first_cycle as byte
Dim remote_signal as byte
Dim remote_switch as byte
Dim Local_stop_mem as byte
Dim start_stops as word
Dim total_cycles as dword
Dim cycle_count as bit
Dim index_a as byte
Dim hour(24) as word
Dim hours as dword
Dim hours_large as dword
Dim run_stop as byte
Dim User_units as byte
Dim temp_Dword as dword ' this will be used for math purposes where large numbers are crunched
Dim units_str as string*3

Dim single_phase as byte
Dim FRateSwitch as byte
Dim RundryPre as byte
Dim flowRate as Long

Dim BurstPipePre as bit
Dim BurstPipe as bit

Dim FormerRly1 as bit 'to record the rising edge of VSD starting signal
Dim CloseLoopDelay as bit 'indicate whether the controller is in close loop delay
Dim WithFM as byte 'indicate whether the controller is with a flowmeter
Dim WithPT as byte 'indicate whether the controller is with a pressure transducer
Dim CloseLoopDelayTime as byte

'Protection Params
Dim RunDryFlowRate as word'Integer
Dim BurstPipeEnable as byte
Dim BurstPipeTime as byte
Dim BurstPipeFlowRate as word'Integer
Dim BurstPipeFRlimit as word
Dim BlockedPipeEnable as byte
Dim LowPressEnable as byte
Dim LowPressTime as byte

'Sensor Range
Dim Fsensor_type as byte
Dim Psensor_type as byte
Dim sensor_size as word  'for 10 bar 1000 for units in bar 1450 for units in psi
Dim sensor_trim as word ' the ADC value when actual pressure is zero
Dim Fsensor_size as word
Dim Fsensor_bias as word


'Get variables from EEPROM
Readeeprom Configured , 0
Readeeprom Fault_01 , 1
Readeeprom Fault_02 , 2
Readeeprom Fault_03 , 3
Readeeprom Fault_04 , 4
Readeeprom Fault_05 , 5
Readeeprom Fault_06 , 6
Readeeprom Fault_07 , 7
Readeeprom Fault_08 , 8
Readeeprom Fault_09 , 9
Readeeprom Fault_10 , 10
Readeeprom Motor_rpm , 11                                   '11 & 12
Readeeprom Minimum_speed , 13                               '13 & 14
Readeeprom Accel_time , 15                                  '15 & 16
Readeeprom Decel_time , 17                                  '17 & 18
Readeeprom Motor_hz , 19                                    '19 & 20
'Readeeprom Resume_threshold , 21
Readeeprom Resume_point , 22
Readeeprom Configured_as , 23
Readeeprom Motor_current , 24
Readeeprom Maximum_speed , 25                               '25 & 26
'Readeeprom Direction , 27
Readeeprom no_water_time , 29
Readeeprom Min_speed_temp , 30                              '30 & 31
Readeeprom CloseLoopDelayTime , 32
Readeeprom low_suction_current , 33
Readeeprom resume_time , 34
'Readeeprom sensor_type , 35
Readeeprom Prime_time , 36
Readeeprom Prime_Loss , 37
Readeeprom Drive , 38
Readeeprom Setpoint_1 , 39
Readeeprom PSensor_type, 41 'Setpoint_2 , 41
Readeeprom FSensor_type, 42
Readeeprom standby_offset , 43
Readeeprom resume_offset , 45
Readeeprom Max_hours , 47
Readeeprom operation_mode , 48
Readeeprom local_stop_mem , 49
Readeeprom LS_pressure , 53
Readeeprom disp_op , 55
Readeeprom user_units , 56
Readeeprom FlowRateSetpoint, 57        '57&58
Readeeprom FlowRateSetpoint1, 59        '59&60
Readeeprom FlowRateSetpoint2, 61        '61&62
Readeeprom WithFM, 63                          'to see the connection of sensors set by user
Readeeprom standby_time, 64
Readeeprom RunDryFlowRate, 65        '65&66
Readeeprom BurstPipeEnable, 67
Readeeprom BurstPipeTime, 68
Readeeprom BurstPipeFlowRate, 69        '69&70
Readeeprom BlockedPipeEnable, 71
Readeeprom LowPressEnable, 72
Readeeprom LowPressTime, 73



If WithFM = 1 Then
    WithPT = 0
Elseif   WithFM = 2 Then
   WithPT = 1
   WithFM = 0
Else
   WithPT = 1
   WithFM = 1
Endif


'backwards compatibility arrays
Dim parameter_ar(62) as string*4      'array to hold register addresses to be used in the program. it will be a copy of of one of the above arrays based on which drive is used with the WLD
Dim parameter_ar_E510(62) as string*4    'array to hold register addresses for E510 parameters
Dim parameter_ar_A510s(62) as string*4    'array to hold register addresses for A510s parameters

'E510 array
parameter_ar_E510(1) = "2524"    ' output hz
parameter_ar_E510(2) = "2527"    ' output amps
parameter_ar_E510(3) = "0403"    ' ain bias
parameter_ar_E510(4) = "0404"    ' ain bias positive or negativ
parameter_ar_E510(5) = "2520"    ' monitor data
parameter_ar_E510(6) = "2521"    ' fault register
parameter_ar_E510(7) = "000E"    ' accel 1
parameter_ar_E510(8) = "000F"    ' decel 1
parameter_ar_E510(9) = "000C"    ' upper hz
parameter_ar_E510(10) = "0204"   ' motor volts
parameter_ar_E510(11) = "0201"   ' motor amps
parameter_ar_E510(12) = "0205"   ' motor kw
parameter_ar_E510(13) = "0203"   ' motor rpm
parameter_ar_E510(14) = "0206"   ' motor hz
parameter_ar_E510(15) = "0207"   ' motor poles
parameter_ar_E510(16) = "0800"   ' trip prevention
parameter_ar_E510(17) = "0C08"   ' not used in E510
parameter_ar_E510(18) = "0801"   ' trip prevention level accel
parameter_ar_E510(19) = "0C08"   ' not used in E510
parameter_ar_E510(20) = "0802"   ' trip prevention level decel
parameter_ar_E510(21) = "0C08"   ' not used in E510
parameter_ar_E510(22) = "0803"   ' trip prevention level run
parameter_ar_E510(23) = "0402"   ' ain gain
parameter_ar_E510(24) = "0807"   ' fan mode
parameter_ar_E510(25) = "0002"   ' run source selection
parameter_ar_E510(26) = "0005"   ' frequency source select
parameter_ar_E510(27) = "0301"   ' S2 setting
parameter_ar_E510(28) = "0302"   ' S3 setting
parameter_ar_E510(29) = "0B01"   ' carrier frequency
parameter_ar_E510(30) = "0B02"   ' carrier frequency mode
parameter_ar_E510(31) = "0B04"   ' S curve accel 1
parameter_ar_E510(32) = "0B05"   ' S curve accel 2
parameter_ar_E510(33) = "0B06"   ' S curve decel 3
parameter_ar_E510(34) = "0B07"   ' S curve decel 4
parameter_ar_E510(35) = "0707"   ' injection brake level
parameter_ar_E510(36) = "0706"   ' injection starting frequency
parameter_ar_E510(37) = "0708"   ' injection braking time
parameter_ar_E510(38) = "0000"   ' operation mode
parameter_ar_E510(39) = "0B37"   ' not used in E510
parameter_ar_E510(40) = "0704"   ' direct start setting
parameter_ar_E510(41) = "0D05"   ' elapsed time accumulation setting
parameter_ar_E510(42) = "0010"   ' accel 2 time
parameter_ar_E510(43) = "0011"   ' decel 2 time
parameter_ar_E510(44) = "0303"   ' S4 setting
parameter_ar_E510(45) = "0C08"   ' not used in E510
parameter_ar_E510(46) = "0100"   ' volts hertz pattern
parameter_ar_E510(47) = "010A"   ' this one may need to be used for E510
parameter_ar_E510(48) = "0300"   ' S1 setting
parameter_ar_E510(49) = "0805"   ' motor selection for overload protection
parameter_ar_E510(50) = "0200"   ' motor no load current
parameter_ar_E510(51) = "0703"   ' reset_mode
parameter_ar_E510(52) = "0D08"   ' drive reset
parameter_ar_E510(53) = "0D04"   'big_hrs
parameter_ar_E510(54) = "0D03"   'small_hrs
parameter_ar_E510(55) = "0109"   ' min_volt
parameter_ar_E510(56) = "0107"   ' mid_volt
parameter_ar_E510(57) = "252F"   ' Drive_check
parameter_ar_E510(58) = "0A03"  'PID control enable
parameter_ar_E510(59) = "0A00"   'PID setpoint source
parameter_ar_E510(60) = "0A02"   'PID setpoint value
parameter_ar_E510(61) = "252D"        'AI2 value reading
parameter_ar_E510(62) = "000D"          'lower limit of VSD Frequency

'A510s Array
parameter_ar_A510s(1) = "2524"    ' output hz
parameter_ar_A510s(2) = "2527"    ' output amps
parameter_ar_A510s(3) = "0403"    ' ain bias
parameter_ar_A510s(4) = "0404"    ' not used in A510s
parameter_ar_A510s(5) = "2520"    ' monitor data
parameter_ar_A510s(6) = "2521"    ' fault register
parameter_ar_A510s(7) = "000E"    ' accel 1
parameter_ar_A510s(8) = "000F"    ' decel 1
parameter_ar_A510s(9) = "000C"    ' upper hz
parameter_ar_A510s(10) = "0204"   ' motor volts
parameter_ar_A510s(11) = "0201"   ' motor amps
parameter_ar_A510s(12) = "0205"   ' motor kw
parameter_ar_A510s(13) = "0203"   ' motor rpm
parameter_ar_A510s(14) = "0206"   ' motor hz
parameter_ar_A510s(15) = "0207"   ' motor poles
parameter_ar_A510s(16) = "0800"   ' trip prevention
parameter_ar_A510s(17) = "0C08"   ' not used in A510s
parameter_ar_A510s(18) = "0801"   ' trip prevention level accel
parameter_ar_A510s(19) = "0C08"   ' not used in A510s
parameter_ar_A510s(20) = "0802"   ' trip prevention level decel
parameter_ar_A510s(21) = "0C08"   ' not used in A510s
parameter_ar_A510s(22) = "0803"   ' trip prevention level run
parameter_ar_A510s(23) = "0402"   ' ain gain
parameter_ar_A510s(24) = "0825"   ' fan mode
parameter_ar_A510s(25) = "0002"   ' run source selection
parameter_ar_A510s(26) = "0005"   ' frequency source select
parameter_ar_A510s(27) = "0301"   ' S2 setting
parameter_ar_A510s(28) = "0302"   ' S3 setting
parameter_ar_A510s(29) = "0B01"   ' carrier frequency
parameter_ar_A510s(30) = "0B02"   ' soft pwm mode
parameter_ar_A510s(31) = "0B04"   ' S curve accel 1
parameter_ar_A510s(32) = "0B05"   ' S curve accel 2
parameter_ar_A510s(33) = "0B06"   ' S curve decel 3
parameter_ar_A510s(34) = "0B07"   ' S curve decel 4
parameter_ar_A510s(35) = "0707"   ' injection brake level
parameter_ar_A510s(36) = "0706"   ' injection starting frequency
parameter_ar_A510s(37) = "0708"   ' injection braking time
parameter_ar_A510s(38) = "0000"   ' operation mode
parameter_ar_A510s(39) = "0B37"   ' set to 0, disable the stop key in A510s
parameter_ar_A510s(40) = "0704"   ' direct start setting
parameter_ar_A510s(41) = "0D05"   ' elapsed time accumulation setting
parameter_ar_A510s(42) = "0010"   ' accel 2 time
parameter_ar_A510s(43) = "0011"   ' decel 2 time
parameter_ar_A510s(44) = "0303"   ' S4 setting
parameter_ar_A510s(45) = "0C08"   ' not used in A510s
parameter_ar_A510s(46) = "0100"   ' volts hertz pattern
parameter_ar_A510s(47) = "010A"   ' this one may need to be used for A510s
parameter_ar_A510s(48) = "0300"   ' S1 setting
parameter_ar_A510s(49) = "0805"   ' motor selection for overload protection
parameter_ar_A510s(50) = "0200"   ' motor no load current
parameter_ar_A510s(51) = "0703"   ' not be used in A510s ?
parameter_ar_A510s(52) = "0D08"   ' drive reset
parameter_ar_A510s(53) = "0D04"   'big_hrs
parameter_ar_A510s(54) = "0D03"   'small_hrs
parameter_ar_A510s(55) = "0109"   ' min_volt
parameter_ar_A510s(56) = "0107"   ' mid_volt
parameter_ar_A510s(57) = "252F"   ' Drive_check
parameter_ar_A510s(58) = "0A03"  'PID control enable
parameter_ar_A510s(59) = "0A00"   'PID setpoint source
parameter_ar_A510s(60) = "0A02"   'PID setpoint value
parameter_ar_A510s(61) = "252D"        'AI2 value reading
parameter_ar_A510s(62) = "000D"          'lower limit of VSD Frequency

' aliases for each parameter
output_hz alias parameter_ar(1)
output_A alias parameter_ar(2)
Ain_bias alias parameter_ar(3)
AIN_pos_neg alias parameter_ar(4)
monitor_data alias parameter_ar(5)
error_register alias parameter_ar(6)
accel1 alias parameter_ar(7)
decel1 alias parameter_ar(8)
upper_hz alias parameter_ar(9)
motor_V alias parameter_ar(10)
motor_A alias parameter_ar(11)
motor_kw alias parameter_ar(12)
motorpm alias parameter_ar(13)
motohz alias parameter_ar(14)
motor_poles alias parameter_ar(15)
trip_prev alias parameter_ar(16)
'trip_prevA alias parameter_ar(17)
Trip_Level_A alias parameter_ar(18)
trip_prevB alias parameter_ar(19)
Trip_Level_B alias parameter_ar(20)
trip_prevR alias parameter_ar(21)
Trip_Level_R alias parameter_ar(22)
Ain_gain alias parameter_ar(23)
fan_mode alias parameter_ar(24)
start_stop alias parameter_ar(25)
freq_source alias parameter_ar(26)
S2 alias parameter_ar(27)              '2nd accel
S3 alias parameter_ar(28)              'RESET
carrier_freq alias parameter_ar(29)
carrier_mode alias parameter_ar(30)
S_curve_A1 alias parameter_ar(31)
S_curve_A2 alias parameter_ar(32)
S_curve_D3 alias parameter_ar(33)
S_curve_D4 alias parameter_ar(34)
brake_level alias parameter_ar(35)
brake_start alias parameter_ar(36)
brake_time alias parameter_ar(37)
op_mode alias parameter_ar(38)
stop_button_off alias parameter_ar(39)
direct_run alias parameter_ar(40)
operation_hours alias parameter_ar(41)
accel2 alias parameter_ar(42)
decel2 alias parameter_ar(43)
S4 alias parameter_ar(44)            '2ND ACCEL
Autotune alias parameter_ar(45)
VF_pattern alias parameter_ar(46)
Torque_boost alias parameter_ar(47)
S1 alias parameter_ar(48)                'FWD
motor_duty alias parameter_ar(49)
NL_motor_A alias parameter_ar(50)
reset_mode alias parameter_ar(51)
Drive_reset alias parameter_ar(52)
big_hrs alias parameter_ar(53)
small_hrs alias parameter_ar(54)
min_volt alias parameter_ar(55)
mid_volt alias parameter_ar(56)
Drive_check alias parameter_ar(57)
PID_enable alias parameter_ar(58)
PID_source alias parameter_ar(59)
PID_setpoint alias parameter_ar(60)
Analog2Value alias parameter_ar(61)
DownHz alias parameter_ar(62)




'error arrays
' error register of the E510S is so different from that of the CV and the E510 that using arrays will not help make the code simpler and
' will make the code longer unnecessarily complicated and more chances of making mistakes


' VARIABLE INITIALISATION
If Configured > 200 Then Configured = 0
FRateSwitch = 0
Null = 0
Password = 0000
Posi = 4
F_num = 1
Func_des1 = ""
Func_des2 = ""
Func_des3 = ""
H = "   "
Fault_num = 1
Pb_mode = 2
Pb_up_arrow = 2
Pb_dsp_fun = 2
Pb_run_stop = 2
Pb_down_arrow = 2
Pb_reset = 2
Pb_enter = 2
Menu = 0
Value = 0
Reset_c = 0
Error_reg = 0
Error_on = 0
Transducer_fault = 0
Cls_temp=0   'bug fix
P_gain = 1
I_gain = 2
5_i_gain = 5 * I_gain
10_i_gain = 10 * I_gain
20_i_gain = 20 * I_gain
25_i_gain = 25 * I_gain
30_i_gain = 30 * I_gain
Integral = 0
Integral2 = 0
Paused = 0
Run_Flag = 0
error_clear = 0
run_inhibit = 0
running_minutes = 0
running_hours = 0
time_exceeded = 0
flowswitch = 0
#IF PT_PressureHigh
on_standby = 0
#ELSE
on_standby = 1
#ENDIF

first_cycle = 1
going_to_standby = 0
resuming = 0
setup_setpoint = 0
'low_suction = 0
real_amps = 0
low_pressure = 0
actual_pressure = 0
'Reset_timer = 0
'reset_attempts = 0
Rundry = 0
test_current = 0
fault_bypass = 0
skip = 0
running = 0
remote_signal = 0
remote_switch = 0
start_stops = 0
total_cycles = 0
cycle_count = 0
Tim(13) = 240
run_stop = 0
temp_dword = 0
Number_of_resets = 0

For index_a = 1 To 24
   hour(index_a) = 0   'set all to zero
Next

If Resume_point > 5 Then Resume_point = 0
If Prime_time > 120 then Prime_time = 30
If Prime_Loss > 1 then Prime_Loss = 1
If BurstPipeTime > 120 then BurstPipeTime = 20
'If BurstPipeFlowRate > BurstPipeFRlimit then BurstPipeFlowRate = 500 'BurstPipeFRlimit
If BurstPipeEnable > 1 then BurstPipeEnable = 1
If Standby_Time > 120 then Standby_Time = 20
If Standby_offset > 999 then Standby_offset = 50
If BlockedPipeEnable > 2 then BlockedPipeEnable = 0
If LowPressEnable > 1 then LowPressEnable = 1
If LowPressTime > 120 then LowPressTime = 60

If operation_mode > 2 then operation_mode = 0
If operation_mode = 2 then soft_stop = 1
'If flow_switch <> 1 then flow_switch = 0
If local_stop_mem > 1 then local_stop_mem = 0
soft_stop = local_stop_mem

If WithFM = 1 and WithPT = 1 Then
   If disp_op >= 5 then disp_op = 0
Else
   If disp_op >= 4 then disp_op = 0
Endif
disp_op1 = disp_op
if user_units > 1 then user_units = 0

Setpoint = Setpoint_1
If user_units = 1 then
        If Psensor_type = 0 then
                sensor_size = 1450
        Elseif Psensor_type = 1 then
                sensor_size = 2030
        Else
                sensor_size = 2900
        EndIf
        units_str = "psi"
else
        If Psensor_type = 0 then
                sensor_size = 1000
        Elseif Psensor_type = 1 then
                sensor_size = 1400
        Else
                sensor_size = 2000
        EndIf
        units_str = "bar"
end if

if Fsensor_type = 0 then
        'Fsensor_size = 56
        Fsensor_size = 60
        Fsensor_bias = 40
        BurstPipeFRlimit = 600
elseif Fsensor_type = 1 then
        'Fsensor_size = 114
        Fsensor_size = 120
        Fsensor_bias = 60
        BurstPipeFRlimit  = 1200
elseif Fsensor_type = 2 then
        'Fsensor_size = 150
        Fsensor_size = 160
        Fsensor_bias = 100
        BurstPipeFRlimit = 1600
elseif Fsensor_type = 3 then
        'Fsensor_size = 234
        Fsensor_size = 250
        Fsensor_bias = 160
        BurstPipeFRlimit = 2500
elseif Fsensor_type = 4 then
        'Fsensor_size = 375
        Fsensor_size = 400
        Fsensor_bias = 250
        BurstPipeFRlimit = 4000
else
        'Fsensor_size = 560
        Fsensor_size = 600
        Fsensor_bias = 400
        BurstPipeFRlimit = 6000
End If

Fsensor_bias = 0

If BurstPipeFlowRate > BurstPipeFRlimit then BurstPipeFlowRate = BurstPipeFRlimit

sensor_trim = 1000


' populate arrays                 ' the register numbers are the same for E510 and E510S drives
If Drive = 2 or drive = 3 then
        for z = 1 to 62 '3 new parameters added, so total number comes to 60 11.11.20 John
                   parameter_ar(z) = parameter_ar_E510(z)     ' drive register array selected here
        next
else
        for z = 1 to 62 '3 new parameters added, so total number comes to 60 11.11.20 John
                   parameter_ar(z) = parameter_ar_A510s(z)     ' drive register array selected here
        next
endif


'===============================================================================
' AT STARTUP ONLY
'===============================================================================
Call Initialise
Call Poweronlogo
Wait 1
enable Interrupts
State = 1
'===============================================================================


Do
   Select Case State
      Case 0 : Call Sub_0()
         ' main screen display
      Case 1 : Call Sub_01
         ' check if new controller/drive
      Case 2 : Call Sub_02()
         ' function number display
      Case 3 : Call Sub_03()
         ' function value display
      Case 4 : Call Sub_04()
         ' fault history display
      Case 6 : Call Sub_06()
         ' setup new drive
   End Select
   Call Sub_fault_check()
   Call ChangeFlowRateSP

   If local_stop_mem <> soft_stop then
      local_stop_mem = soft_stop
      Writeeeprom local_stop_mem , 49      ' to remember if it was local stopped at powerup
   End If
Loop

'===============================================================================
' This is the start of the initialisation and opening screen display
'===============================================================================
Sub Initialise()
'Configure PortA as mixed I/O
   Porta = &H00
   Ddra = &HFB
'Configure PortB as mixed I/O
   Portb = &H00
   Ddrb = &HBF
   Config Portc = Output

'Configure PortD as mixed I/O

   Portd = &HC4
   Ddrd = &H3A

'Configure Timer0 for overflow interrupt
   Tccr0a = &H00
   Tccr0b = &H05
   Tcnt0 = &H00
   Ocr0a = &H00
   Ocr0b = &H00
'Configure Timer1 for 10 bit PWM
   Tccr1a = &H83                                            '83
   Tccr1b = &H0A                                            '0A
   Tcnt1h = &H00
   Tcnt1l = &H00

   Icr1h = &H00
   Icr1l = &H00

   Ocr1ah = &H00
   Ocr1al = &H00
   Ocr1bh = &H00
   Ocr1bl = &H00
'Configure Timer2 to be off
   Assr = &H00
   Tccr2a = &H00
   Tccr2b = &H00
   Tcnt2 = &H00
   Ocr2a = &H00
   Ocr2b = &H00
'External Interrupt(s) initialization
' INT0 : Off
' INT1 : Off
' INT2 : Off

   'Mcucr = &H00
 '   Eicra = &H00
'   Eimsk = &H00
'   Pcicr = &H00

   Eicra = &H02    ' falling edge of Int0
   Eimsk = &H01    ' select Int0 in mask
   Pcicr = &H00

'   Mcucsr = &H00
' Timer(s)/Counter(s) Interrupt(s) initialization
   Timsk0 = &H01
   Timsk1 = &H00
   Timsk2 = &H00
'Analog Comparator initialization
'Analog Comparator: Off
'Analog Comparator Input Capture by Timer/Counter 1: Off
'Analog Comparator Output: Off
   Acsr = &H80
   Adcsrb = &H00
'   Sfior = &H00
'Initialise global variables
   Ticks = 0
   Buzzer = 1
   Beeper = 0                                               'Make sure buzzer off
   Config Graphlcd = 128 * 64sed , Dataport = Portc , Controlport = Porta , Ce = 3 , Ce2 = 4 , Cd = 5 , Rd = 6 , Enable = 7 , Reset = Portd4
   Setfont Font8x8
   Ddra = &HFB
   Config Serialout = Buffered , Size = 40
   Config Serialin = Buffered , Size = 40
'configure single mode and auto prescaler setting
'The single mode must be used with the GETADC() function
'The prescaler divides the internal clock by 2,4,8,16,32,64 or 128
'Because the ADC needs a clock from 50-200 KHz
'The AUTO feature, will select the highest clockrate possible
'Config Adc = Single , Prescaler = Auto , Reference = Internal
'Now give power to the chip
'Start Adc
'With STOP ADC, you can remove the power from the chip
'Stop Adc
   Didr0 = &H04
   Admux = &HC2
   Adcsra = &H87
   Adcsrb = &H00
   Anaval_l = 0
   Return
End Sub

'===============================================================================
Sub Readadc()
   Bitwait Adcsra.6 , Reset
   Set Adcsra.6
   Bitwait Adcsra.6 , Reset
   Adcvallo = Adcl
   Adcvalhi = Adch
   Shift Adcvalhi , Left , 8
   Anaval_l = Adcvalhi Or Adcvallo
End Sub
'===============================================================================
Sub Clear_buff()
   Clear Serialout
   Clear Serialin
   Waitms 10
   Return
End Sub
'===============================================================================
Sub Receive_data()
   A = Inkey()
   If A <> "" Then
      Comstring = Comstring + A
   End If
   B = B + 1
   Return
End Sub
'===============================================================================
Sub Sub_0

#IF Test_disp                                 'standby "; str(on_standby); "
Lcdat 7, 9, pressure1 ; " "; standby_pressure1; " ";  tim(7); " "; Anaval_l
#ENDIF


'reset the password and the f_num and the cursor position
   If Password <> 0 Then
      Password = 0
   End If
   If F_num <> 1 Then
      F_num = 1
   End If
   If Posi <> 4 Then
      Posi = 4
   End If

'RPM line
   Top_speed = Maximum_speed - 50
   Function_number = output_hz
   Call Build_read_string()      'read output RPM string

   'This block of code waits until there is something in the receive
    'buffer before proceeding
   Passes = 0
   Do
      Waitms 10
      Read_buffer_count = _rs_bufcountr0
      Passes = Passes + 1
      If Passes >= 10 Then Exit Do
   Loop Until Read_buffer_count > 0
   Waitms 10

   Comstring = ""
   B = 0
   Do
      Call Receive_data()
      If B > 25 Then
         No_read_rpm = No_read_rpm + 1
         Exit Do
      End If
   Loop Until A = Chr(10)
   If Len(comstring) = 15 Then
      D = Mid(comstring , 8 , 4)
      No_read_rpm = 0
      C = Hexval(d)
      C = C * Motor_rpm
      C = C / Motor_hz
      C = C / 10
      If Running = 1 Then
         If C < Minimum_speed Then C = Minimum_speed
         If C > Top_speed Then C = Maximum_speed
      End If

      Actual_rpmN = C 'get the real speed now to check whether pipe burst occurs

      D = Str(c)
      Actual_rpm = Format(d , "###0")
   End If
   Call Clear_buff()

'Current(A) line
   Function_number = output_A
   Call Build_read_string()      'read output current string

  'This block of code waits until there is something in the receive
    'buffer before proceeding
   Passes = 0
   Do
      Waitms 10
      Read_buffer_count = _rs_bufcountr0
      Passes = Passes + 1
      If Passes >= 10 Then Exit Do
   Loop Until Read_buffer_count > 0
   Waitms 10

   Comstring = ""
   B = 0
   Do
      Call Receive_data()
      If B > 25 Then
         No_read_current = No_read_current + 1
         Exit Do
      End If
   Loop Until A = Chr(10)
   If Len(comstring) = 15 Then
      D = Mid(comstring , 8 , 4)
      If Hexval(d) < 500 Then
         No_read_current = 0
         C = Hexval(d)
         Real_amps = C
         D = Str(c)
         Actual_amps = Format(d , "#0.0")
      End If
   End If
   Call Clear_buff()

 'read hours every 4 minutes
   If Tim(13) <= 0 Then
      Tim(13) = 240
      Call Clear_buff()
'get Hours
      function_number = small_hrs
      call build_read_string

      Comstring = ""
      B = 0
      Do
         Call Receive_data()
         If B > 25 Then
            Tim(13) = 0
            Exit Do
         End If
      Loop Until A = Chr(10)
      If Len(comstring) = 15 Then
         D = Mid(comstring , 8 , 4)
         If Hexval(d) < 10000 Then
            Hours = Hexval(d)
         Else
            Tim(13) = 0
         End If
      Else
         Tim(13) = 0
      End If

  'get Big Hours
      function_number = big_hrs
      call build_read_string

      Comstring = ""
      B = 0
      Do
         Call Receive_data()
         If B > 25 Then
            Tim(13) = 0
            Exit Do
         End If
      Loop Until A = Chr(10)
      If Len(comstring) = 15 Then
         D = Mid(comstring , 8 , 4)
         If Hexval(d) < 100000 Then
            hours_large = Hexval(d)
         Else
            Tim(13) = 0
         End If
      Else
         Tim(13) = 0
      End If

      'If drive = 2 then
         hours_large = hours_large * 24
      'elseif drive = 1 then
         'hours_large = hours_large * 10000
      'end If
      hours = hours + hours_large
   End If

   lcdat 2 , 1 , "                "
   If operation_mode = 1 then
      lcdat 2 , 5 , "**FLOW SWITCH**"
      lcdat 5 , 5 , "              "
      If rly1 = 0 then
         If remote_switch = 1 then
            If soft_stop = 0 then
               lcdat 5 , 5 , "     READY    "
            else
               lcdat 5 , 1 , "   LOCAL STOP   "
            end if
         else
            Lcdat 5 , 5 , "  REMOTE STOP  "
         end if
      else
         lcdat 5 , 5 , "    RUNNING   "
      End If
   elseif operation_mode = 2 then
      lcdat 2 , 1 , "                "
      lcdat 2 , 5 , "MANUAL OVERRIDE"

      lcdat 5 , 1 , "                "
      If rly1 = 0 then
         If remote_switch = 0 then
            Lcdat 5 , 5 , "  REMOTE STOP  "
         else
            lcdat 5 , 1 , "   LOCAL STOP   "
         end if
      else
         lcdat 5 , 5 , "    RUNNING   "
      End If
   else

      If WithPT = 1 Then
         disp_setpoint_str = str(setpoint) 'MAXIMUM PRESSURE LIMIT
         If user_units = 0 then
            disp_setpoint_str = Format(disp_setpoint_str , "#0.00")
         else
            disp_setpoint_str = Format(disp_setpoint_str , "#0.0")
         end if
      Endif

      If WithFM = 1 and on_switch = 0 Then
            select case FRateSwitch
               case 1: disp_setpoint_str = str(FlowRateSetpoint)    'flow rate target value
               case 2: disp_setpoint_str = str(FlowRateSetpoint1)
               case 3: disp_setpoint_str = str(FlowRateSetpoint2)
            End Select

            disp_setpoint_str = Format(disp_setpoint_str , "#0.0")
      Endif

      If  on_switch = 1 and WithFM = 1 and WithPT = 1 Then
         select case len(disp_setpoint_str)
            case 1 : lcdat 1 , 1 , "MAX   :    " ; disp_setpoint_str ; " " ; units_str
            case 2 : lcdat 1 , 1 , "MAX   :   "  ; disp_setpoint_str ; " " ; units_str
            case 3 : lcdat 1 , 1 , "MAX   :  "  ; disp_setpoint_str ; " " ; units_str
            case 4 : lcdat 1 , 1 ,"MAX   : "  ; disp_setpoint_str ; " " ; units_str
            case else : lcdat 1 , 1 , "MAX   :"  ; disp_setpoint_str ; " " ; units_str
         End Select
      Else
         If WithFM = 1 Then
               select case len(disp_setpoint_str)
                     case 1 : lcdat 1 , 1 , "TARGET:    " ; disp_setpoint_str ; "CMH"
                     case 2 : lcdat 1 , 1 , "TARGET:   " ; disp_setpoint_str ; "CMH "
                     case 3 : lcdat 1 , 1 , "TARGET:  " ; disp_setpoint_str ;  "CMH"
                     case 4 : lcdat 1 , 1 , "TARGET: " ; disp_setpoint_str ;  "CMH "
                     case else : lcdat 1 , 1 , "TARGET:" ; disp_setpoint_str ; "CMH "
               End Select
          Else
               select case len(disp_setpoint_str)
                     case 1 : lcdat 1 , 1 , "TAGET:    " ; disp_setpoint_str ; " " ; units_str
                     case 2 : lcdat 1 , 1 , "TAGET:   "  ; disp_setpoint_str ; " " ; units_str
                     case 3 : lcdat 1 , 1 , "TAGET:  "  ; disp_setpoint_str ; " " ; units_str
                     case 4 : lcdat 1 , 1 ,"TAGET: "  ; disp_setpoint_str ; " " ; units_str
                     case else : lcdat 1 , 1 , "TAGET:"  ; disp_setpoint_str ; " " ; units_str
               End Select
          Endif
      Endif

   End If

   If  on_switch = 1 or WithFM = 0 Then
      pressure_display_str = str(actual_pressure)
      if user_units = 0 then
         pressure_display_str = Format(pressure_display_str , "#0.00")
      else
         pressure_display_str = Format(pressure_display_str , "#0.0")
      end if
   else
      pressure_display_str = str( flowRate)
      pressure_display_str = Format(pressure_display_str , "#0.0")
   Endif




   If operation_mode = 0 then
      Lcdat 2 , 1 , "                 "
      If  on_switch = 1 or  WithFM = 0 Then
         select case len(Pressure_display_str)
            case 1 : Lcdat 2 , 1 , "ACTUAL:    " ; Pressure_display_str ; " " ; units_str
            case 2 : Lcdat 2 , 1 , "ACTUAL:   " ; Pressure_display_str ; " " ; units_str
            case 3 : Lcdat 2 , 1 , "ACTUAL:  " ; Pressure_display_str ; " " ; units_str
            case 4 : Lcdat 2 , 1 , "ACTUAL: " ; Pressure_display_str ; " " ; units_str
            case else : Lcdat 2 , 1 , "ACTUAL:" ; Pressure_display_str ; " " ; units_str
         End Select
      Else
         select case len(Pressure_display_str)
            case 1 : Lcdat 2 , 1 , "ACTUAL:    " ; Pressure_display_str ; "CMH"
            case 2 : Lcdat 2 , 1 , "ACTUAL:   " ; Pressure_display_str ;"CMH "
            case 3 : Lcdat 2 , 1 , "ACTUAL:  " ; Pressure_display_str ; "CMH "
            case 4 : Lcdat 2 , 1 , "ACTUAL: " ; Pressure_display_str ; "CMH"
            case else : Lcdat 2 , 1 , "ACTUAL:" ; Pressure_display_str ; "CMH "
         End Select
      Endif

   #IF test_disp
   'Show the raw data of pressure
   'Lcdat 3, 1, str(Pressure1); " "; str(switch_pressure1); " "; str(standby_pressure1);  " "; str(anaval_l)
   #ENDIF

   'Status line
      Lcdat 4 , 1 , "                "
      Lcdat 6 , 1 , "                "
      If Running = 1 Then

#IF PT_PressureHigh
            If going_to_standby = 1 and Rly1 = 1 and on_standby = 0 then
               Lcdat 4 , 1 , "    Pressure    "
               Lcdat 5 , 1 , "      High      "
               If BlockedPipeEnable = 1 Then   Lcdat 6 , 1 , " STOP IN " ; tim(3) ; "s"
                If BlockedPipeEnable = 2 Then   Lcdat 6 , 1 , "STANDBY IN" ; tim(3) ; "s"
               If tim(7) > 0 then tim(7) = resume_time
  #ENDIF

  #IF FM_BurstPipe
            elseif BurstPipePre = 1 and Rly1 = 1 and burstpipe = 0 then
               Lcdat 4 , 1 , "  Burst Pipe"
               Lcdat 5 , 1 , "                "
               Lcdat 6 , 1 , " STOP  IN " ; tim(16) ; "s"
               If tim(7) > 0 then tim(7) = resume_time
            else
  #ENDIF
               If tim(7) > 0   then
                  If on_standby = 0 Then
                        Lcdat 4 , 1 , "  START DELAY   "
                        Lcdat 5 , 1 , "                "
                        Lcdat 6 , 1 , "  START IN " ; tim(7) ; "s"
                  elseif  anaval_l < pressure1   then
                         Lcdat 4 , 1 , "                "
                         Lcdat 5 , 1 , "    Standby     "
                         Lcdat 6 , 1 , "                "
                  Else
                        Lcdat 4 , 1 , "  RESUMING...   "
                        Lcdat 5 , 1 , "                "
                        Lcdat 6 , 1 , "  START IN " ; tim(7) ; "s"
                  Endif
                   tim(10) =  accel_time/10
                   tim(17) = 35
               elseif  going_to_switch = 1 then
                  Lcdat 4 , 1 , "  SWITCH TO"
                  Lcdat 5 , 1 , "  Control (P)   "
                  Lcdat 6 , 1 , "  SWITCH IN " ; tim(15) ; "s"
               else
                  If on_switch = 1 then
                      Lcdat 4 , 1 , " RUNNING(P) FOR"
                  Else
                      Lcdat 4 , 1 , " RUNNING  FOR"
                  Endif

                  Lcdat 5 , 1 , "      " ; running_hours ; "H " ; running_minutes ; "M   "

               Endif
            End If

      else
         If soft_stop = 1 then
            Lcdat 4 , 1 , "   LOCAL STOP  "
            Lcdat 5 , 1 , "                "
         else
            Lcdat 4 , 5 , "  REMOTE STOP  "
            Lcdat 5 , 1 , "                "
         End If
      End If

      If max_hours > 0     Then
            Dim max_hour as byte
            Dim max_minute as byte
            max_hour = max_hours / 10
            max_minute = max_hours MOD 10

            If max_minute > 0 Then max_minute = 30
            If running_hours >= max_hour and running_minutes >= max_minute Then 'and max_hours <> 0 then
               time_exceeded = 1
               pb_run_stop = 1
               cls
               Lcdat 1 , 1 , "  MAX RUN TIME  "
               Lcdat 2 , 1 , "    EXCEEDED    "
               Lcdat 4 , 1 , " TURN OFF OR"
               Lcdat 6 , 1 , " PRESS RESET TO"
               Lcdat 7 , 1 , "    CONTINUE    "

               do
                  if pb_reset = 1 then
                     pb_reset = 2
                     pb_enter = 2
                     pb_mode = 2
                     pb_run_stop = 1
                     running_hours = 0
                     running_minutes = 0
                     time_exceeded = 0
                     cls
                     exit do
                  end if
               loop
         End If
       End If
   End If

   Lcdat 8 , 1 , "                "

   If disp_op = 1 then

      If No_read_current > 5 Then
         No_read_current = 6
         Lcdat 8 , 5 , "     ##.#A     "
      else
         Select Case Len(actual_amps)
            Case 0 : Lcdat 8 , 1 , "     " ; Actual_amps ; " Amps"
            Case 1 : Lcdat 8 , 1 , "     " ; Actual_amps ; " Amps"
            Case 2 : Lcdat 8 , 5 , "    " ; Actual_amps ; " Amps"
            Case 3 : Lcdat 8 , 1 , "    " ; Actual_amps ; " Amps"
            Case 4 : Lcdat 8 , 5 , "   " ; Actual_amps ; " Amps"
            Case else : Lcdat 8 , 5 , " " ; Actual_amps ; " Amps"
         End Select
      end if
   elseif disp_op = 2 then
      If No_read_rpm > 5 Then
         Lcdat 8 , 1 , "    #### RPM    "
         No_read_rpm = 6
      else
         if rly1 = 0 then
            Lcdat 8 , 5 ,  "     0 RPM     "
         else
            Select Case Len(actual_rpm)
               Case 1 : Lcdat 8 , 5 , "     " ; Actual_rpm ; " RPM "
               Case 2 : Lcdat 8 , 1 , "     " ; Actual_rpm ; " RPM "
               Case 3 : Lcdat 8 , 5 , "    " ; Actual_rpm ; " RPM "
               Case 4 : Lcdat 8 , 1 , "    " ; Actual_rpm ; " RPM "
            End Select
         End If
      end if
   elseif disp_op = 3 then
      select case hours
         case 0 to 9: lcdat 8 , 21 , hours ; " Run Hours"
         case 9 to 99: lcdat 8 , 17 , hours ; " Run Hours"
         case 100 to 999: lcdat 8 , 13 , hours ; " Run Hours"
         case 1000 to 9999: lcdat 8 , 9 , hours ; " Run Hours"
         case 10000 to 99999: lcdat 8 , 5 , hours ; " Run Hours"
         case 100000 to 999999: lcdat 8 , 1 , hours ; " Run Hours"
         case 1000000 to 9999999: lcdat 8 , 1 , hours ; "Run Hours"
         case 10000000 to 99999999: lcdat 8 , 1 , hours ; "Run Hours"
         case 100000000 to 999999999: lcdat 8 , 1 , hours ; "Run Hours"
         case else: lcdat 8 , 1 , hours ; "Run Hours"
      end select
   elseif disp_op = 4 then
      select case total_cycles
         case 0 to 9: lcdat 8 , 33 , total_cycles ; " Cycles"
         case 9 to 99: lcdat 8 , 29 , total_cycles ; " Cycles"
         case 100 to 999: lcdat 8 , 25 , total_cycles ; " Cycles"
         case 1000 to 9999: lcdat 8 , 21 , total_cycles ; " Cycles"
         case 10000 to 99999: lcdat 8 , 17 , total_cycles ; " Cycles"
         case 100000 to 999999: lcdat 8 , 13 , total_cycles ; " Cycles"
         case 1000000 to 9999999: lcdat 8 , 9 , total_cycles ; " Cycles"
         case 10000000 to 99999999: lcdat 8 , 5 , total_cycles ; " Cycles"
         case else: lcdat 8 , 1 , total_cycles ; " Cycles"
      end select
   elseif disp_op = 5 then
      If WithFM = 1 and WithPT = 1 Then
           If On_switch = 1 Then
                bottom_display_str = str( flowRate)
                bottom_display_str = Format(bottom_display_str , "#0.0")
                select case len(Pressure_display_str)
                              case 1 : Lcdat 8 , 1 , "        " ; bottom_display_str ; "CMH"
                              case 2 : Lcdat 8 , 1 , "       " ; bottom_display_str ;"CMH "
                              case 3 : Lcdat 8 , 1 , "      " ; bottom_display_str ; "CMH "
                              case 4 : Lcdat 8 , 1 , "     " ; bottom_display_str ; "CMH"
                              case else : Lcdat 8 , 1 , "    " ; bottom_display_str ; "CMH"
                 End Select
           Else
                   bottom_display_str = str(actual_pressure)
                   If user_units = 0 then
                        bottom_display_str = Format(bottom_display_str , "#0.00")
                   Else
                        bottom_display_str = Format(bottom_display_str , "#0.0")
                   End if
                  select case len(bottom_display_str)
                           case 1 : Lcdat 8, 1 , "       " ; bottom_display_str ; " " ; units_str
                           case 2 : Lcdat 8, 1 , "     " ; bottom_display_str ; " " ; units_str
                           case 3 : Lcdat 8, 1 , "    " ; bottom_display_str ; " " ; units_str
                           case 4 : Lcdat 8, 1 , "    " ; bottom_display_str ; " " ; units_str
                           case else : Lcdat 8, 1 , "   "; bottom_display_str ; " " ; units_str
                  End Select
           Endif
      Else
            disp_op = 0
      Endif
   else
      Lcdat 8 , 1 , " Version: 1.0.0 "
   end if

#IF Test_Disp
   Lcdat 7, 1, disp_op
#ENDIF

   If error_clear = 0 then
      If Pb_reset = 1 Then
         Pb_reset = 2
         Waitms 100
      End If
   else
      error_clear = 0
   End If

   If pb_up_arrow = 1 then
      tim(14) = 10
      pb_up_arrow = 2
      pb_down_arrow = 2


      disp_op = disp_op + 1
      If WithFM = 1 and WithPT = 1 Then
         If disp_op > 5 then disp_op = 0
      Else
         If disp_op > 4 then disp_op = 0
      Endif

   end if

   If pb_down_arrow = 1 then
      tim(14) = 10
      pb_up_arrow = 2
      pb_down_arrow = 2

      disp_op = disp_op - 1
      If WithFM = 1 and WithPT = 1 Then
         If disp_op > 5 then disp_op = 5
      Else
         If disp_op > 4 then disp_op = 4
      Endif

   end if

   If tim(14) <= 0 and disp_op <> disp_op1 then
      disp_op1 = disp_op
      writeeeprom disp_op1 , 55
   end if

   '  Enter function menu
   If Pb_dsp_fun = 1 Then
      If f_num = 2 or f_num = 3 or f_num = 4 or f_num = 5 or f_num = 6 then f_num = 1
      If f_num = 8 or f_num = 9 or f_num = 10 or f_num = 11 or f_num = 12 then f_num = 7
      If f_num = 14 or f_num = 15 then f_num = 13
      If f_num = 18 or f_num = 17 then f_num = 16
      Pb_dsp_fun = 2
      State = 2
      Waitms 100
      Cls
   End If

   If pb_mode = 1 then
      pb_mode = 2
      pb_enter = 2
      pb_reset = 2
      pb_up_arrow = 2
      pb_down_arrow = 2
      pb_dsp_fun = 2
      pb_run_stop = 2
      call mode_select
   End If

'Do a reset of the pushbuttons that aren't used in this sub to ensure that they
'don't latch on

   If Pb_reset = 1 Then
      Reset_flag = 1
      Pb_reset = 2
   End If
   Pb_enter = 2
   Pb_mode = 2
   Return
End Sub

'===============================================================================
Sub mode_select()

   Dim temp_mode as byte
   Dim temp_mode1 as byte
   temp_mode = operation_mode
   temp_mode1 = operation_mode + 1
   fault_bypass = 1
   transducer_fault = 0
   If error_reg = 99 then error_reg = 0

   cls

   Lcdat 1 , 1 , "MODE OPTION LIST"
   Lcdat 6 , 1 , " ^/v To Change"
   Lcdat 7 , 1 , "  Press Enter "
   Lcdat 8 , 1 , "   To Select  "
   Do
      If temp_mode1 <> temp_mode then
         temp_mode1 = temp_mode
         Select Case Temp_mode1
            Case 0:
               Lcdat 4 , 1 , "                "
               Lcdat 4 , 1 , "   Transduced   "
            Case 1:
               Lcdat 4 , 1 , "                "
               Lcdat 4 , 5 , "  Flow Switch  "
            Case 2:
               Lcdat 4 , 1 , "                "
               Lcdat 4 , 5 , "Manual Override"                                                       '
         End Select
      End If

      If pb_up_arrow = 1 then
         pb_up_arrow = 2
         pb_down_arrow = 2
         pb_dsp_fun = 2
         pb_run_stop = 2
         pb_mode = 2
         pb_enter = 2
         pb_reset = 2

         If temp_mode = 2 then temp_mode = 0 else temp_mode = temp_mode + 1
      End If

      If pb_down_arrow = 1 then
         pb_up_arrow = 2
         pb_down_arrow = 2
         pb_dsp_fun = 2
         pb_run_stop = 2
         pb_mode = 2
         pb_enter = 2
         pb_reset = 2

         If temp_mode = 0 then temp_mode = 2 else temp_mode = temp_mode - 1
      End If

      If pb_enter = 1 then
         pb_up_arrow = 2
         pb_down_arrow = 2
         pb_dsp_fun = 2
         pb_run_stop = 2
         pb_mode = 2
         pb_enter = 2
         pb_reset = 2

         'first cycle flag clears the standby timer so if there is already pressure
         'in the system then the controller can go into standby straight away
         If operation_mode <> temp_mode then
            If operation_mode = 0 then first_cycle = 1
            If operation_mode = 2 and temp_mode = 0  Then soft_stop = 1

            operation_mode = temp_mode

            If operation_mode <> 2 then rly2 = 0
            If operation_mode = 2 then
               pb_run_stop = 1
               'Call MyAssert(999)
            Endif
            If operation_mode = 1 then pb_reset = 1
            Writeeeprom operation_mode , 48
         End If
         waitms 100
         fault_bypass = 0
         exit do
      End If

      If pb_dsp_fun = 1 or pb_mode = 1 then
         pb_up_arrow = 2
         pb_down_arrow = 2
         pb_dsp_fun = 2
         pb_run_stop = 2
         pb_mode = 2
         pb_enter = 2
         pb_reset = 2
         fault_bypass = 0
         exit do
      End If
   loop

   cls

End Sub

'===============================================================================
Sub Sub_fault_check()

'Call MyAssert(WithFM)
'Call MyAssert(WithPT)

If WithFM = 1 Then
#IF   FM_Rundry
   flowRate = FnGetFlowRate()
   if flowRate > -1 and flowRate < RunDryFlowRate and Prime_loss = 1 then
      RundryPre = 1
   else
      RundryPre = 0
   Endif
#ENDIF

#IF FM_BurstPipe
     if Actual_rpmN < min_speed_temp and flowRate > BurstPipeFlowRate and BurstPipeEnable = 1 then    'if the pump is running at a very low speed meanwhile flow rate is high, error "burst pipe" happened
       BurstPipePre = 1
     else
       BurstPipePre = 0
     Endif

     if BurstPipe = 1 Then   Error_reg = 101
#ENDIF
Endif

#IF PT_PressureHigh
   If on_standby = 1 And BlockedPipeEnable = 1 Then   Error_reg = 100
#ENDIF



   If Rundry >= 1 Then Error_reg = 98

   If ext_interlock >= 100 then Error_reg = 97

   'If low_suction >= 30 then Error_reg = 96             'low_suction is not bing used

   If low_pressure >= 254 then
      Error_reg = 95
   Endif
'   If no_flow = 1 then error_reg = 100

   If Error_reg = 99 Or Error_reg = 220 Or Error_reg = 98 Then Call Write_faults()
   If Error_reg = 95 Or Error_reg = 96 Or Error_reg = 97  Then Call Write_faults()

#IF PT_PressureHigh
   If error_reg = 100 then Call Write_faults()
#ENDIF

#IF FM_BurstPipe
    If error_reg = 101 then Call Write_faults()
#ENDIF

'check register 120H to see if bit 3 ('abnormal') is set
'if it is then log whatever fault is in the fault register

   C = 0
   Function_number = monitor_data
   call build_read_string()

   'This block of code waits until there is something in the receive
   'buffer before proceeding
   Passes = 0
   Do
      Waitms 10
      Read_buffer_count = _rs_bufcountr0
      Passes = Passes + 1
      If Passes >= 10 Then Exit Do
   Loop Until Read_buffer_count > 0
   Waitms 10

   Comstring = ""
   B = 0
   Do
      Call Receive_data()
      'We can check if we've dropped comms here, instead of everytime in sub_0
      If B > 50 Then
         Waitms 100
         Call Clear_buff()
         Exit Sub
      End If
   Loop Until A = Chr(10)
   If Len(comstring) = 15 Then
      D = Mid(comstring , 8 , 4)
      C = Hexval(d)
    '  Lcdat 1 , 1 , C
   'c now contains the results of the set bits in register 120H
   'if bit 3 is set (value 8) then we want to proceed to getting the number
   'from the fault register, otherwise we will just exit the sub
      If drive = 2 Then 'drive <> 3 then, this is the code before A510s is added  ' mask out last digit
         if C >= 16 Then
            C = C - 16
         End If
      else
      ' mask out last 11 bits
         Select Case C
            case Is  => 32768: C = C - 32768
            case Is  => 16384: C = C - 16384
            case Is  => 8192: C = C - 8192
            case Is  => 4096: C = C - 4096
            case Is  => 2048: C = C - 2048
            case Is  => 1024: C = C - 1024
            case Is  => 512: C = C - 512
            case Is  => 256: C = C - 256
            case Is  => 128: C = C - 128
            case Is  => 64: C = C - 64
            case Is  => 32: C = C - 32
            case Is  => 16: C = C - 16
         end select
      end if
      If C < 8 Then
         Call Clear_buff()
         Error_on = 0
         Exit Sub
      End If
   End If
   Call Clear_buff()

'*******
   C = 0
   Function_number = error_register
   call build_read_string()
   Comstring = ""
   B = 0
   Do
      Call Receive_data()
      If B > 50 Then
         Waitms 100
         Call Clear_buff()
         Exit Sub
      End If
   Loop Until A = Chr(10)
   If Len(comstring) = 15 Then
      Write_string_reg = Mid(comstring , 2 , 10)
      Call Calc_chksm()
      Write_string_reg = Hex(csum)
      If Write_string_reg = Mid(comstring , 12 , 2) Then
         D = Mid(comstring , 8 , 4)
         C = Hexval(d)
         Error_reg = C

         if drive = 2 then
            If Error_reg >= 1 And Error_reg <= 45 And Error_reg <> 32 And Error_reg <> 33 And Error_reg <> 3 And Error_reg <> 10 And Error_reg <> 26 Then
               Call Write_faults()
            end if
         else
            If Error_reg >= 1 And Error_reg <= 61 And Error_reg <> 44 Then
               Call Write_faults()
            end if
         end if
         If Error_reg = 0 Then Error_on = 0
      End If
   End If
   Call Clear_buff()
   Return
End Sub
'=============================================================================
Sub Sub_02
   Waitms 100
'   cls
   String_to_send = ""
   Lcdat 2 , 1 , " ______________ "
   Lcdat 3 , 1 , "|              |"

   Select Case F_num
      Case 00 : Lcdat 4 , 1 , "|  CTRL RESET  |"
      Case 01 : Lcdat 4 , 1 , "|   GENERAL    |"
      Case 02 : Lcdat 4 , 1 , "|  OPERATION   |"
      Case 03 : Lcdat 4 , 1 , "|  PROTECTION  |"
      Case 05 : Lcdat 4 , 1 , "|  ACCEL TIME  |"
      Case 06 : Lcdat 4 , 1 , "|  DECEL TIME  |"
      Case 07 : Lcdat 4 , 1 , "|  MIN. SPEED  |"        'GENERAL
      Case 08 : Lcdat 4 , 1 , "|  MAX. SPEED  |"
      Case 09 : Lcdat 4 , 1 , "| MOTOR CURRENT|"
      Case 10 : Lcdat 4 , 1 , "| SWITCH PRESS |"
      Case 11 : Lcdat 4 , 1 , "|PRESSURE RANGE|"
      Case 12 : Lcdat 4 , 1 , "|FLOWRATE RANGE|"

'                Lcdat 5 , 1 , "|   PRESSURE   |"
'      Case 11 : Lcdat 4 , 1 , "|  SP. PRESS2  |"
'      Case 12 : Lcdat 4 , 1 , "|MAX.PRESS OFFSET|"       'OPERATION
'      Case 13 : Lcdat 4 , 1 , "|RESUME OFFSET |"
      Case 14 : Lcdat 4 , 1 , "| MAX RUN TIME |"
      Case 15 : Lcdat 4 , 1 , "|  CTL DELAY   |"
'                Lcdat 5 , 1 , "|     DELAY    |"
      Case 16 : Lcdat 4 , 1 , "| START DELAY  |" '"| RESUME TIMER |" is used as start delay

      Case 17 : Lcdat 4 , 1 , "|HIGH PRESSURE |"
      Case 18 : Lcdat 4 , 1 , "| LOW PRESSURE |"      'PROTECTION
      Case 19 : Lcdat 4 , 1 , "|   RUN DRY    |"
      Case 20 : Lcdat 4 , 1 , "|  BURST PIPE  |"

'      Case 23 : Lcdat 4 , 1 , "|  FLOW SWITCH |"
      Case 21 : Lcdat 4 , 1 , "|    STATE     |"  'RUNDRY PROTECTION ENABLED OR DISABLED
      Case 22 : Lcdat 4 , 1 , "|  DELAY TIME  |"  'FLOWMETER
      Case 23 : Lcdat 4 , 1 , "|    LIMIT     |"  'FLOWMETER

      Case 24 : Lcdat 4 , 1 , "|  MONITORING  |"  ' this function group has functions 25 and 26 but is not needed in the functions list therefore hidden
      Case 25 : Lcdat 4 , 1 , "|CYCLES IN 24HR|"   'Monitoring
      Case 26 : Lcdat 4 , 1 , "|RUNNING HOURS |"

      case 27 : Lcdat 4 , 1 , "|    STATE     |"         'Low Pressure Protection
      Case 28 : Lcdat 4 , 1 , "|  DELAY TIME  |"
      Case 29 : Lcdat 4 , 1 , "|    LIMIT     |"

      case 30 : Lcdat 4 , 1 , "|    STATE     |"         'Burst Pipe Protection
      Case 31 : Lcdat 4 , 1 , "|  DELAY TIME  |"
      Case 32 : Lcdat 4 , 1 , "|    LIMIT     |"

      case 33 : Lcdat 4 , 1 , "|    STATE     |"         'Blocked Pipe Protection
      Case 34 : Lcdat 4 , 1 , "|  DELAY TIME  |"
      Case 35 : Lcdat 4 , 1 , "|    OFFSET    |"

      Case 97 : Lcdat 4 , 1 , "|  USER UNITS  |"
      Case 98 : Lcdat 4 , 1 , "|  FAULT HIST. |"
      Case 99 : Lcdat 4 , 1 , "|   PASSWORD   |"

      Case 106 : Lcdat 4 , 1, "|  SETPOINT 1  |"
      Case 107 : Lcdat 4 , 1, "|  SETPOINT 2  |"
      Case 108 : Lcdat 4 , 1, "|  SETPOINT 3  |"
      Case Else : F_num = 1
   End Select

   Lcdat 5 , 1 , "|______________|"
   Lcdat 7 , 1 , " ^/v To Change  "
   Lcdat 8 , 1 , "   Press Read   "

   If Jp1_24 = 1 then
      F_num = 98
   Else
      If Pb_up_arrow = 1 Then
         Pb_up_arrow = 2

         If Password = 1111 then
            If f_num = 3 then
               f_num = 97
               goto done
            End If
         end if

         If f_num = 3 then
'            f_num = 24
'            goto done
'         End If                    'skip the monitoring group
'         If f_num = 24 then
            f_num = 98
            goto done
         End If

         If WithPT = 1 and WithFM = 1 Then
                 If f_num = 20 then
                            f_num = 17
                            goto done
                 End If
         Elseif WithPT = 1 and WithFM = 0 Then
                  If f_num = 18 then
                            f_num = 17
                            goto done
                 End If
         Elseif WithPT = 0 and WithFM = 1 Then
                  If f_num = 20 then
                            f_num = 19
                            goto done
                 End If
         Endif

         'for newly added setpoint of flow rate
         If f_num = 108 then
            If WithPT = 1 and WithFM = 1 Then
               f_num = 10
            Else
               f_num = 14
            Endif
            goto done
         End If

         If f_num = 16 then
           ' f_num = 10
           f_num = 106 'for newly added setpoint of flow rate
            goto done
         End If

         If f_num = 23 then
            f_num = 21
            goto done
         End If

         If f_num = 29 then
            f_num = 27
            goto done
         End If

         If f_num = 32 then
            f_num = 30
            goto done
         End If

         If f_num = 35 then
            f_num = 33
            goto done
         End If


         If f_num = 26 then
            f_num = 25
            goto done
         End If

         If Password = 9999 Then
            If f_num = 99 then
               f_num = 0
               goto done
            End If

 '           If f_num = 98 then
 '              f_num = 1
 '             goto done
 '           End If

 '           If f_num = 9 then
            If f_num = 12  then
               f_num = 5
               goto done
            End If

            If f_num = 11 and WithFM = 0 Then
                 f_num = 5
                 goto done
            End If

            If f_num = 9  then
              If WithPT = 1 Then
                  f_num = 11
              Else
                  f_num = 12
              Endif
               goto done
            End If
         else
            If f_num = 99 then
               f_num = 1
               goto done
            End If

            If f_num = 99 then
               f_num = 0
               goto done
            End If

            If f_num = 8 then
               f_num = 5
               goto done
            End If

            If f_num = 10 then
              ' f_num = 12
'               goto done
'            End If

'            If f_num = 12 then
               f_num = 14
               goto done
            End If
         End If

         f_num = f_num + 1
         goto done
      End If

      If Pb_down_arrow = 1 Then
         Pb_down_arrow = 2



         If Password = 1111 then
            If f_num = 97 then
               f_num = 3
               goto done
            end if
            If f_num = 98 then
               f_num = 97
               goto done
            end if
         end if

         If f_num = 98 then
'            f_num = 24
'            goto done
'         End If                     'skip the monitoring group
'         If f_num = 24 then
            f_num = 3
            goto done
         End If

         If WithPT = 1 and WithFM = 1 Then
                 If f_num = 17 then
                            f_num = 20
                            goto done
                 End If
         Elseif WithPT = 1 and WithFM = 0 Then
                  If f_num = 17 then
                            f_num = 18
                            goto done
                 End If
         Elseif WithPT = 0 and WithFM = 1 Then
                  If f_num = 19 then
                            f_num = 20
                            goto done
                 End If
         Endif


         'for newly added setpoint of flow rate
         If f_num = 10 then
            f_num = 108
            goto done
         End If

         'for newly added setpoint of flow rate
         If f_num = 106 then
            f_num = 16
            goto done
         End If

         If f_num = 21 then
            f_num = 23
            goto done
         End If

         If f_num = 27 then
            f_num = 29
            goto done
         End If

         If f_num = 30 then
            f_num = 32
            goto done
         End If

         If f_num = 33 then
            f_num = 35
            goto done
         End If



         If f_num = 25 then
            f_num = 26
            goto done
         End If

         If Password = 9999 Then
            If f_num = 0 then
               f_num = 99
               goto done
            End If

'            If f_num = 98 then
'               f_num = 24
'               goto done
'            End If
'            If f_num = 24 then
'               f_num = 3
'               goto done
'            End If

            If f_num = 5 then
              'f_num = 9
              If WithFM = 1 Then
                  f_num = 12
              Else
                  f_num = 11
              Endif
               goto done
            End If

            If f_num = 11 then
               f_num = 9
               goto done
            End If

         else
            If f_num = 1 then
               f_num = 99
               goto done
            End If

            If f_num = 5 then
               f_num = 8
               goto done
            End If

            If f_num = 14 then
               'f_num = 12
'               goto done
'            End If

'            If f_num = 12 then
               If WithPT = 1 and WithFM = 1 Then
                  f_num = 10
               Else
                  f_num = 108
               Endif

               goto done
            End If
         End If
         f_num = f_num - 1
      End If
   End If

   Done:

      If Pb_enter = 1 Then
         Pb_enter = 2

         If F_num = 1 then
            F_num = 5
            goto Done
         End If

         'the first item after entering operation menu
          If F_num = 2 then
            F_num = 106
            goto Done
         End If

         'If F_num = 2 then
          '  F_num = 10
           ' goto Done
         'End If

         If F_num = 3 then
            If WithPT = 1 Then
                    F_num = 17
            Else
                F_num = 19
            Endif

            goto Done
         End If

         If F_num = 18 then
            F_num = 27
            goto Done
         End If


         If F_num = 19 then
            F_num = 21
            goto Done
         End If

         If F_num = 17 then
            F_num = 33
            goto Done
         End If

         If F_num = 20 then
            F_num = 30
            goto Done
         End If


         If F_num = 24 then
            f_num = 25
            goto Done
         End If

         If f_num = 25 then
            tim(12) = 240'120
            cls
            Lcdat 1 , 5 , "Total Number Of"
            Lcdat 2 , 1 , " Cycles In Past "
            Lcdat 3 , 1 , "    24 Hours    "

            Lcdat 7 , 1 , " To Clear Press"
            Lcdat 8 , 5 , "     Reset     "

            Do
               lcdat 5 , 1 , "                "
               select case total_cycles
                  case 0 to 9: lcdat 5 , 61 , total_cycles
                  case 9 to 99: lcdat 5 , 57 , total_cycles
                  case 100 to 999: lcdat 5 , 53 , total_cycles
                  case 1000 to 9999: lcdat 5 , 49 , total_cycles
                  case 10000 to 99999: lcdat 5 , 45 , total_cycles
                  case 100000 to 999999: lcdat 5 , 41 , total_cycles
                  case 1000000 to 9999999: lcdat 5 , 37 , total_cycles
                  case 10000000 to 99999999: lcdat 5 , 33 , total_cycles
                  case 100000000 to 999999999: lcdat 5 , 29 , total_cycles
                  case else: lcdat 5 , 25 , total_cycles
               end select

               If tim(12) <= 0 or pb_enter = 1 or pb_reset = 1 or pb_dsp_fun = 1 then
                  cls
                  pb_mode = 2
                  pb_up_arrow = 2
                  pb_down_arrow = 2
                  pb_dsp_fun = 2

                  if pb_reset = 1 then

                     do
                        lcdat 1 , 1 , "  This Counter"
                        lcdat 2 , 5 , "Will Be Cleared"
                        lcdat 4 , 1 , "   To Confirm  "
                        lcdat 5 , 5 , "  Press Enter  "
                        lcdat 7 , 5 , "   Otherwise   "
                        lcdat 8 , 5 , " Press DSP/FUN "
                        wait 1
                        if pb_enter = 1 then
                           pb_enter = 2
                           pb_mode = 2
                           pb_up_arrow = 2
                           pb_down_arrow = 2
                           pb_dsp_fun = 2
                           tim(8) = 10 '3600
                           total_cycles = 0
                           start_stops = 0
                           For index_a = 1 To 24
                              hour(index_a) = 0   'set all to zero
                           Next
                           exit do
                        elseif pb_dsp_fun = 1 then
                           pb_enter = 2
                           pb_mode = 2
                           pb_up_arrow = 2
                           pb_down_arrow = 2
                           pb_dsp_fun = 2
                           exit do
                        end if
                     loop
                  end if

                  pb_reset = 2

                  If tim(12) <= 0 or pb_enter = 1 then
                     pb_dsp_fun = 2
                     pb_enter = 2
                     state = 0
                     return
                  end if

                  pb_enter = 2
                  goto done

                  exit do
               end if
               wait 1
            loop
         End If

         If f_num = 26 then
            tim(12) = 240 '120
            cls
            Lcdat 1 , 5 , " Total Running "
            Lcdat 2 , 5 , "    Time Of    "
            Lcdat 3 , 5 , " Drive (Hours) "

            Lcdat 8 , 1 , "  Press Enter  "
            Lcdat 7 , 5 , "    To Exit    "

            Do
               lcdat 5 , 1 , "                "
               select case hours
                  case 0 to 9: lcdat 5 , 61 , hours
                  case 9 to 99: lcdat 5 , 57 , hours
                  case 100 to 999: lcdat 5 , 53 , hours
                  case 1000 to 9999: lcdat 5 , 49 , hours
                  case 10000 to 99999: lcdat 5 , 45 , hours
                  case 100000 to 999999: lcdat 5 , 41 , hours
                  case 1000000 to 9999999: lcdat 5 , 37 , hours
                  case 10000000 to 99999999: lcdat 5 , 33 , hours
                  case 100000000 to 999999999: lcdat 5 , 29 , hours
                  case else: lcdat 5 , 25 , hours
               end select

               If tim(12) <= 0 or pb_enter = 1 or pb_dsp_fun = 1 then
                  cls
                  pb_mode = 2
                  pb_up_arrow = 2
                  pb_down_arrow = 2
                  pb_dsp_fun = 2
                  pb_reset = 2

                  If tim(12) <= 0 or pb_enter = 1 then
                     pb_dsp_fun = 2
                     pb_enter = 2
                     state = 0
                     return
                  end if

                  pb_enter = 2
                  goto done

                  exit do
               end if
               wait 1
            loop
         End If

         Pumpovermenu:

            Title_str = ""
            Func_des1 = ""
            Func_des2 = ""
            Func_des3 = ""

            Select Case F_num
               Case 0 :
                  Title_str = " Reinitialise"
                  Func_des1 = "Do You Want To"
                  Func_des2 = "Re-Setup This"
                  Func_des3 = " Controller?"
                  H = "0000"
               Case 5 :
                  Title_str = "  Acceleration  "
                  Func_des2 = "    Time (s)"
                  H = Str(accel_time)
               Case 6:
                  Title_str = "  Deceleration  "
                  Func_des2 = "    Time (s)"
                  H = Str(decel_time)
               Case 7 :
                  Func_des1 = "  Minimum Speed"
                  Func_des2 = "     (RPM)"
                  H = Str(minimum_speed)
               Case 8 :
                  Func_des1 = "  Maximum Speed"
                  Func_des2 = "     (RPM)"
                  H = Str(maximum_speed)
               Case 9:
                  Title_str = "Motor Current"
                  Func_des2 = "     (A)     "
                  H = Str(motor_current)
                  F_num = 101
               Case 10 :
                  Func_des1 = "     Switch     "
                  Func_des2 = "    Pressure    "
'                  If user_units = 0 then
'                     Func_des2 = "Pressure(bar)"
'                  else
'                     Func_des2 = "Pressure(psi)"
'                  end if
                  H = Str(Setpoint_1)
               Case 11 :
                  Title_str = "  The Range Of "
                  Func_des1 = "    Pressure   "

                  If user_units = 0 then
                     Func_des2 = " Sensor(bar)    "
                  else
                     Func_des2 = " Sensor(psi)    "
                  end if
                  H = Str(PSensor_Type)
               Case 12 :
                  Title_str = "  The Range Of  "
                  Func_des1 = "    Flowmeter   "
                  Func_des2 = "      (CMH)     "
                  H = Str(FSensor_Type)
               Case 13 :
                  Func_des1 = "  Resume Offset"
                  If user_units = 0 then
                     Func_des2 = "      (bar)    "
                  else
                     Func_des2 = "      (psi)    "
                  end if
                  H = Str(resume_offset)
               Case 14 :
                  Title_str = "    Maximum   "
                  Func_des1 = "    Run Time   "
                  Func_des2 = "    (Hours)    "
                  H = Str(max_hours)
               Case 15 :
                  Func_des1 = "Closeloop Delay"
                  Func_des2 = "    Time(s)    "
                  H = Str(CloseLoopDelayTime)
               Case 16 :
                  Func_des1 = "  Start Delay "
                  Func_des2 = "   Time(s)   "
                  H = Str(resume_time)
               Case 17:
                  Title_str = " Blocked Pipe "
                  Func_des2 = " High Pressure  "
                  'H = Str(low_suction_current) 'todo : change it to blocked pipe
                  'F_num = 105
               Case 18:
                  Title_str = "  Low Pressure  "
                  If user_units = 0 then
                     Func_des1 = " Pressure (bar)"
                  else
                     Func_des1 = " Pressure (psi)"
                  end if
                  H = Str(LS_pressure)
'               Case 20:
'                  Title_str = "  No/Low Water  "
'                  Func_des1 = "  Restart Timer "
'                  Func_des2 = "     (min)      "
'                  H = Str(No_Water_time)

               Case 21:
                  Title_str = "    Run Dry     "
                  Func_des1 = "   Protection   "
                  H = Str(Prime_Loss)
               Case 22:
                  Title_str = " Pump Will Run  "
                  Func_des1 = " Without Prime  "
                  Func_des2 = "    For  (s)    "
                  H = Str(Prime_time)
               Case 23:
                  Title_str = "    Run Dry     "
                  Func_des1 = "   Threshold    "
                  Func_des2 = "     (CMH)      "
                  H = Str(RunDryFlowRate)

               Case 27:
                  Title_str = "  Low Pressure  "
                  Func_des1 = "   Protection   "
                  H = Str(LowPressEnable)
               Case 28:
                  Title_str = "  Low Pressure  "
                  Func_des1 = "   Protection   "
                  Func_des2 = "    Delay (s)   "
                  H = Str(LowPressTime)
               Case 29:
                  Title_str = "  Low Pressure  "
                  Func_des1 = "   Threshold "
                  If user_units = 0 then
                     Func_des2 = "     (bar)      "
                  else
                     Func_des2 = "     (psi)      "
                  end if
                  H = Str(LS_pressure)


               Case 30:
                  Title_str = "   Burst Pipe   "
                  Func_des1 = "   Protection   "
                  H = Str(BurstPipeEnable)
               Case 31:
                  Title_str = "   Burst Pipe   "
                  Func_des1 = "   Protection   "
                  Func_des2 = "   Delay (s)    "
                  H = Str(BurstPipeTime)
               Case 32:
                  Title_str = "   Burst Pipe   "
                  Func_des1 = " Flowrate Limit "
                  Func_des2 = "     (CMH)      "
                  H = Str(BurstPipeFlowRate)

               Case 33:
                  Title_str = " Pressure High  "
                  Func_des1 = "   Protection   "
                  H = Str(BlockedPipeEnable)
               Case 34:
                  Title_str = "  Blocked Pipe  "
                  Func_des1 = "   Protection   "
                  Func_des2 = "   Delay (s)    "
                  H = Str(standby_time)
               Case 35:
                  Title_str = "  Blocked Pipe  "
                  Func_des1 = " Pressure Offset"
                  If user_units = 0 then
                     Func_des2 = "     (bar)      "
                  else
                     Func_des2 = "     (psi)      "
                  end if
                  H = Str(standby_offset)



'               Case 23:
'                  Title_str = " Pump Will Run  "
'                  Func_des1 = " Without Prime  "
'                  Func_des2 = "    For  (s)    "
'                  H = Str(Prime_time)
'               Case 23:
'                  Title_str = "   Number of    "
'                  Func_des1 = "   Start/Stop   "
'                  Func_des2 = "    Cycles      "
'                  H = Str(start_stops)
               Case 106:
                  If  WithFM = 1 Then
                    Func_des1 = "    FlowRate    "
                  Elseif WithPT = 1 Then
                     Func_des1 = "    Pressure    "

'                      If user_units = 0 then
'                         Func_des2 = "     ( bar )    "
'                       Else
'                         Func_des2 = "     ( psi)    "
'                       Endif
                   Endif
                   Func_des2 = "    Setpoint1   "
                   H = Str(FlowRateSetpoint)
                '  Lcdat 1,1 ,H
               Case 107:
                  If  WithFM = 1 Then
                    Func_des1 = "    FlowRate    "
                    Func_des2 = "                "
                  Elseif WithPT = 1 Then
                     Func_des1 = "    Pressure    "

'                      If user_units = 0 then
'                         Func_des2 = "     ( bar )    "
'                       Else
'                         Func_des2 = "     ( psi)    "
'                       Endif
                   Endif
                   Func_des2 = "    Setpoint2   "
                   H = Str(FlowRateSetpoint1)
                '  Lcdat 1,1 ,H
               Case 108:
                  If  WithFM = 1 Then
                    Func_des1 = "    FlowRate    "
                  Elseif WithPT = 1 Then
                    Func_des1 = "    Pressure    "
  '                    If user_units = 0 then
'                         Func_des2 = "     ( bar )    "
'                       Else
'                         Func_des2 = "     ( psi)    "
'                       Endif
                   Endif
                   Func_des2 = "    Setpoint3   "
                   H = Str(FlowRateSetpoint2)
                ' Lcdat 1,1 ,Str(ic13)

               Case 97:
                  Title_str = "Pressure Will Be"
                  Func_des1 = "  Measured In  "
                  Func_des2 = "               "
                  H = Str(user_units)
               Case 98 :
                  State = 4
                  Cls
                  Exit Sub
               Case 99 :
                  Title_str = "   Settings"
                  Func_des1 = "  Protection"
                  Func_des2 = "   Password"
                  H = Str(password)
            End Select
            Call Format_value()
            State = 3
            Cls
      End If

      If Pb_dsp_fun = 1 Then
         Pb_mode = 2
         Pb_dsp_fun = 2

         If configured = 20 then

            if F_num = 27 or F_num = 28 or F_num = 29 then
               F_num = 18
               State = 2
               goto Done
            End If


            if F_num = 21 or F_num = 22 or F_num = 23 then
               F_num = 19
               State = 2
               goto Done
            End If

            if F_num = 30 or F_num = 31 or F_num = 32 then
               F_num = 20
               State = 2
               goto Done
            End If


            if F_num = 33 or F_num = 34 or F_num = 35 then
               F_num = 17
               State = 2
               goto Done
            End If


            if F_num = 17 or F_num = 18 or F_num = 19 or F_num = 20 then
               F_num = 3
               State = 2
               goto Done
            End If

            if F_num = 13 or F_num = 14 or F_num = 15 or F_num = 16 or F_num = 106 or F_num = 107 or F_num = 108 or F_num = 10 then
               F_num = 2
               State = 2
               goto Done
            End If

            if F_num = 5 or F_num = 6 or F_num = 7 or F_num = 8 or F_num = 9 or F_num = 11 or F_num = 12 then
               F_num = 1
               State = 2
               goto Done
            End If

            if f_num = 25 or F_num = 26 then
               F_num = 24
               State = 2
               goto Done
            End If

            State = 0

            Cls
         else
            skip = 1
            Return
         End If
      End If
      Waitms 100
   Return
End Sub
'===============================================================================
Sub Format_value
   Value = Val(h)
   Select Case Len(h)
      Case 4 : H = Str(value)
      Case 3 : H = "0" + Str(value)
      Case 2 : H = "00" + Str(value)
      Case 1 : H = "000" + Str(value)
   End Select
   D1 = Mid(h , 1 , 1)
   D2 = Mid(h , 2 , 1)
   D3 = Mid(h , 3 , 1)
   D4 = Mid(h , 4 , 1)
   Return
End Sub
'===============================================================================
Sub Sub_03
   Lcdat 1 , 1 , Title_str
   Lcdat 2 , 1 , Func_des1
   Lcdat 3 , 1 , Func_des2
   Lcdat 4 , 1 , Func_des3

   If F_num = 0 Or F_num = 21 Or F_num = 27 Or F_num = 30 Or F_num = 33 Or f_num = 97 or f_num = 11 or f_num = 12 Then
      Posi = 4

      If F_num = 0 Then
         If Value = 1 Then
            Lcdat 5 , 1 , "      Yes"
         Else
            Lcdat 5 , 1 , "      No "
         End If
      End If

      If F_num = 21 or F_num = 27 or F_num = 30 Then
         If Value = 1 Then
            Lcdat 5 , 1 , "    ENABLE    "
         Else
            Lcdat 5 , 1 , "    DISABLE   "
         End If
      End If

      If F_num = 33 Then
         If Value = 1 Then
            Lcdat 5 , 1 , " Enable Stop  "
         Elseif Value = 2 Then
            Lcdat 5 , 1 , "Enable Standby"
         Else
            Lcdat 5 , 1 , "    DISABLE   "
         End If
      End If


      If F_num = 97 Then
         If Value = 0 Then
            Lcdat 5 , 5 , "      bar      "
         Elseif Value = 1 then
            Lcdat 5 , 5 , "      psi      "
         End If
      End If

      If F_num = 11 Then
         If Value = 0 Then
'           sensor_size = 1000
            Lcdat 5 , 1 , "   0 - 10 bar  "
         Elseif Value =  1 Then
'           sensor_size = 500
            Lcdat 5 , 1 , "   0 - 14 bar  "
         Else
'           sensor_size = 500
            Lcdat 5 , 1 , "   0 - 20 bar  "
         End If
      Endif

      If f_num = 12 Then
         If Value = 0 Then
            Lcdat 5 , 1 , "   4 - 60 CMH  "
         Elseif Value = 1 Then
            Lcdat 5 , 1 , "   6 - 120 CMH "
         Elseif Value = 2 Then
            Lcdat 5 , 1 , "  10 - 160 CMH "
         Elseif Value = 3 Then
            Lcdat 5 , 1 , "  16 - 250 CMH "
         Elseif Value = 4 Then
            Lcdat 5 , 1 , "  25 - 400 CMH "
         Else
            Lcdat 5 , 1 , "  40 - 600 CMH "
         End If
      Endif


   Else
      Lcdat 5 , 1 , "              "

      if f_num = 10  or f_num = 13 or f_num = 18  then
         If user_units = 0 then
            Lcdat 5 , 1 , "    " ; D1 ; " " ; D2 ; " " ; D3 ; " " ; D4 ; " bar"
         else
            Lcdat 5 , 1 , "    " ; D1 ; " " ; D2 ; " " ; D3 ; " " ; D4 ; " psi"
         end if
      elseif f_num = 15 or f_num = 16 then
         Lcdat 5 , 1 , "    " ; D1 ; " " ; D2 ; " " ; D3 ; " " ; D4 ;
      elseif f_num = 106 or f_num = 107 or f_num = 108 then
         If  WithFM = 1 Then
                     Lcdat 5 , 1 , "    " ; D1 ; " " ; D2 ; " " ; D3 ; " " ; D4 ; "CMH"
         Else
                     If user_units = 0 then
                           Lcdat 5 , 1 , "    " ; D1 ; " " ; D2 ; " " ; D3 ; " " ; D4 ; " bar"
                     else
                           Lcdat 5 , 1 , "    " ; D1 ; " " ; D2 ; " " ; D3 ; " " ; D4 ; " psi"
                     end if
         Endif
      else
         Lcdat 5 , 1 , "    " ; D1 ; " " ; D2 ; " " ; D3 ; " " ; D4 ; "    "
      end If

      If Posi < 1 Then Posi = 4
      Select Case Posi
         Case 4 : Lcdat 6 , 1 , "          -"
         Case 3 : Lcdat 6 , 1 , "        -  "
         Case 2 : Lcdat 6 , 1 , "      -    "
         Case 1 : Lcdat 6 , 1 , "    -      "
      End Select
   End If
   Lcdat 7 , 1 , "  ^/v Change "
   Lcdat 8 , 1 , "  Press Enter"

   Select Case F_num
      ' reconfigure
      Case 0:
         Max_val = 1
         Min_val = 0
      ' accel and decel time
      Case 5 To 6 :
         Lcdat 5 , 74 , "."
         Max_val = 1200
         Min_val = 10
      'Minimum speed setting
      Case 7 :
         Max_val = Maximum_speed
         Min_val = 0
      'Maximum speed setting (RPM)
      Case 8:
         Max_val = 2 * Motor_rpm
         Min_val = 0
      'Setpoint 1 & 2
      Case 10:
         If user_units = 1 then
                If Psensor_type = 0 Then
                            max_val = 1450
                Elseif Psensor_type = 1 Then
                        max_val = 2030
                Else
                        max_val = 2900
                Endif
            Lcdat 5 , 73 , "."
            Min_val = 20
         else
                   If Psensor_type = 0 Then
                            max_val = 1000
                Elseif Psensor_type = 1 Then
                        max_val = 1400
                Else
                        max_val = 2000
                Endif
            Lcdat 5 , 57 , "."
            Min_val = 10
         end if
      'Standby and resume offsets
      Case 11:
            Max_val = 2
            Min_val = 0
      Case 12:
            Max_val = 5
            Min_val = 0
      Case 13:
         if user_units = 1 then
            Lcdat 5 , 73 , "."
            Max_val = 280
            Min_val = 20
         else
            Lcdat 5 , 57 , "."
            Max_val = 200
            Min_val = 20
         end if                     '
      ' max running time
      Case 14:
         Lcdat 5 , 74 , "."
         Max_val = 60
         Min_val = 0
     'Standby and Resume times
      Case 15:
         Max_val = 120
         Min_val = 5
      Case 16:
         Max_val = 120
         Min_val = 1
      ' low sunction current
      Case 17:
         Lcdat 5 , 57 , "."
         Max_val = 200
         Min_val = 0
         f_num = 105
      ' low suction Pressure
      Case 18:
         if user_units = 1 then
            Lcdat 5 , 73 , "."
            Max_val = 700
            Min_val = 0
         else
            Lcdat 5 , 57 , "."
            Max_val = 500
            Min_val = 0
         end if
      ' No water restart timer
'      Case 20:
'         Max_val = 60
'         Min_val = 2
      ' Run Dry Enable
      Case 21:
         Max_val = 1
         Min_val = 0
      'sensor Run Dry timer
      Case 22:
         Max_val = 120
         Min_val = 5
      'Run Dry Threshold of Flow Rate
      Case 23:
         Lcdat 5 , 74 , "."
         Max_val = Fsensor_bias + 100
         Min_val = Fsensor_bias

      'Low Pressure Protection Enable/disable
      Case 27:
         Max_val = 1
         Min_val = 0
      'Low Pressure Protection triggering delay
      Case 28:
         Max_val = 120
         Min_val = 5
      'Low Pressure Protection Threshold
      Case 29:
         if user_units = 1 then
            Lcdat 5 , 73 , "."
            Max_val = 700
            Min_val = 0
         else
            Lcdat 5 , 57 , "."
            Max_val = 500
            Min_val = 0
         end if

      'Burst Pipe Protection Enable/disable
      Case 30:
         Max_val = 1
         Min_val = 0
      'Burst Pipe Protection triggering delay
      Case 31:
         Max_val = 120
         Min_val = 5
      'Burst Pipe Protection Flow Rate Threshold
      Case 32:
         Lcdat 5 , 74 , "."
         Max_val = BurstPipeFRlimit
         Min_val = Fsensor_bias

      'Blocked Pipe Protection Enable/disable
      Case 33:
         Max_val = 2        '0 disable 1 stop enable 2 standby enable
         Min_val = 0
      'Blocked Pipe Protection Triggering Delay
      Case 34:
         Max_val = 120
         Min_val = 5
      'Blocked Pipe Protection Pressure Threshold
      Case 35:
         if user_units = 1 then
            Lcdat 5 , 73 , "."
            Max_val = 500
            Min_val = 5
         else
            Lcdat 5 , 57 , "."
            Max_val = 725
            Min_val = 7
         end if

      'Flow switch protection
'      Case 23:
'         Max_val = 1
'         Min_val = 0
      'User Units

      Case 97:
         Max_val = 1
         Min_val = 0
   '=========================
   'Case 98 is Fault History
   '=========================

         'Password control
      Case 99:
         Max_val = 9999
         Min_val = 0

      'The following parameters are used when reconfiguring controllers
      'Motor voltage(V)
      Case 100:
         Lcdat 5 , 74 , "."
         If Value < 3000 Then
            Max_val = 2640
            Min_val = 1850
         Else
            Max_val = 5280
            Min_val = 3230
         End If
   'Motor Current(A)
      Case 101:
         Lcdat 5 , 74 , "."
         Max_val = 200
         Min_val = 3
   'Motor Power(kW)
      Case 102:
         Lcdat 5 , 57 , "."
         Max_val = 9999
         Min_val = 1
   'Motor rated speed (RPM)
      Case 103:
         Max_val = 3500
         Min_val = 400
   'Motor rated frequency (Hz)
      Case 104:
         Max_val = 65
         Min_val = 45
      'run dry Current(A)
      Case 105:
         Lcdat 5 , 74 , "."
         Max_val = 200
         Min_val = 0
      'flow rate setpoint
      Case 106 to 108:
      If WithFM = 1 Then
         Lcdat 5 , 74 , "."
         if Fsensor_type = 0 then
                max_val = 600
         elseif Fsensor_type = 1 then
                max_val = 1200
         elseif Fsensor_type = 2 then
                max_val = 1600
         elseif Fsensor_type = 3 then
                max_val =2500
         elseif Fsensor_type = 4 then
                max_val = 4000
         else
                max_val = 6000
         End If
         Min_val = 005
      Else
         If user_units = 1 then
                    If Psensor_type = 0 Then
                            max_val = 1160
                Elseif Psensor_type = 1 Then
                        max_val = 1624
                Else
                        max_val = 2320
                Endif
            Lcdat 5 , 73 , "."
            Min_val = 20
         else
                   If Psensor_type = 0 then
                        max_val  = 1000
                  Elseif Psensor_type = 1 then
                        max_val = 1400
                Else
                        max_val = 2000
                EndIf
            Lcdat 5 , 57 , "."
            Min_val = 10
         end if
      Endif
   End Select

   If Pb_up_arrow = 1 Then                                  ' Up arrow pressed
      Pb_up_arrow = 2
      If Value = Max_val Then
         Value = Min_val
      Else
         Select Case Posi
            Case 1 : Value = Value + 1000
            Case 2 : Value = Value + 100
            Case 3 : Value = Value + 10
            Case 4 : If F_num = 14 Then Value = Value + 5 Else Value = Value + 1
         End Select
      End If
      H = Str(value)
      Call Format_value()
   End If

   If Pb_down_arrow = 1 Then                                ' Down arrow pressed
      Pb_down_arrow = 2
      If Value = Min_val Then
         Value = Max_val
      Else
         Select Case Posi
            Case 1 : Value = Value - 1000
            Case 2 : Value = Value - 100
            Case 3 : Value = Value - 10
            Case 4 : If F_num = 14 Then Value = Value - 5 Else Value = Value - 1
         End Select
      End If
      H = Str(value)
      Call Format_value()
   End If

'If values are over or under limits
   If Value < Min_val Then
      Value = Min_val
      H = Str(value)
      Call Format_value()
   End If

   If Value > Max_val Then
      Value = Max_val
      H = Str(value)
      Call Format_value()
   End If

' If shift key pressed
   If Pb_reset = 1 Then
      Pb_reset = 2
      If Posi = 1 Then
         Posi = 4
      Else
         Posi = Posi - 1
      End If
   End If

  'If enter pressed
   If Pb_enter = 1 Then
      Cls
      Pb_enter = 2
      Temp = 0
      Temp = Val(h)

   #IF TEST
      LCDAT 1 , 1 , "h: "; h
      LCDAT 2, 1, "Temp: "; Str(Temp)
      pb_enter = 2
      do
         wait 1
         if pb_enter = 1 then
            pb_enter = 2
            exit do
         end if
      loop
  #ENDIF

      Select Case F_num
         Case 0 :
            Reset_c = Temp
            If Reset_c = 1 Then
               Temp = 0
               Lcdat 1 , 1 , "   WARNING!"
               Lcdat 2 , 1 , "This Will Clear"
               Lcdat 3 , 1 , "All Setup Values"
               Lcdat 4 , 1 , "  ARE YOU SURE?"
               Lcdat 7 , 1 , "  ^/v Change "
               Lcdat 8 , 1 , "  Press Enter"
               Do
                  If Temp = 0 Then
                     Lcdat 5 , 1 , "      No "
                  Else
                     Lcdat 5 , 1 , "      Yes"
                  End If
                  If Pb_down_arrow = 1 Or Pb_up_arrow = 1 Then       ' Up or Down arrow pressed
                     Pb_down_arrow = 2
                     Pb_up_arrow = 2
                     If Temp = 0 Then Temp = 1 Else Temp = 0
                  End If
                  If Pb_enter = 1 Then
                     Pb_enter = 2
                     Exit Do
                  End If
                  Waitms 100
               Loop
               If Temp = 1 Then
                  Cls
                  Writeeeprom Configured_as , 0
                  waitms 100
                  operation_mode = 0
                  writeeeprom operation_mode , 48
                  Lcdat 3 , 5 , "  Cycle Power  "
                  Lcdat 5 , 5 , "  To Complete  "
                  Lcdat 7 , 5 , "     Reset     "
                  Do
                     Waitms 100
                  Loop
               End If
            End If
         Case 5 :
            Accel_time = Temp
            Writeeeprom Accel_time , 15
            Function_number = accel1
            Function_value = Hex(accel_time)
         Case 6 :
            Decel_time = Temp
            Writeeeprom Decel_time , 17
            Function_number = decel1
            Function_value = hex(Decel_time)
         Case 7 :

            Minimum_speed = Temp
            min_speed_temp = minimum_speed  + 50 ' the threshold to detect whether the pump is running in the lowest speed
            Writeeeprom Minimum_speed , 13
            Writeeeprom Min_speed_temp , 30
            Temp_l = Temp * 100
            Temp_l = Temp_l / Maximum_speed

            Temp = Temp_l
            if drive <> 2 then temp = temp * 10

           '     Call MyAssert(temp)
            Function_number = Ain_bias
            Function_value = hex(temp)
            call build_write_string()

            if drive <> 2 then
               function_number = Ain_gain
               temp = 1000 - temp           ' 1000 means 100.0%   gain = 100% - bias for correct scaling
            '    Call MyAssert(temp)

               Function_value = hex(temp)
               call build_write_string()

               'to use the pid control of flowrate feedback
               If Drive = 3 Then
                  Temp_l = Minimum_speed * Motor_hz
               Elseif Drive = 5 Then
                   Temp_l =  Minimum_speed * 100
               Endif
               Temp_l = Temp_l / Motor_rpm
               Temp = Temp_l  * 10
               'Call MyAssert(temp)

               function_number =  DownHz
               Function_value = hex(temp)
               call build_write_string()

            end if

         Case 8 :
            If Rly1 = 0 Then
               Maximum_speed = Temp

               Temp = Minimum_speed
               Temp_l = Temp * 100
               Temp_l = Temp_l / Maximum_speed
               Temp = Temp_l
               if drive <> 2 then temp = temp * 10

               Function_number = Ain_bias
               Function_value = hex(temp)
               call build_write_string()

               if drive <> 2 then
                  function_number = Ain_gain
                  temp = 1000 - temp           ' 1000 means 100.0%   gain = 100% - bias for correct scaling
                  Function_value = hex(temp)
                  call build_write_string()
               end if

               Waitms 500

               Writeeeprom Maximum_speed , 25

               Temp_l = Maximum_speed * Motor_hz
               Temp_l = Temp_l / Motor_rpm
               Temp = Temp_l

               function_value = hex(temp)
               If temp >= 500 Then
                if drive = 3 then
                        Function_number = "0102"
                Elseif drive = 5 then
                        Function_number = "0102"
                Endif
                       Function_value = hex(temp)
                Call build_write_string()
               end if

               Function_number = upper_hz
               temp = temp * 10
               function_value = hex(temp)
               call build_write_string()

            Else
               Cls
               Lcdat 2 , 1 , " Cannot Change  "
               Lcdat 4 , 1 , "  This Value    "
               Lcdat 6 , 1 , " While Running  "
               Wait 3
               Goto End_setting
            End If

         Case 10 :
            Setpoint_1 = temp
            Writeeeprom setpoint_1 , 39
         'deal with the changing of pressure transducor ranges 41
         Case 11 :
            Psensor_type = Temp
            Writeeeprom Psensor_type , 41
            If Psensor_type = 0 then
               If  user_units = 1 Then
                  sensor_size = 1450
               Else
                  sensor_size = 1000
               Endif
               sensor_trim = 995

            Elseif Psensor_type = 1 then
               If  user_units = 1 Then
                  sensor_size = 2030
               Else
                  sensor_size = 1400
               Endif
               sensor_trim = 995

            Else
               If  user_units = 1 Then
                  sensor_size = 2900
               Else
                  sensor_size = 2000
               Endif
               sensor_trim = 995

            End If
         'deal with the changing of flowmeter ranges 42
         Case 12 :
            Fsensor_type = Temp
            Writeeeprom Fsensor_type , 42
            if Fsensor_type = 0 then
                    'Fsensor_size = 56
                    Fsensor_size = 60
                    Fsensor_bias = 40
                    BurstPipeFRlimit = 600
            elseif Fsensor_type = 1 then
                    'Fsensor_size = 114
                    Fsensor_size = 120
                    Fsensor_bias = 60
                    BurstPipeFRlimit  = 1200
            elseif Fsensor_type = 2 then
                    'Fsensor_size = 150
                    Fsensor_size = 160
                    Fsensor_bias = 100
                    BurstPipeFRlimit = 1600
            elseif Fsensor_type = 3 then
                    'Fsensor_size = 234
                    Fsensor_size = 250
                    Fsensor_bias = 160
                    BurstPipeFRlimit = 2500
            elseif Fsensor_type = 4 then
                    'Fsensor_size = 375
                    Fsensor_size = 400
                    Fsensor_bias = 250
                    BurstPipeFRlimit = 4000
            else
                    'Fsensor_size = 560
                    Fsensor_size = 600
                    Fsensor_bias = 400
                    BurstPipeFRlimit = 6000
            End If

         Fsensor_bias = 0

         Case 13 :
            resume_offset = temp
            Writeeeprom resume_offset , 45
         Case 14 :
            Max_hours = temp
            Writeeeprom Max_hours , 47
         Case 15 :
            CloseLoopDelayTime = temp
            Writeeeprom CloseLoopDelayTime , 32
         Case 16 :
            resume_time = temp
            Writeeeprom resume_time , 34
         Case 18 :
            LS_pressure = temp
            Writeeeprom LS_pressure , 53
'         Case 20 :
'            no_water_time = temp
'            Writeeeprom no_water_time , 29
         'liquid sensor eable
         Case 21 :
            Prime_loss = Temp
            Writeeeprom Prime_loss , 37
           'loss of prime time
         Case 22 :
            Prime_time = Temp
            Writeeeprom Prime_time , 36
            'run dry flowrate threshold time
         Case 23 :
            RunDryFlowRate = Temp
            Writeeeprom RunDryFlowRate , 65 '65,66 are used for RunDryFlowRate, 64 is used for standby_time

         Case 27 :
            LowPressEnable = Temp
            Writeeeprom Prime_time ,72
         Case 28 :
            LowPressTime = Temp
            Writeeeprom Prime_time ,73
         Case 29 :
            LS_pressure = temp
            Writeeeprom LS_pressure , 53

         'enable/disable burstpipe protection
         Case 30 :
            BurstPipeEnable = Temp
            Writeeeprom BurstPipeEnable , 67
         'burstpipe protection time
         Case 31 :
            BurstPipeTime = Temp
            Writeeeprom  BurstPipeTime  , 68
         'burstpipe protection flowrate threshold
         Case 32 :
            BurstPipeFlowRate = Temp
            Writeeeprom BurstPipeFlowRate , 69 '69,70 are used for BurstPipeFlowRate
         Case 33 :
            BlockedPipeEnable = Temp
            Writeeeprom BlockedPipeEnable , 71
         Case 34:
            standby_time = temp
            Writeeeprom standby_time , 64
         Case 35:
            Call MyAssert(temp)
            standby_offset = temp
            Writeeeprom standby_offset , 43


         Case 97 :
            If user_units <> temp then
               cls
               lcdat 4 , 5 , "  Please Wait  "

               For B = 1 To 120
                  Lcdat 6 , B , "."
                  Waitms 100
                  B = B + 7
               Next B

               If user_units = 0 then   'its 0 before changing to 1 and writing to eeprom at end of this case
                  units_str = "psi"
                  sensor_size = 1450



'                  temp_dword = setpoint_2 * 1450
'                  temp_dword = temp_dword / 1000         'convert all to psi
'                  setpoint_2 = temp_dword
'                  Writeeeprom setpoint_2 , 41           '1 bar = 14.504 psi

                  If WithPT = 1 Then
                     If   WithFM = 1 Then
                       temp_dword = setpoint_1 * 1450
                       temp_dword = temp_dword / 1000
                       setpoint_1 = temp_dword
                       Writeeeprom setpoint_1 , 39
                     Else
                        temp_dword = FlowRateSetpoint * 1450
                        temp_dword = temp_dword / 1000
                        FlowRateSetpoint = temp_dword
                        Writeeeprom FlowRateSetpoint ,    57

                        temp_dword = FlowRateSetpoint1 * 1450
                        temp_dword = temp_dword / 1000
                        FlowRateSetpoint1 = temp_dword
                        Writeeeprom FlowRateSetpoint1 ,   59

                        temp_dword = FlowRateSetpoint2 * 1450
                        temp_dword = temp_dword / 1000
                        FlowRateSetpoint2 = temp_dword
                        Writeeeprom FlowRateSetpoint2 ,    61
                      Endif
                  Endif

                  temp_dword = standby_offset * 1450
                  temp_dword = temp_dword / 1000
                  standby_offset = temp_dword
                  Writeeeprom standby_offset , 43

                  temp_dword = resume_offset * 1450
                  temp_dword = temp_dword / 1000
                  resume_offset = temp_dword
                  Writeeeprom resume_offset , 45

                  temp_dword = LS_pressure * 1450
                  temp_dword = temp_dword / 1000
                  LS_pressure = temp_dword
                  Writeeeprom LS_pressure , 53

               else
                  units_str = "bar"
                  sensor_size = 1000

                  temp_dword = setpoint_1 * 1000
                  setpoint_1 = temp_dword / 1450         'convert all to bar
                  setpoint_1 = temp_dword
                  Writeeeprom setpoint_1 , 39           '1 bar = 14.504 psi

'                  temp_dword = setpoint_2 * 1000
'                  setpoint_2 = temp_dword / 1450
'                  setpoint_2 = temp_dword
'                  Writeeeprom setpoint_2 , 41

                  temp_dword = standby_offset * 1000
                  standby_offset = temp_dword / 1450
                  standby_offset = temp_dword
                  Writeeeprom standby_offset , 43

                  temp_dword = resume_offset * 1000
                  resume_offset = temp_dword / 1450
                  resume_offset = temp_dword
                  Writeeeprom resume_offset , 45

                  temp_dword = LS_pressure * 1000
                  LS_pressure = temp_dword / 1450
                  LS_pressure = temp_dword
                  Writeeeprom LS_pressure , 53
               end if
            end if

            user_units = Temp
            Writeeeprom user_units , 56
         Case 99 :
            Password = Temp
            F_num = 99
         Case 100 :
            Motor_volts = Temp
         Case 101 :
            Motor_current = Temp
            set_current = motor_current
            Writeeeprom Motor_current , 24


            Function_number = motor_A
            Temp = motor_current
            F_num = 12

'            Function_value = hex(temp)
'            call build_write_string()

'            Temp = motor_current * 8
'            if drive = 2 then Temp = temp / 10
'            Function_number = NL_motor_A




         Case 102 :
            Function_number = motor_kW
            Temp = Temp / 10
            Motor_power = Temp
            If drive <> 2 then temp = temp * 10

         Case 103 :
            Motor_rpm = Temp
            Writeeeprom Motor_rpm , 11

            Function_number = motor_poles

            if motor_rpm >= 2100 then
               num_of_poles  = "0002"
            elseif 1300 =< motor_rpm and motor_rpm < 2100 then
               num_of_poles  = "0004"
            elseif 900 <= motor_rpm and motor_rpm < 1300 then
               num_of_poles  = "0006"
            elseif motor_rpm < 900 then
               num_of_poles  = "0008"
            else
            end if
            Function_Value = num_of_poles
            call build_write_string()

            Function_number = motorpm
            Function_value = hex(temp)

         Case 104 :
            Function_number = motohz
            Temp = Temp * 10
            Motor_hz = Temp
            Writeeeprom Motor_hz , 19

            'Run Dry current (A)
         Case 105:
            low_suction_current = Temp
            Writeeeprom low_suction_current , 33
            f_num = 17

            'Setpoint of flowrate, temperarily %
         Case 106:
            FlowRateSetpoint =  temp
            Writeeeprom FlowRateSetpoint, 57

'            temp =  temp * 10
'            Function_Number = PID_setpoint
         Case 107:
            FlowRateSetpoint1 =  temp
            Writeeeprom FlowRateSetpoint1, 59
            'temp =  temp * 10
            'Function_Number = PID_setpoint   'do not change the setpoint to vsd immediately, depends on the switch
         Case 108:
            FlowRateSetpoint2 =  temp
            Writeeeprom FlowRateSetpoint2, 61
            'temp =  temp * 10
            'Function_Number = PID_setpoint
      End Select

      If F_num <> 0 And f_num <> 7 and f_num <> 8 And F_num <> 10 And F_num <> 11 and f_num <> 13 And F_num <> 14 And F_num <> 21 And F_num <> 22 And F_num <> 23 And F_num <> 27 And F_num <> 28 And F_num <> 29 And F_num <> 30 And F_num <> 31 And F_num <> 32 And F_num <> 33 And F_num <> 34 And F_num <> 35 Then
         If F_num <> 15 And F_num <> 16 And F_num <> 17 And F_num <> 18 and F_num <> 20 And F_num <> 22 and f_num <> 97 and F_num <> 99 and f_num <> 105 And F_num <> 11 And F_num <> 12 And F_num <> 106 and F_num <> 107 and F_num <> 108 Then
            Function_Value = hex(temp)
            call build_write_string()
         End If
      End If

      'depends on switch position, set different flow rate setpoint to VSD
      If WithFM = 1 Then
          If   F_num = 106  or F_num = 107 or F_num = 108 or F_num = 12 Then
                Function_Number = PID_setpoint
                If Ic13.3 = 1 then
                        temp_l = FlowRateSetpoint1
                elseif Ic13.5 = 1 then
                        temp_l = FlowRateSetpoint2
                else
                        temp_l = FlowRateSetpoint
                Endif

               temp_l = temp_l - Fsensor_bias
               temp_l = temp_l* 1000
               temp_l = temp_l / Fsensor_size
               temp = temp_l

                Function_Value = hex(temp)
                call build_write_string()
                'call myassert(666)
                Waitms 100
          End if
      Elseif WithPT = 1 Then
         If F_num = 106 and Ic13.3 = 0 and Ic13.5 = 0 Then
            Setpoint =  FlowRateSetpoint
            Call MyAssert(555)
         Endif
         If F_num = 107 and Ic13.3 = 1 Then Setpoint =  FlowRateSetpoint1
         If F_num = 108 and Ic13.5 = 1 Then Setpoint =  FlowRateSetpoint2

      Endif

#IF TEST
      LCDAT 1 , 1 , "FlowRateSP ";  Str(FlowRateSetpoint)
      LCDAT 2, 1, "Temp: "; Str(Temp)
      LCDAT 3, 1, "FV: ";  Function_Value
      pb_enter = 2
      do
         wait 1
         if pb_enter = 1 then
            pb_enter = 2
            exit do
         end if
      loop
#ENDIF

      End_setting:
         State = 2
         Cls
   End If

'if this sub has been entered via the 'user function' then go back to Sub0
'when the timer gets to 1 (not 0 as it will be 0 if the sub wasn't entered
'through the user function)
   If Tim(2) = 1 Then
      Tim(2) = 0
      State = 2
   End If

   If Pb_reset = 1 Or Pb_up_arrow = 1 Or Pb_down_arrow = 1 Then
      Tim(5) = 60
   End If

'if dsp/fun entered, make no changes and go back to the f numbers - same operation as the CV
   If Pb_dsp_fun = 1 Then
      Pb_dsp_fun = 2
      If Configured = 20 Then
         If F_num = 101 Then F_num = 9
         If F_num = 105 Then F_num = 17
         State = 2
         cls
      else
         skip = 1
         Return
         Cls
      End If
   End If
   Return
End Sub
'===============================================================================
Sub Sub_04



   'Lcdat 4 , 1 , "                 "



   'If fault_var < 1 or fault_var > 200 Then
   '   Lcdat 2 , 1 , "   Fault Log"
   'Endif

   Lcdat 3 , 1 , "Fault No: " ; Fault_num ;" "

   Select Case Fault_num
      Case 01 : Fault_var = Fault_01
      Case 02 : Fault_var = Fault_02
      Case 03 : Fault_var = Fault_03
      Case 04 : Fault_var = Fault_04
      Case 05 : Fault_var = Fault_05
      Case 06 : Fault_var = Fault_06
      Case 07 : Fault_var = Fault_07
      Case 08 : Fault_var = Fault_08
      Case 09 : Fault_var = Fault_09
      Case 10 : Fault_var = Fault_10
   End Select

   'Lcdat 6 , 1 , "                "
   Lcdat 7 , 1 , "                "
   Lcdat 8 , 1 , "                "

   'if fault_var > 94 and fault_var < 100 then
   '   cls
   'end if

   call display_fault_var()

   If Pb_enter = 1 Or Pb_dsp_fun = 1 Then
      Pb_dsp_fun = 2
      Pb_enter = 2
      Fault_num = 1
      State = 2
      Cls
   End If

   If Pb_up_arrow = 1 Then
      Pb_up_arrow = 2
      If Fault_num = 10 Then
         Fault_num = 1
      Else
         Fault_num = Fault_num + 1
      End If
      Cls
   End If

   If Pb_down_arrow = 1 Then
      Pb_down_arrow = 2
      If Fault_num = 1 Then
         Fault_num = 10
      Else
         Fault_num = Fault_num - 1
      End If
      Cls
   End If

   If Pb_reset = 1 Then
      Pb_reset = 2
      Call Clear_faults
   End If

   Return
End Sub
'===============================================================================
Sub display_fault_var


   If Drive = 2 then
      Select Case Fault_var
         Case 0
            Lcdat 4 , 1 , "                "
            Lcdat 5 , 1 , "                "
            Lcdat 6 , 1 , "No Fault Logged"
         Case 1 :
            Lcdat 6 , 1 , "Drive Overheated"
         Case 2
            Lcdat 6 , 1 , " Overcurrent At"
            Lcdat 7 , 1 , "      Stop"
         Case 3
            Lcdat 6 , 1 , "   Low Supply"
            Lcdat 7 , 5 , "    Voltage"
         Case 4
            Lcdat 6 , 5 , "  Over Supply"
            Lcdat 7 , 5 , "    Voltage"
         '5 not used
         Case 6
            Lcdat 6 , 1 , "    External"
            Lcdat 7 , 5 , "   Base Block"
         Case 7
            Lcdat 6 , 1 , "Drive CTER Error"
         Case 8
            Lcdat 6 , 5 , "   Drive PID"
            Lcdat 7 , 5 , "     Error"
         Case 9
            Lcdat 6 , 1 , "  Drive EEPROM"
            Lcdat 7 , 5 , "     Error"
         Case 10
            Lcdat 6 , 1 , "    Autotune   "
            Lcdat 7 , 5 , "     Error    "
         Case 11 :
            Lcdat 6 , 1 , "      Over"
            Lcdat 7 , 1 , "     Torque"
         Case 12 :
            Lcdat 6 , 1 , "    Inverter"
            Lcdat 7 , 1 , "   Overloaded"
         Case 13
            Lcdat 6 , 5 , "     Motor"
            Lcdat 7 , 1 , "   Overloaded"
         Case 14
            Lcdat 6 , 1 , "   Comms(EFO)"
            Lcdat 7 , 5 , "     Error"
         Case 15
            Lcdat 6 , 1 , "    External"
            Lcdat 7 , 1 , " Emergency Stop"
         Case 16
            Lcdat 6 , 5 , "   Parameter"
            Lcdat 7 , 1 , "  Locked (LOC)"
       '17 is reserved
         Case 18
            Lcdat 6 , 1 , "  Over Current"
            Lcdat 7 , 5 , "  At Constant"
            Lcdat 8 , 5 , "     Speed   "
         Case 19
            Lcdat 6 , 1 , " Overcurrent At"
            Lcdat 7 , 1 , "  Acceleration"
         Case 20
            Lcdat 6 , 1 , " Overcurrent At"
            Lcdat 7 , 1 , "  Deceleration"
         Case 21
            Lcdat 6 , 5 , "  Overcurrent "
            Lcdat 7 , 1 , "    At Start"
        '22 is reserved
         Case 23
            Lcdat 6 , 5 , "  Low Voltage"
            Lcdat 7 , 5 , " While Running"
         Case 24
            Lcdat 6 , 1 , "  Over Voltage"
            Lcdat 7 , 5 , "  At Constant"
            Lcdat 8 , 5 , "     Speed   "
         case 25
            Lcdat 6 , 1 , " Drive Heatsink"
            Lcdat 7 , 1 , "   Overheated"
         Case 26
            Lcdat 6 , 1 , "   Zero Speed"
            Lcdat 7 , 5 , "   Stop STP0"
         Case 27
            Lcdat 6 , 5 , " Fail to Start"
            Lcdat 7 , 5 , " Directly STP1"
         Case 28
            Lcdat 6 , 1 , "     Keypad"
            Lcdat 7 , 1 , " Emergency Stop"
         Case 29
            Lcdat 6 , 1 , " Disallowed Use"
            Lcdat 7 , 5 , "Of Drive Keypad"
         Case 30
            Lcdat 6 , 5 , "   Parameter"
            Lcdat 7 , 5 , " Setting Error"
         Case 31
            Lcdat 6 , 1 , "Illegal Interupt"
            Lcdat 7 , 5 , "  Of Drive CPU"
         Case 32
            Lcdat 6 , 1 , " Communications"
            Lcdat 7 , 5 , "     Error"
         Case 33
            Lcdat 6 , 1 , " Communications"
            Lcdat 7 , 5 , "     Error"
         Case 34
            Lcdat 6 , 5 , "   Parameter"
            Lcdat 7 , 5 , " Setting Error"
         Case 35
            Lcdat 6 , 5 , "      Err8"
         Case 38
            Lcdat 6 , 1 , "     EPR1     "
            Lcdat 7 , 1 , "              "
         Case 39
            Lcdat 6 , 1 , "     EPR2     "
            Lcdat 7 , 1 , "              "
         Case 40
            Lcdat 6 , 5 , "     Drive "
            Lcdat 7 , 1 , "   Over Speed"
         Case 41
            Lcdat 6 , 5 , "  Input Phase"
            Lcdat 7 , 5 , "      Loss"
         Case 42
            Lcdat 6 , 5 , " Under Current"
            Lcdat 7 , 5 , "   Detection"
         Case 43
            Lcdat 6 , 1 , "  Output phase"
            Lcdat 7 , 5 , "      Loss"
         Case 44
            Lcdat 6 , 1 , "     Ground    "
            Lcdat 7 , 1 , "      Fault   "
         Case 45
            Lcdat 6 , 5 , "  Power Relay"
            Lcdat 7 , 5 , "      Off"
         Case 46
            Lcdat 6 , 5 , "   Fire Mode"
            Lcdat 7 , 1 , "     Active"
  Case 95:
            Lcdat 1 , 1 , "   Low Suction "
            Lcdat 2 , 5 , "    Pressure   "
            Lcdat 4 , 1 , "1. Check Inlet  "
            Lcdat 5 , 1 , "2. Check Wiring "
'            Lcdat 6 , 5 , "  Press RESET  "
'            Lcdat 7 , 5 , "  To Continue  "
         Case 96:
            Lcdat 1 , 5 , "  Low Suction  "
            Lcdat 2 , 1 , "   (Current)   "
            Lcdat 3 , 5 , "  Check Water "
            Lcdat 4 , 5 , "   Supply To "
            Lcdat 5 , 1 , "    The Pump "
'            Lcdat 7 , 5 , "  Press RESET  "
'            Lcdat 8 , 5 , "  To Continue  "
         Case 97:
            Lcdat 1 , 1 , " Ext Interlock "
            Lcdat 2 , 1 , "    Tripped    "
            Lcdat 4 , 1 , "Check Tank Level"
            Lcdat 5 , 1 , "  Check Wiring "
'            Lcdat 7 , 1 , "  Press RESET  "
'            Lcdat 8 , 1 , "  To Continue  "
         Case 98:
            Lcdat 1 , 1 , "    Run Dry   "
            Lcdat 2 , 1 , "   Protection  "
            Lcdat 4 , 1 , "1. Check Priming"
            Lcdat 5 , 1 , "2. Check Wiring "
'            Lcdat 7 , 1 , "   Press RESET  "
'            Lcdat 8 , 1 , "   To Continue  "

         Case 99:
            Lcdat 2 , 1 , "   Transducer  "
            Lcdat 3 , 1 , "   Signal Lost "
            Lcdat 5 , 1 , "  Check Wiring "
            Lcdat 6 , 1 , "  To Transducer"
 #IF PT_PressureHigh

        case 100:
            Lcdat 1 , 1 , "  Blocked Pipe  "
            Lcdat 2 , 1 , "   Protection  "
            Lcdat 4 , 1 , "1. Check Block"
            Lcdat 5 , 1 , "2. Check Wiring "
#ENDIF

#IF FM_BurstPipe
       case 101:
            Lcdat 1 , 1 , "   Burst Pipe  "
            Lcdat 2 , 1 , "   Protection  "
            Lcdat 4 , 1 , "1. Check Pipe"
            Lcdat 5 , 1 , "2. Check Wiring "
#ENDIF

         Case 255
            Lcdat 4 , 1 , "                "
            Lcdat 5 , 1 , "                "
            Lcdat 6 , 1 , "No Fault Logged"
         Case Else
            Lcdat 6 , 1 , "Error Number"
            Lcdat 7 , 50 , Error_reg
      End Select
   elseif drive = 5 then
      Select Case Fault_var
         Case 0 :
           Lcdat 4 , 1 , "                "
           Lcdat 5 , 1 , "                "
           Lcdat 6 , 1 , "No Fault Logged"
         Case 1 :
            Lcdat 6 , 1 , "  DC Bus Under"
            Lcdat 7 , 1 , "  Voltage (UV)"
         Case 2 :
            Lcdat 6 , 1 , "    Inverter   "
            Lcdat 7 , 1 , "  Over Current "
         Case 3 :
            Lcdat 6 , 5 , "  DC Bus Over"
            Lcdat 7 , 1 , "  Voltage (OV)"
         Case 4 :
            Lcdat 6 , 1 , "   Heatsink   "
            Lcdat 7 , 1 , "   Overheat  "
         Case 5 :
            Lcdat 6 , 1 , "Motor Overload"
         Case 6 :
            Lcdat 6 , 5 , "   Inverter   "
            Lcdat 7 , 1 , " Overload (OL2)"
         Case 7
            Lcdat 6 , 1 , "     Over    "
            Lcdat 7 , 1 , "    Torque    "
         Case 8 :
            Lcdat 6 , 1 , "     Under    "
            Lcdat 7 , 1 , "    Torque    "
         Case 9 :
            Lcdat 6 , 1 , "Drive Output "
            Lcdat 7 , 1 , "Short Circuit"
         Case 10 :
            Lcdat 6 , 1 , "Ground Fault"
            Lcdat 7 , 1 ,  "           "
         Case 11 :
            Lcdat 6 , 1 , "Fuse Broken"
            Lcdat 7 , 1 , "           "
         Case 12 :
            Lcdat 6 , 1 , "Input Phase "
            Lcdat 7 , 1 , "    Loss    "
         Case 13 :
            Lcdat 6 , 1 , "Output Phase "
            Lcdat 7 , 1 , "    Loss    "
         Case 14 :
            Lcdat 6 , 1 , "PG Overspeed"
            Lcdat 7 , 1 ,  "           "
         Case 15 :
            Lcdat 6 , 1 , "   PG Open "
            Lcdat 7 , 1 , "           "
         Case 16 :
            Lcdat 6 , 1 , "  PG Speed "
            Lcdat 7 , 1 , "  Deviation"
         Case 17 to 24 :
            Lcdat 6 , 1 , "External Fault"
            Lcdat 7 , 1 , "             "
         Case 25 :
            Lcdat 6 , 1 , "  Drive PID "
            Lcdat 7 , 5 , " Feedback Loss"
         Case 26 :
            Lcdat 6 , 1 , "     OPR     "
            Lcdat 7 , 1 , "     Error   "
         Case 28 :
            Lcdat 6 , 1 , " Communications "
            Lcdat 7 , 1 , "     Error     "

         Case 29 :
            Lcdat 6 , 1 , "  Safe Torque "
            Lcdat 7 , 1 , "      Off     "
         Case 38 :
            Lcdat 6 , 1 , " Motor Control "
            Lcdat 7 , 1 , "  Fault (CF07) "
         Case 41 :
            Lcdat 6 , 1 , "     OLDOP     "
            Lcdat 7 , 1 , "             "
         Case 47 :
            Lcdat 6 , 1 , "  Stop Command "
            Lcdat 7 , 5 , " Enabled (SS1) "
         Case 48 :
            Lcdat 6 , 1 , "Communication "
            Lcdat 7 , 1 , "  Failure     "
         Case 49 :
            Lcdat 6 , 1 , " Run Switch  "
            Lcdat 7 , 1 , "   Error     "
         Case 50 :
            Lcdat 6 , 1 , " Overcurrent at"
            Lcdat 7 , 1 , "  acceleration"
         Case 51 :
            Lcdat 6 , 1 , " Overcurrent at"
            Lcdat 7 , 1 , "  deceleration"
         Case 52 :
            Lcdat 6 , 1 , " Overcurrent "
            Lcdat 7 , 1 , " at constant"
            Lcdat 8 , 1 , "   speed"
         Case 53 :
            Lcdat 6 , 1 , " Motor Control "
            Lcdat 7 , 1 , "  Fault (CF08) "
 Case 95:
            Lcdat 1 , 1 , "   Low Suction "
            Lcdat 2 , 5 , "    Pressure   "
            Lcdat 4 , 1 , "1. Check Inlet  "
            Lcdat 5 , 1 , "2. Check Wiring "
'            Lcdat 6 , 5 , "  Press RESET  "
'            Lcdat 7 , 5 , "  To Continue  "
         Case 96:
            Lcdat 1 , 5 , "  Low Suction  "
            Lcdat 2 , 1 , "   (Current)   "
            Lcdat 3 , 5 , "  Check Water "
            Lcdat 4 , 5 , "   Supply To "
            Lcdat 5 , 1 , "    The Pump "
'            Lcdat 7 , 5 , "  Press RESET  "
'            Lcdat 8 , 5 , "  To Continue  "
         Case 97:
            Lcdat 1 , 1 , " Ext Interlock "
            Lcdat 2 , 1 , "    Tripped    "
            Lcdat 4 , 1 , "Check Tank Level"
            Lcdat 5 , 1 , "  Check Wiring "
'            Lcdat 7 , 1 , "  Press RESET  "
'            Lcdat 8 , 1 , "  To Continue  "
         Case 98:
            Lcdat 1 , 1 , "    Run Dry   "
            Lcdat 2 , 1 , "   Protection  "
            Lcdat 4 , 1 , "1. Check Priming"
            Lcdat 5 , 1 , "2. Check Wiring "
'            Lcdat 7 , 1 , "   Press RESET  "
'            Lcdat 8 , 1 , "   To Continue  "

         Case 99:
            Lcdat 2 , 1 , "   Transducer  "
            Lcdat 3 , 1 , "   Signal Lost "
            Lcdat 5 , 1 , "  Check Wiring "
            Lcdat 6 , 1 , "  To Transducer"
 #IF PT_PressureHigh

        case 100:
            Lcdat 1 , 1 , "  Blocked Pipe  "
            Lcdat 2 , 1 , "   Protection  "
            Lcdat 4 , 1 , "1. Check Block"
            Lcdat 5 , 1 , "2. Check Wiring "
#ENDIF

#IF FM_BurstPipe
       case 101:
            Lcdat 1 , 1 , "   Burst Pipe  "
            Lcdat 2 , 1 , "   Protection  "
            Lcdat 4 , 1 , "1. Check Pipe"
            Lcdat 5 , 1 , "2. Check Wiring "
#ENDIF
        Case 255 :
            Lcdat 4 , 1 , "                "
            Lcdat 5 , 1 , "                "
            Lcdat 6 , 1 , "No Fault Logged"
         Case Else :
            Lcdat 6 , 1 , "Error Number"
            Lcdat 7 , 50 , Fault_var
      End Select

   else
      Select Case fault_var
         Case 0 :
           Lcdat 4 , 1 , "                "
           Lcdat 5 , 1 , "                "
           Lcdat 6 , 1 , "No Fault Logged"
         Case 1 :
            Lcdat 6 , 1 , "  DC Bus Under"
            Lcdat 7 , 1 , "  Voltage (UV)"
         Case 2 :
            Lcdat 6 , 1 , "    Inverter"
            Lcdat 7 , 1 , "  Over Current"
         Case 3 :
            Lcdat 6 , 5 , "  DC Bus Over"
            Lcdat 7 , 1 , "  Voltage (OV)"
         Case 4 :
            Lcdat 6 , 1 , "Drive Heatsink"
            Lcdat 7 , 1 , " Overheated"
         Case 5 :
            Lcdat 6 , 1 , "     Motor"
            Lcdat 7 , 1 , "   Overloaded"
         Case 6 :
            Lcdat 6 , 1 , "    Inverter"
            Lcdat 7 , 1 , "   Overloaded"
         Case 7 :
            Lcdat 6 , 1 , "     Over"
            Lcdat 7 , 1 , "    Torque"
         Case 9 :
            Lcdat 6 , 1 , "     Short"
            Lcdat 7 , 1 , "    Circuit"
         Case 10 :
            Lcdat 6 , 1 , "     Ground"
            Lcdat 7 , 1 , "  Over Current"
         Case 12 :
            Lcdat 6 , 1 , "     Input"
            Lcdat 7 , 1 , "  Phase Loss"
         Case 13 :
            Lcdat 6 , 1 , "     Output"
            Lcdat 7 , 1 , "  Phase Loss"
         Case 25 :
            Lcdat 6 , 1 , "   Drive PID"
            Lcdat 7 , 1 , " Feedback Error"
         Case 26 :
            Lcdat 6 , 1 , "     Keypad"
            Lcdat 7 , 1 , "    Removed"
         Case 28 :
            Lcdat 6 , 1 , " Communications"
            Lcdat 7 , 1 , "  Error (CE)  "
         Case 29 :
            Lcdat 6 , 1 , "      STO1     "
         Case 38 :
            Lcdat 6 , 1 , " Motor Control"
            Lcdat 7 , 1 , "  Fault (CF07)"
         Case 46 :
            Lcdat 6 , 1 , "Motor Overheated"
            Lcdat 7 , 1 , "(PTC Thermistor)"
         Case 49 :
            Lcdat 6 , 1 , "  MtrSw (run)  "
         Case 50 :
            Lcdat 6 , 1 , " Overcurrent at"
            Lcdat 7 , 1 , " Acceleration"
         Case 51 :
            Lcdat 6 , 1 , " Overcurrent at"
            Lcdat 7 , 1 , " Deceleration"
         Case 52 :
            Lcdat 6 , 1 , " Overcurrent "
            Lcdat 7 , 1 , " at Constant"
            Lcdat 8 , 1 , "   Speed"
         Case 54 :
            Lcdat 6 , 1 , " Motor Control"
            Lcdat 7 , 1 , "  Fault (CF08)"
         Case 55 :
            Lcdat 6 , 1 , "      STO2     "
         Case 56 :
            Lcdat 6 , 1 , "      STO3     "
         Case 57 :
            Lcdat 6 , 1 , " Power Voltage "
            Lcdat 7 , 1 , "    Off (PO)   "
         Case 58 :
            Lcdat 6 , 1 , "Protection Error"
            Lcdat 7 , 1 , "      (PF)     "
         Case 95:
            Lcdat 1 , 1 , "   Low Suction "
            Lcdat 2 , 5 , "    Pressure   "
            Lcdat 4 , 1 , "1. Check Inlet  "
            Lcdat 5 , 1 , "2. Check Wiring "
'            Lcdat 6 , 5 , "  Press RESET  "
'            Lcdat 7 , 5 , "  To Continue  "
         Case 96:
            Lcdat 1 , 5 , "  Low Suction  "
            Lcdat 2 , 1 , "   (Current)   "
            Lcdat 3 , 5 , "  Check Water "
            Lcdat 4 , 5 , "   Supply To "
            Lcdat 5 , 1 , "    The Pump "
'            Lcdat 7 , 5 , "  Press RESET  "
'            Lcdat 8 , 5 , "  To Continue  "
         Case 97:
            Lcdat 1 , 1 , " Ext Interlock "
            Lcdat 2 , 1 , "    Tripped    "
            Lcdat 4 , 1 , "Check Tank Level"
            Lcdat 5 , 1 , "  Check Wiring "
'            Lcdat 7 , 1 , "  Press RESET  "
'            Lcdat 8 , 1 , "  To Continue  "
         Case 98:
            Lcdat 1 , 1 , "    Run Dry   "
            Lcdat 2 , 1 , "   Protection  "
            Lcdat 4 , 1 , "1. Check Priming"
            Lcdat 5 , 1 , "2. Check Wiring "
'            Lcdat 7 , 1 , "   Press RESET  "
'            Lcdat 8 , 1 , "   To Continue  "

         Case 99:
            Lcdat 2 , 1 , "   Transducer  "
            Lcdat 3 , 1 , "   Signal Lost "
            Lcdat 5 , 1 , "  Check Wiring "
            Lcdat 6 , 1 , "  To Transducer"
 #IF PT_PressureHigh

        case 100:
            Lcdat 1 , 1 , "  Blocked Pipe  "
            Lcdat 2 , 1 , "   Protection  "
            Lcdat 4 , 1 , "1. Check Block"
            Lcdat 5 , 1 , "2. Check Wiring "
#ENDIF

#IF FM_BurstPipe
       case 101:
            Lcdat 1 , 1 , "   Burst Pipe  "
            Lcdat 2 , 1 , "   Protection  "
            Lcdat 4 , 1 , "1. Check Pipe"
            Lcdat 5 , 1 , "2. Check Wiring "
#ENDIF

         Case 255 :
            Lcdat 4 , 1 , "                "
            Lcdat 5 , 1 , "                "
            Lcdat 6 , 1 , "No Fault Logged"
         Case Else :
            Lcdat 6 , 1 , "Error Number"
            Lcdat 7 , 50 , Error_reg
      End Select

   End If

   return
end sub
'===============================================================================
Sub Clear_faults
   Cls
   Lcdat 2 , 1 , "To Clear The    "
   Lcdat 3 , 1 , "Fault Log"
   Lcdat 4 , 1 , "Press RESET Or"
   Lcdat 5 , 1 , "Press DSP/FUN To"
   Lcdat 6 , 1 , "Return To The "
   Lcdat 7 , 1 , "Fault Display"

   Do
      If Pb_dsp_fun = 1 Then
         Pb_dsp_fun = 2
         Pb_up_arrow = 2
         Pb_run_stop = 2
         Pb_down_arrow = 2
         Pb_reset = 2
         Pb_enter = 2
         Cls
         Return
      End If
      Waitms 50
   Loop Until Pb_reset = 1

   Writeeeprom Null , 1
   Writeeeprom Null , 2
   Writeeeprom Null , 3
   Writeeeprom Null , 4
   Writeeeprom Null , 5
   Writeeeprom Null , 6
   Writeeeprom Null , 7
   Writeeeprom Null , 8
   Writeeeprom Null , 9
   Writeeeprom Null , 10
   Wait 1
   Readeeprom Fault_01 , 1
   Readeeprom Fault_02 , 2
   Readeeprom Fault_03 , 3
   Readeeprom Fault_04 , 4
   Readeeprom Fault_05 , 5
   Readeeprom Fault_06 , 6
   Readeeprom Fault_07 , 7
   Readeeprom Fault_08 , 8
   Readeeprom Fault_09 , 9
   Readeeprom Fault_10 , 10

   Pb_dsp_fun = 2
   Pb_up_arrow = 2
   Pb_run_stop = 2
   Pb_down_arrow = 2
   Pb_reset = 2
   Pb_enter = 2
   Cls
   Return
End Sub
'==============================================================================

 Sub Write_faults()
   Error_on = 1
   error_clear = 1    ' set a flag that will avoid changing direction when reset is pushed to clear error

   If drive = 2 then
      if error_reg = 26 or error_reg = 3 then
         goto skip_LSV
      end if
   else
      if error_reg = 1 then
         goto skip_LSV
      end if
   end if

   If Error_reg <> Fault_01 Then
      Writeeeprom Error_reg , 1
      Writeeeprom Fault_01 , 2
      Writeeeprom Fault_02 , 3
      Writeeeprom Fault_03 , 4
      Writeeeprom Fault_04 , 5
      Writeeeprom Fault_05 , 6
      Writeeeprom Fault_06 , 7
      Writeeeprom Fault_07 , 8
      Writeeeprom Fault_08 , 9
      Writeeeprom Fault_09 , 10
      Wait 1
      Readeeprom Fault_01 , 1
      Readeeprom Fault_02 , 2
      Readeeprom Fault_03 , 3
      Readeeprom Fault_04 , 4
      Readeeprom Fault_05 , 5
      Readeeprom Fault_06 , 6
      Readeeprom Fault_07 , 7
      Readeeprom Fault_08 , 8
      Readeeprom Fault_09 , 9
      Readeeprom Fault_10 , 10
   End If


   skip_LSV:

      cls
      fault_var = error_reg

      call display_fault_var()

   'wait for a time to give the physical error a chance to clear
   'or the motor a chance to cool down if it has tripped on I2T

      If Error_reg = 95 Then    'low suction - pressure

         Lcdat 7 , 1 , "   Press RESET  "
         Lcdat 8 , 1 , "   To Continue  "
         'reset_timer = 1
         rly1 = 0    'when the error occurs, the vsd is stopped
         Do
            Waitms 100
            If pb_reset = 1 or pb_reset = 1 then
               pb_reset = 2
               cls
               Exit do
            End If
         Loop
'         'reset_timer = 1
'         Do
'            if reset_attempts < 3 then
'               Lcdat 8 , 1 , "                "
'               Lcdat 8 , 5 , "Auto Retry " ; tim(11) ; "s"
'            else
'               cls
'               lcdat 1 , 1 , "  Low Pressure"
'               lcdat 3 , 5 , " 3 Auto Resets"
'               lcdat 4 , 5 , "   Attempted  "
'               lcdat 6 , 1 , "Check Pump Inlet"
'               lcdat 7 , 5 , "  Press RESET"
'               lcdat 8 , 1 , "  To Continue"
'            end if
'            Waitms 100
'         Loop Until Pb_reset = 1
'         'reset_timer = 0
         error_reg = 0
         pb_reset = 2
         first_cycle = 1
         low_pressure = 0
         error_on = 0
         cls
         return
      End If

      If Error_reg = 96 Then    'low suction - current
         Lcdat 7 , 5 , "  Press RESET  "
         Lcdat 8 , 5 , "  To Continue  "
        ' reset_timer = 1
         Do
            Waitms 100
         Loop Until Pb_reset = 1
        ' reset_timer = 0
         error_reg = 0
         pb_reset = 2
         first_cycle = 1
         error_on = 0
         cls
         return
      End If

      If Error_reg = 97 Then    'external interlock
         Lcdat 7 , 1 , "  Press RESET  "
         Lcdat 8 , 1 , "  To Continue  "
         Do
            Waitms 100
         Loop Until Pb_reset = 1 or Ic13.1 = 1
         actual_rpm = "00000" ' avoid restart until rpm value from drive has been read
         Error_reg = 0
         Transducer_fault = 0
         first_cycle = 1
         error_on = 0
         cls
         Return
      End If

      If Error_reg = 98 Then
         Lcdat 7 , 1 , "   Press RESET  "
         Lcdat 8 , 1 , "   To Continue  "
         'reset_timer = 1
         rly1 = 0    'when the error occurs, the vsd is stopped
         Do
            Waitms 100
            If pb_reset = 1 or pb_reset = 1 then
               pb_reset = 2
               cls
               Exit do
            End If
         Loop
         'reset_timer = 0
         Pb_reset = 2
         Error_reg = 0
         RunDry = 0
         soft_stop = 0
         first_cycle = 1
         error_on = 0
         cls
         Return
      End If

      If Error_reg = 99 Then
         'reset_timer = 1
         Do
            Waitms 100
         Loop Until Pb_reset = 1 or anaval_l > 1 or pb_mode = 1
         actual_rpm = "00000" ' avoid restart until rpm value from drive has been read
         'Error_reg = 0
         Transducer_fault = 0
         first_cycle = 1
         error_on = 0
         cls
         Return
      End If

      #IF PT_PressureHigh
        'this is for faulty error "pressure too high"
      If Error_reg = 100 Then
      'Call MyAssert(Error_reg)
         Lcdat 7 , 1 , "   Press RESET  "
         Lcdat 8 , 1 , "   To Continue  "
         'reset_timer = 1
         rly1 = 0    'when the error occurs, the vsd is stopped
         Do
            Waitms 100
            If pb_reset = 1 or pb_reset = 1 then
               pb_reset = 2
               cls
               Exit do
            End If
         Loop
         'reset_timer = 0
         Pb_reset = 2
         Error_reg = 0
         On_standby = 0 'go out of suspending state
         soft_stop = 0
         first_cycle = 1
         error_on = 0
         'Call  MyAssert(tim(7))
         cls
         Return
      End If
      #EndIf

      #IF FM_BurstPipe
       'this is for faulty error "burst pipe"
      If Error_reg = 101 Then
      'Call MyAssert(Error_reg)
         Lcdat 7 , 1 , "   Press RESET  "
         Lcdat 8 , 1 , "   To Continue  "
        'reset_timer = 1
         rly1 = 0    'when the error occurs, the vsd is stopped
         Do
            Waitms 100
            If pb_reset = 1 or pb_reset = 1 then
               pb_reset = 2
               cls
               Exit do
            End If
         Loop
         'reset_timer = 0
         Pb_reset = 2
         Error_reg = 0
         BurstPipe = 0 'go out of suspending state
         soft_stop = 0
         first_cycle = 1
         error_on = 0
         'Call  MyAssert(tim(16))
         cls
         Return
      End If
      #Endif

      first_cycle = 1 ' prevent starting up when pressure is already up

   'wait for a time to give the physical error a chance to clear
   'or the motor a chance to cool down if it has tripped on I2T

      Wait 2


      If Number_of_resets < 3 Then
         Lcdat 2 , 1 , "****  FAULT ****"
         Lcdat 4 , 1 , "RESETTING...."
         Number_of_resets = Number_of_resets + 1
         Wait 10
         Reset_flag = 1
         cls
      else
         if pb_reset = 1 then
            pb_reset = 2
            number_of_resets = 0
         end if
      End If

   Return
End Sub

'===============================================================================

Sub Build_read_string()

   Write_string_reg = "0103" + Function_number + "0001"
   Call Calc_chksm
   'Lcdat  1,1, Write_string_reg
   'Call MyAssert(190)
   Print ":" ; Write_string_reg ; Hex(csum)
   waitms 100

'This block of code waits until there is something in the receive
    'buffer before proceeding
   Passes = 0
   Do
      Waitms 10
      Read_buffer_count = _rs_bufcountr0
      Passes = Passes + 1
      If Passes >= 10 Then Exit Do
   Loop Until Read_buffer_count > 0
   Waitms 10
End Sub

'===============================================================================

Sub Build_write_string()
   Write_string_reg = "0110" + Function_number + "000102" + Function_value
   Call Calc_chksm

#IF TEST
   cls
   lcdat 2 , 1 , "reg : " ; function_number
   lcdat 4 , 1 , "val : " ; function_value

   iF DRIVE = 2 THEN
      LCDAT 8 , 1 , "   E510 "
   ELSEIF DRIVE = 3 THEN
      LCDAT 8 , 1 , "   E510S "
   ELSEIF DRIVE = 5 THEN
      LCDAT 8 , 1 , "   A510S "
   END IF

   LCDAT  7 , 1 , "  DRIVE ; "
   pb_enter = 2

   do
      wait 1
      if pb_enter = 1 then
         pb_enter = 2
         exit do
      end if
   loop
#ENDIF
   Print ":" ; Write_string_reg ; Hex(csum)
   Waitms 100
End Sub

'===============================================================================

Sub Calc_chksm()
' Function for calculating the checksum
' You must provide it with the write string(minus the stx ":" at the beginning
' It will return the checksum value at the end
'Clear the array

   For J = 1 To 10
      Framebytes(j) = 0
   Next J

   Char01 = Mid(write_string_reg , 1 , 2)
   Char02 = Mid(write_string_reg , 3 , 2)
   Char03 = Mid(write_string_reg , 5 , 2)
   Char04 = Mid(write_string_reg , 7 , 2)
   Char05 = Mid(write_string_reg , 9 , 2)
   Char06 = Mid(write_string_reg , 11 , 2)
   Char07 = Mid(write_string_reg , 13 , 2)
   Char08 = Mid(write_string_reg , 15 , 2)
   Char09 = Mid(write_string_reg , 17 , 2)

   'load the transmit string into the array
   Framebytes(1) = Hexval(char01)
   Framebytes(2) = Hexval(char02)
   Framebytes(3) = Hexval(char03)
   Framebytes(4) = Hexval(char04)
   Framebytes(5) = Hexval(char05)
   Framebytes(6) = Hexval(char06)
   Framebytes(7) = Hexval(char07)
   Framebytes(8) = Hexval(char08)
   Framebytes(9) = Hexval(char09)

   Csum = 0

   'total the bytes in the array
   For Bytecount = 1 To 9
      Csum = Csum + Framebytes(bytecount)
   Next Bytecount

   'subtract from 255 and then add 1
   Csum = 255 - Csum
   Csum = Csum + 1

   'return the checksum value
   Return
End Sub

'===============================================================================
'Now the opening screens are displayed
Sub Poweronlogo()
   Cls
   Glcdcmd &H3E , 1 : Glcdcmd &H3E , 2
   'both displays off
   Waitms 500
   Glcdcmd &H3F , 1 : Glcdcmd &H3F , 2
   'both on
   'GLCDCMD accepts an additional param to select the chip
   'With multiple, GLCDCMD statements, it is best to specify the chip only the first time

   'Cls
   'Showpic 0 , 0 , Picdata
      'show a compressed picture
   'Wait 5

   cls

   Lcdat 2 , 1 , " ______________ "
   Lcdat 3 , 1 , "|              |"
   Lcdat 4 , 1 , "| Initialising |"
   Lcdat 5 , 1 , "|______________|"
   Lcdat 6 , 1 , "                "
   Lcdat 7 , 1 , "     v1.0.0     "
   Wait 2
   Cls

   Return
End Sub
'===============================================================================
'Check the internal byte of the controller that the controller has been configured
Sub Sub_01()

   'Call MyAssert(Configured)
   'Call MyAssert(State)

   If Configured = 20 Then
      State = 0
      Initialised = 1
      Running = 0
   Else
      Configured = 1
      State = 6
      Running = 0
      Initialised = 0
      WithFM = 0
      WithPT = 0
   End If

   Cls

End Sub
'===============================================================================
Sub Set_value
   Call Format_value()
   Do
      Call Sub_03
      Call Clear_buff()
      Waitms 100
      If State = 2 or skip = 1 Then Exit Do
   Loop
   State = 1
   Cls
   Title_str = ""
   Func_des1 = ""
   Func_des2 = ""
   Func_des3 = ""
   Pb_enter = 2
   Pb_reset = 2
   Pb_up_arrow = 2
   Pb_down_arrow = 2
   Pb_dsp_fun = 2
   Return
End Sub
'===============================================================================
Sub Sub_06()

   minimum_speed = 0

   'Call MyAssert(State)

   Cls
   Lcdat 2 , 1 , " New Controller"
   Lcdat 3 , 1 , " ______________"
   Lcdat 5 , 1 , "   Press Enter "
   Lcdat 7 , 1 , "  To Configure "

   Do
      If Pb_enter = 1 Then
         Pb_enter = 2
         Exit Do
      End If
      Waitms 100
   Loop

   Pb_reset = 2
   Pb_enter = 2
   Pb_up_arrow = 2
   Pb_down_arrow = 2
   Pb_mode = 2
   Pb_run_stop = 2

   If Resume_point <> 0 Then
      Cls
      Lcdat 1 , 1 , "The Last Time"
      Lcdat 2 , 1 , "That This Setup"
      Lcdat 3 , 1 , "Routine Was Run"
      Lcdat 4 , 1 , "AUTOTUNE Failed"
      Lcdat 5 , 1 , "Please Enter All"
      Lcdat 6 , 1 , "Data Carefully."
      Lcdat 8 , 1 , "PRESS ENTER"

      Do
         If Pb_enter = 1 Then
            Pb_enter = 2
            Exit Do
         End If
         Waitms 100
      Loop
   End If

   Pb_reset = 2
   Pb_enter = 2
   Pb_up_arrow = 2
   Pb_down_arrow = 2
   Pb_mode = 2
   Pb_run_stop = 2

   cls
   Lcdat 3 , 1 , " Checking COMMS"
   Lcdat 5 , 1 , "  To The Drive "
   wait 1

   C = 0
   Retry_comms:
      function_number = monitor_data
      call build_read_string()

      Comstring = ""
      B = 0
      Do
         Call Receive_data()

         If B > 25 Then
            Waitms 100
            C = C + 1
            If C = 6 Then
               Cls
               Wait 1
               Lcdat 2 , 1 , "  Setup cannot"
               Lcdat 3 , 1 , "continue because"
               Lcdat 4 , 1 , "  COMMS to the"
               Lcdat 5 , 1 , "   Drive have"
               Lcdat 6 , 1 , "   been lost!"
            End If

            Call Clear_buff()
            Goto Retry_comms

         End If

      Loop Until A = Chr(10)

      cls
      Lcdat 2 , 1 , "  Connected To "
      Lcdat 3 , 5 , "     Drive     "
      wait 1
      Lcdat 5 , 1 , " Checking Model"
      Lcdat 6 , 1 , "    Of Drive   "
      wait 1



   Get_model:
      Comstring = ""
      Function_number = "252F"      'drive_check
      Call Build_read_string()
      B = 0
      Do
         Call Receive_data()
         If B > 10000 Then Goto Get_model
      Loop Until A = Chr(10)
      If Len(comstring) = 15 Then
         D = Mid(comstring , 8 , 4)      '
         'LCDat 6,1, D
         if D = "0200" then
            drive = 2
         elseif D = "0500" then
            drive = 3
         elseif D = "0300" or D = "0340"  or D = "0350"then
            drive = 5
         else
            Goto Get_model
         end if
      Else
         Goto Get_model
      End If
      Call Clear_buff()
      Cls

      Pb_dsp_fun = 2
      Pb_up_arrow = 2
      Pb_run_stop = 2
      Pb_down_arrow = 2
      Pb_reset = 2
      Pb_enter = 2

      Writeeeprom Drive , 38
      waitms 10

      cls

      If drive = 3 then
         lcdat 2 , 1 , " Connecting To  "
         lcdat 4 , 1 , "      TECO      "
         lcdat 6 , 5 , "     E510S     "
      elseif drive = 2 then
         lcdat 2 , 1 , " Connecting To  "
         lcdat 4 , 1 , "      TECO      "
         lcdat 6 , 1 , "      E510      "
      elseif drive = 5 then
         lcdat 2 , 1 , " Connecting To  "
         lcdat 4 , 1 , "      TECO      "
         lcdat 6 , 1 , "      A510s     "
      else
         lcdat 4 , 2 , "    Unknown     "
         lcdat 6 , 2 , "     Drive      "
      end if
      wait 1

      ' populate arrays                 ' the register numbers are the same for E510 and E510S drives
      If Drive = 2 or drive = 3 then
              for z = 1 to 62 '3 new parameters added, so total number comes to 60 11.11.20 John
                         parameter_ar(z) = parameter_ar_E510(z)     ' drive register array selected here
              next
      elseIf Drive = 5 then
              for z = 1 to 62 '3 new parameters added, so total number comes to 60 11.11.20 John
                         parameter_ar(z) = parameter_ar_A510s(z)     ' drive register array selected here
              next
      Else
              for z = 1 to 62 '3 new parameters added, so total number comes to 60 11.11.20 John
                         parameter_ar(z) = parameter_ar_A510s(z)     ' drive register array selected here
              next
      endif

      cls


   label1:

      Pb_dsp_fun = 2
      Pb_up_arrow = 2
      Pb_run_stop = 2
      Pb_down_arrow = 2
      Pb_reset = 2
      Pb_enter = 2
      Configured = 1

      'Reset the drive
      Function_number = drive_reset
      If drive = 3 then
         Function_value = "0003"
      elseif drive = 5 then
         Function_value = "0004"
      else
         Function_value = "04E2"
      End If
      Call Build_write_string()
      wait 1

      'Set to VF mode
      Function_number = op_mode
      Function_value = "0000"
      Call Build_write_string()
      Wait 1

      if drive <> 2 then          ' set the VF pattern to Custom which is default and as default matches the general pattern 0
         function_value = "000F"
      else
         function_value = "0000"
      end if
      function_number = vf_pattern
      Call Build_write_string()

      'Wait 1

      Cls
      Dim yes_no as byte
      yes_no = 0
    '  tempByte = 0
          '  WithFM = ChooseFM_PT(tempByte)
      'Call MyAssert(WithFM)
      lcdat 1 , 1 , "  IS PUMP WITH  "
      lcdat 2 , 1 , "  A FLOWMETER  "


      lcdat 3 , 1 , "    Yes/No    "

      Lcdat 7 , 1 , "  ^/v Change "
      Lcdat 8 , 1 , "  Press Enter"

      do
         lcdat 5 , 1 , "                "
         If yes_no = 0 Then
            Lcdat 5 , 5 , "      NO      "
         Else
            Lcdat 5 , 5 , "      YES      "
         End If




         If pb_up_arrow = 1 or pb_down_arrow = 1 then
            pb_up_arrow = 2
            pb_down_arrow = 2
            if yes_no = 1 then
               yes_no = 0
            else
               yes_no = 1
            end if
         end if


         if pb_enter = 1 then
            pb_enter = 2
            Pb_dsp_fun = 2
            Pb_up_arrow = 2
            Pb_run_stop = 2
            Pb_down_arrow = 2
            Pb_reset = 2
            'writeeeprom WithFM , 56
            waitms 100
            exit do
         end if
         waitms 100

      loop
      cls
     WithFM = yes_no

        If WithFM = 1 Then
                Title_str = "   Flow Rate    "
                Func_des1 = "     Range      "
                H = "0"
                F_num = 12
                Call Set_value
        Endif

     yes_no = 0
      wait 1
      Cls
   '   tempByte = 1
          '  WithPT = ChooseFM_PT(tempByte)
      'Call MyAssert(WithPT)
      lcdat 1 , 1 , "  IS PUMP WITH  "
      lcdat 2 , 1 , "  A TRANSDUCER "

      lcdat 3 , 1 , "    Yes/No    "

      Lcdat 7 , 1 , "  ^/v Change "
      Lcdat 8 , 1 , "  Press Enter"

      do
         lcdat 5 , 1 , "                "
         If yes_no = 0 Then
            Lcdat 5 , 5 , "      NO      "
         Else
            Lcdat 5 , 5 , "      YES      "
         End If




         If pb_up_arrow = 1 or pb_down_arrow = 1 then
            pb_up_arrow = 2
            pb_down_arrow = 2
            if yes_no = 1 then
               yes_no = 0
            else
               yes_no = 1
            end if
         end if


         if pb_enter = 1 then
            pb_enter = 2
            Pb_dsp_fun = 2
            Pb_up_arrow = 2
            Pb_run_stop = 2
            Pb_down_arrow = 2
            Pb_reset = 2
            'writeeeprom WithFM , 56
            waitms 100
            exit do
         end if
         waitms 100

      loop
      cls
     WithPT = yes_no

        If WithPT = 1 Then
                Title_str = "   Pressure     "
                Func_des1 = "     Range      "
                H = "0"
                F_num = 11
                Call Set_value
        Endif

     yes_no = 0

      If WithFM = 0 and WithPT = 0   Then
        cls
         Lcdat 2 , 1 , "  Setup cannot"
         Lcdat 3 , 1 , "continue because"
         Lcdat 4 , 1 , "  No Sensor is"
         Lcdat 5 , 1 , "   Connected!"
        Do
            Waitms 100
        Loop
      Else
        cls
         If WithFM = 1 Then Lcdat 1, 1 , "Flowmeter Linked"
         If WithPT = 1 Then Lcdat 2, 1 , "P Sensor  Linked"
         Lcdat 4 , 1 , " Press Enter To"
         Lcdat 5 , 1 , "   Continue     "
         Lcdat 6 , 1 , "Or Restart Again"
        Do
         if pb_enter = 1 then
            pb_enter = 2
            waitms 100
            exit do
         end if
        Loop
      Endif

      If WithFM = 1 and WithPT = 0 Then
         writeeeprom WithFM, 63  'the controller is only with Flowmeter
      Elseif  WithFM = 0 and WithPT = 1 Then
         WithFM = 2
         writeeeprom WithFM, 63
         WithFM = 0
       Elseif  WithFM = 1 and WithPT = 1 Then
         WithFM = 3
         writeeeprom WithFM, 63
         WithFM = 1
       Endif
       Waitms 100

      cls

      If WithPT = 0 Then Goto  Get_volt_range

      lcdat 1 , 1 , "Select Preferred"
      lcdat 2 , 1 , " User Units For "
      lcdat 3 , 1 , "    Pressure    "

      Lcdat 7 , 1 , "  ^/v Change "
      Lcdat 8 , 1 , "  Press Enter"

      do
         lcdat 5 , 1 , "                "
         If user_units = 0 Then
            Lcdat 5 , 5 , "      bar      "
         ElseIf user_units = 1 then
            Lcdat 5 , 5 , "      psi      "
         End If

         waitms 100



         If pb_up_arrow = 1 or pb_down_arrow = 1 then
            pb_up_arrow = 2
            pb_down_arrow = 2
            if user_units = 1 then
               user_units = 0
            else
               user_units = 1
            end if
         end if

         if pb_enter = 1 then
            pb_enter = 2
            Pb_dsp_fun = 2
            Pb_up_arrow = 2
            Pb_run_stop = 2
            Pb_down_arrow = 2
            Pb_reset = 2
            writeeeprom user_units , 56
            waitms 100
            exit do
         end if
      loop

      if user_units = 0 then
         units_str = "bar"
         If Psensor_type = 0 then
                sensor_size = 1000
         Elseif Psensor_type = 1 then
                sensor_size = 1400
              Else
                sensor_size = 2000
         EndIf
      else
         units_str = "psi"
          If Psensor_type = 0 then
                sensor_size = 1450
         Elseif Psensor_type = 1 then
                sensor_size = 2030
         Else
                sensor_size = 2900
         EndIf
      end if

      cls

      configured = 1
      Configured_as = Configured
      Writeeeprom Configured_as , 23
      waitms 100
'          If drive = 3 then
'
'         If configured_as = 2 or configured_as = 4 then
'            If single_phase = 1 then
'               function_value = "009F"    ' 15.9
'            else
'               function_value = "0114"    ' 27.6
'            end if
'            function_number = mid_volt
'            Call Build_write_string()
'
'            if single_phase = 1 then
'               function_value = "0051"    ' 8.1
'            else
'               function_value = "008C"    ' 14.0
'            end if
'            function_number = min_volt
'            Call Build_write_string()
'         End If
'      end if


      If Configured = 1 then suggestions = 8



   'Get the voltage value, so to provide the range of voltage the user can enter
   Get_volt_range:

      Pb_reset = 2
      Pb_enter = 2
      Pb_up_arrow = 2
      Pb_down_arrow = 2
      Pb_mode = 2
      Pb_run_stop = 2
      Call Clear_buff()

      Comstring = ""
      Function_number = motor_V
      'Function_number = "0204"
      Call Build_read_string()
      B = 0

      Do
         Call Receive_data()
         If B >= 25 Then
            Goto Get_volt_range
         Endif
      Loop Until A = Chr(10)
      Cls

     ' Lcdat 5,1, Comstring
     ' Call MyAssert(180)

      If Len(comstring) = 15 Then
         D = Mid(comstring , 8 , 4)
         C = Hexval(d)
         Volt_out = C
         If Volt_out = 0 Then Goto Get_volt_range
         Else
         Goto Get_volt_range
      End If


      Call Clear_buff()
      Cls
    ' Enter the voltage
      Title_str = "  Motor Rated "
      Func_des1 = "    Voltage"
      If Volt_out < 3230 Then
         H = "2300"
         Func_des2 = "  170V - 264V"
         single_phase = 1
      Else
         H = "4000"
         Func_des2 = "  323V - 528V"
      End If
      F_num = 100
      Call Set_value()

      suggestions = 7

      If skip = 1 then
         skip = 0
         goto label1
      end if


   label2:

      cls

   ' Enter the current
      Title_str = "  Motor Rated "
      Func_des2 = "  Current (A) "

'      Select Case Suggestions
      H = "54"
'         Case 4 : H = "53"
'         Case 5 : H = "53"
'         Case 6 : H = "115"
'         Case 7 : H = "80"
'         Case 8 : H = "16" 'H = "44"
'         Case 11 : H = "53"
'         Case 10 : H = "43"
'         Case 12 : H = "53"
'         Case else : H = "18"
'      End Select

      F_num = 101
      Call Set_value()

      If Skip = 1 then
         skip = 0
         goto label1
      End If

   label3:

  ' If Configured = 4 Then
      Title_str = "  Motor Rated"
      Func_des2 = "   Power(kW) "
'      Select Case Suggestions
      H = "220"
'         Case 4 : H = "220"
'         Case 5 : H = "220"
'         Case 6 : H = "550"
'         Case 7 : H = "400"
'         Case 8 : H = "50" '"220"
'         Case 11 : H = "220"
'         Case 10 : H =  "220"
'         Case 12 : H = "220"
'         Case else : H = "75"
'      End Select
      F_num = 102
      Call Set_value()
  ' End If

      If Skip = 1 then
         skip = 0
         goto label2
      End If

   label4:

      Title_str = "  Motor Rated "
      Func_des2 = "  Speed (RPM) "
  '    Select Case Suggestions
  '       Case 3 : H = "935"
  '       Case 4 : H = "965"
  '       Case 5 : H = "965"
  '       Case 6 : H = "970"
  '       Case 7 : H = "1455"
      H = "960"
  '       Case 11 : H = "965"
  '       Case 10 : H = "1445"
  '       Case 12 : H = "965"
  '       Case else : H = "935"
  '    End Select
      F_num = 103
      Call Set_value()
      If Skip = 1 then
         skip = 0
         goto label3
      End If

   label5:

   'If Configured = 4 Then
      Title_str = "  Motor Rated "
      Func_des2 = " Frequency (Hz)"
      Func_des1 = "            "
      H = "50"
      F_num = 104
      Call Set_value()

      If Skip = 1 then
         skip = 0
         goto label4
      End If

   label6:

      Cls

      Title_str = " Max. Operating"
      Func_des2 = "   Speed (RPM)"

'      Select Case Suggestions
      H = "1200"
'         Case 4 : H = "1400"
'         Case 5 : H = "1300"
'         Case 6 : H = "1000"
'         Case 7 : H = "1455"
'         Case 8 : H = "3300" '"2900"
'         Case 11 : H = "1000"
'         Case 10 : H = "1500"
'         Case 12 : H = "1000"
'         Case else : H = "1400"
'      End Select
      F_num = 8
      Call Set_value()

      If Skip = 1 then
         skip = 0
         goto label5
      End If

   label7:

      cls

      Title_str = " Min. Operating"
      Func_des2 = "   Speed (RPM)"

'      Select Case Suggestions
      H = "200"
'         Case 4 : H = "250"
'         Case 5 : H = "250"
'         Case 6 : H = "250"
'         Case 7 : H = "300"
'         Case 8 : H = "1500" '"1000"
'         Case 11 : H = "250"
'         Case 10 : H = "250"
'         Case 12 : H = "250"
'         Case else : H = "200"
'      End Select
      F_num = 7
      Call Set_value()

      If Skip = 1 then
         skip = 0
         goto label6
      End If

      Cls

      Lcdat 4 , 1 , "  Please Wait.. "
      waitms 10

      If drive = 2 then
   'set AIN bias to positive
         Function_number = AIN_pos_neg
         Function_value =  "0000"
         Call Build_write_string()
      end if

   'disable trip prevention
      Function_number = trip_prev
      Function_value = "000F"
      Call Build_write_string()

    'Make the trip acceleration level 200%
      Function_number = Trip_Level_A
      Function_value = "00C8"
      Call Build_write_string()

   'Make the trip decel level 200%
      Function_number = Trip_Level_B
      if drive = 2 then
         Function_value = "00C8"
      else
         if single_phase = 0 then
            function_value = "0334"
         else
            function_value = "019A"
         end if
      end if
      Call Build_write_string()


    'make the trip prevention level in run mode 200%
      Function_number = Trip_Level_R
      Function_value = "00C8"
      Call Build_write_string()

   'enable reset when the run command is on
      If Drive <> 5 Then
              Function_number = reset_mode
              Function_value = "0001"
              Call Build_write_string()
      Endif

      If drive = 2 then
   'Make sure that the float scale is set to 100%
         Function_number = Ain_gain
         Function_value = "0064"
         Call Build_write_string()
      end if

     'set the fan mode to turn on when the drive is running
      Function_number = fan_mode
      Function_value = "0000"
      Call Build_write_string()

   'Set the MFIT1 as the start/stop input
      Function_number = start_stop
      Function_value = "0001"
      Call Build_write_string()

      'Set the MFIT4 as the PID disable input , 11.11.20 John
      Function_number = S4
      Function_value = "0010"
      Call Build_write_string()
      Rly3 = 1

      'set AI 2 of vsd is 4-20mA
      Function_number = "0400"
      Function_value = "0001"
      Call Build_write_string()

      If  WithFM = 1  Then


         'Set PID control enabled, 11.11.20 John
           Function_number = PID_enable
           Function_value = "0001"
           Call Build_write_string()

         'Set PID source as VSD parameters 10-02m , 11.11.20 John
           Function_number = PID_source
           Function_value = "0004"
           Call Build_write_string()

         'Set Pulse Input  as the PID control feedback
            Function_number = "0A01"
            Function_value = "0003"
            Call Build_write_string()

          'Set PID setpoint as 50% temporarily, to be part of configuration 11.11.20 John
            Function_number = PID_setpoint
            Function_value = "1388"
            Call Build_write_string()


     Elseif    WithPT = 1 Then

            'Disable the PID control
             Function_number = PID_enable
             Function_value = "0000"
             Call Build_write_string()

     Endif

   'set carrier frequency to 10 kHz
      Function_number = carrier_freq
      Function_value = "0008"
      Call Build_write_string()

   'set S curves to 1 seconds
      Function_number = S_curve_A1
      Function_value = "000A"
      Call Build_write_string()

      Function_number = S_curve_A2
      Function_value = "000A"
      Call Build_write_string()

      Function_number = S_curve_D3
      Function_value = "000A"
      Call Build_write_string()

      Function_number = S_curve_D4
      Function_value = "000A"

      Call Build_write_string()


   'Set the frequency source as the 0-10V signal
      Function_number = freq_source
      If Drive = 5 Then
         Function_value = "0001"
      Else
         Function_value = "0002"
      Endif
      Call Build_write_string()


      if drive = 3 then
         'first set 03-05 (S6) to something other than reset
         function_value = "0012"
         function_number = "0305"
         call build_write_string()
      end if

   'set MFIT 3 as the external reset input
      Function_number = S3
      Function_value = "0011"
      Call Build_write_string()


   'DC Injection braking
   '  apply 5% of DC braking
      Function_number = brake_level
      if drive = 2 then
         Function_value = "0032"
      else
         Function_value = "0005"
      end if
      Call Build_write_string()

   '  start at 2.5 Hz
      Function_number = brake_start

      Function_value = "0019"

      Call Build_write_string()

   '  for .5 second
      Function_number = brake_time
      If drive = 2 then
         Function_value = "0005"
      else
         Function_value = "0032"
      end if
      Call Build_write_string()
      Cls

      If drive <> 2 then

         Function_number = stop_button_off                         ' unavailable in E510
         Function_value = "0000"
         Call Build_write_string()
      end if

      Cls

   'Set the Direct Running After Power Up
      Function_number = direct_run
      Function_value = "0000"
      Call Build_write_string()

   'Set the Accumulated Operation Time Mode
      Function_number = operation_hours
      Function_value = "0001"
      Call Build_write_string()

   'Accel 2 set to 10s and Decel 2 set to 30 second
      Function_number = accel2
      Function_value = "0064"
      Call Build_write_string()
      Function_number = decel2
      Function_value = "012C"
      Call Build_write_string()

   'enable soft PWM
   If drive <> 2 then        'if the vsd is not E510
      Function_value = "0001"
      Function_number = carrier_mode
      Call Build_write_string()
   end if

      Cls

      Lcdat 4 , 1 , "  Please Wait.. "
      waitms 20

   label8:

   'set accel and decel times to 10s
      Function_number = accel1
      Function_value = "0064"
      Call Build_write_string()

      Function_number = decel1
      Function_value = "0064"
      Call Build_write_string()

   'and set MFIT S1 to forward
      Function_number = S1
      Function_value = "0000"
      Call Build_write_string()

   ' set S2 for 2nd accel
      Function_number = S2
      Function_value = "000A"
      Call Build_write_string()

      Dim Dor As Byte
      dor = 0

      cls
      Do
         Lcdat 1 , 1 , "Rotor Direction"
         Lcdat 2 , 1 , "Press UP Arrow"
         Lcdat 3 , 1 , "To Bump Rotor"
         Lcdat 6 , 1 , "Push Reset To"
         Lcdat 7 , 1 , "Reverse If Wrong"
         Lcdat 8 , 1 , "Else Press Enter"

         If Dor = 0 Then
            Lcdat 5 , 1 , "Direction = FWD"
         Else
            Lcdat 5 , 1 , "Direction = REV"
         End If

         If Pb_reset = 1 Then
            Pb_reset = 2
            If Dor = 0 Then
               Function_number = S1
               Function_value = "0001"
               Call Build_write_string()
               Dor = 1
            Else
               Dor = 0

               Function_number = S1
               Function_value = "0000"
               Call Build_write_string()
            End If
         End If

         #IF TEST_CONF
            Exit Do
         #ELSE
         If Pb_enter = 1 Then
            Pb_enter = 2
            Exit Do
         End If
         #ENDIF

         If Pb_dsp_fun = 1 then
            Pb_dsp_fun = 2
            goto label7
         End If

         If Pb_up_arrow = 1 Then
            Pb_up_arrow = 2
            Disable Interrupts
            Rly1 = 1                                    'run the vsd to test the direction of the pump
            Anaout = 500
            Waitms 2000
            Rly1 = 0
            Anaout = 0
            Enable Interrupts
         End If
      Loop

      Cls

      Wait 1

   'set current 30% higher than nameplate
      Over_current = Motor_current * 110      'johnson original 130
      Over_current = Over_current / 100
      Motor_current = Over_current
      D = Hex(motor_current)
      H = Format(d , "0000")
      Function_number = motor_A
      Function_value = H
      Call Build_write_string()
      Writeeeprom Motor_current , 24

   'set accel to 10 seconds = default 5 seconds for impellor
      If Configured = 2 or configured = 4 then
         Accel_time = 50
      else
         Accel_time = 50
      end if

      Function_number = accel1
      Function_value = hex(accel_time)
      Call Build_write_string()
      Writeeeprom Accel_time , 15

   'set decel to 10 seconds = default  5 seconds for impellor
      If Configured = 2 or configured = 4 then
         Decel_time = 50
      else
         Decel_time = 150
      end if
      Function_number = decel1
      Function_value = hex(decel_time)
      Call Build_write_string()
      Writeeeprom Decel_time , 17

   'set the drive overload protection setting to inverter duty motor setting
      Function_number = motor_duty
      If drive = 2 then
         Function_value = "0001"
      else
         Function_value = "0007"
      end if
      Call Build_write_string()

'check that remote run signal is healthy
      If Ic13.4 = 0 then
         cls
         Lcdat 2 , 5 , " Switch Remote"
         Lcdat 3 , 5 , "   Signal to "
         Lcdat 5 , 1 , "       ON  "
         Lcdat 7 , 5 , "  To Continue"
         do
            wait 2
         loop until Ic13.4 = 1
      end if

' check that external interlock is healthy
      If Ic13.1 = 0 then
         cls
         Lcdat 1 , 1 , " Ext. Interlock"
         Lcdat 2 , 5 , "    Tripped"
         Lcdat 4 , 5 , "Check Interlock"
         Lcdat 6 , 1 , "  Or Wiring To"
         Lcdat 7 , 5 , "    Input A"
         do
            wait 2
         loop until Ic13.1 = 1
      end if

      Dim freq as Integer
      freq = 5000
  '#IF TEST_CONF
  '      setpoint_1 = 500
  '#ELSE


  Dim flowRate_1 as Long

   resume_point2:

      Cls
      Lcdat 1 , 5 , "System Needs To"
      Lcdat 2 , 5 , " Collect  Data "
      Lcdat 3 , 5 , " At Max Speed  "
      Lcdat 4 , 1 , "               "
      Lcdat 6 , 1 , " Close All Taps"
      Lcdat 8 , 5 , "  Press ENTER  "

      Do
         If Pb_enter = 1 Then
            Pb_enter = 2
            Exit Do
         End If

         If pb_dsp_fun = 1 then
            pb_dsp_fun = 2
            goto label8
         end if
         Waitms 100
      Loop

      Accel_time = 80
      Function_number = accel1
      Function_value = hex(accel_time)
      Call Build_write_string()
      Writeeeprom Accel_time , 15

      Cls
      Lcdat 2 , 5 , " The Pump Will"
      Lcdat 3 , 1 , "     Run At    "
      Lcdat 4 , 1 , " Full Speed For"
      Lcdat 5 , 1 , "   30 Seconds  "
      Lcdat 7 , 5 , "  Press ENTER  "

      pb_mode = 2
      pb_run_stop = 2

      Do
         If Pb_enter = 1 Then
            Pb_enter = 2
            Exit Do
         End If

         If Pb_dsp_fun = 1 then
            Pb_dsp_fun = 2
            goto label8
         End If

         If pb_mode = 1 and pb_run_stop = 1 then
            pb_mode = 2
            pb_run_stop = 2
            goto skip_measure
         end if

         Waitms 100
      Loop

      cls
      Lcdat 2 , 5 , "  Running At  "
      Lcdat 3 , 1 , "   Max Speed  "
      'Lcdat 4 , 1 , "  Pressure And"
      'Lcdat 5 , 1 , "  Current Draw"
      Lcdat 7 , 5 , "  Please Wait  "
'      Call MyAssert(WithFM)
'      Call MyAssert(WithPT)

      operation_mode = 10
      setup_setpoint = 1
      rly1 = 1                      'run pump at the maximum speed, to test the current
      rly3 = 1

      initialised = 1
      Dim waittime as byte
      waittime = 0
      Dim press_measure as word
      Dim press_measureMax as word
      press_measure = 0

   Get_current:
      Wait 25
   Get_current_1:
      Call Clear_buff()
      Function_number = output_A
      Call Build_read_string()
      Comstring = ""
      B = 0
      Do
         Call Receive_data()
         If B > 25 Then Goto Get_current_1
'Lcdat 8,1, Inst_dac;" ";setup_setpoint

      Loop Until A = Chr(10)
      If Len(comstring) = 15 Then
         D = Mid(comstring , 8 , 4)
         If Hexval(d) < 900 Then
            test_current = val(d)
         Else
            Goto Get_current_1
         End If
      End If

      If test_current <= 0 Then Goto Get_current_1

      If WithFM = 1 Then flowRate = FnGetFlowRate()

      If WithPT = 1 Then
            do
               waittime = waittime + 1
               If press_measure < actual_pressure then press_measure = actual_pressure
               if waittime >= 30 then exit do
            loop
      Endif

      press_measureMax  =   press_measure

      rly1 = 0
      setup_setpoint = 0
      initialised = 0

      wait 15

      Dim More_current As Word
      Dim Current_lim As Word

      More_current = Set_current * 120                         '     margin 20% over nameplate
      More_current = More_current / 100
      Current_lim = Set_current * 8
      Current_lim = Current_lim / 10

      If test_current <= Current_lim Then Goto Current_set      '    less than 80% nameplate draw requires no action
      If test_current > Current_lim And test_current <= More_current Then
         Cls
      'set amps 20% higher than reading
         test_current = test_current * 12
         test_current = test_current / 10
         Motor_current = test_current / 10
         Writeeeprom Motor_current , 24
         H = Hex(test_current)
         If Len(h) = 8 Then
            H = Mid(h , 5 , 4)
         End If

         Function_number = motor_A
         function_value = H
         call build_write_string()

         Goto Current_set
      Else
         If test_current > More_current Then
            Cls
         'set maximum current 144% of nameplate
            More_current = Set_current * 144
            More_current = More_current / 100
            H = Hex(more_current)
            Motor_current = More_current / 10
            Writeeeprom Motor_current , 24
            If Len(h) = 8 Then
               H = Mid(h , 5 , 4)
            End If

            Function_number = motor_A
            function_value = H
            call build_write_string()

            Freq = Freq - 100
            H = Hex(freq)
            If Len(h) = 8 Then
               H = Mid(h , 5 , 4)
            End If

            Function_number = upper_Hz
            function_value = H
            call build_write_string()

            If Freq <= 4500 Then
               do
                  if pb_enter = 1 then
                     pb_enter = 2
                     exit do
                  end if
                  wait 1
               loop

               Cls
               Do
                  Lcdat 2 , 5 , "     Motor     "
                  Lcdat 3 , 1 , "   Overloaded  "
                  Lcdat 5 , 1 , " Please Contact"
                  Lcdat 6 , 5 , "  JOHN BROOKS  "
                  Waitms 100
               Loop
            Else
               Goto Resume_point2
            End If
         End If
      End If

   Current_set:

      If WithPT = 1 Then
         pressure_display_str = str(press_measure)
         if user_units = 0 then
            pressure_display_str = Format(pressure_display_str , "#0.00")
         else
            pressure_display_str = Format(pressure_display_str , "#0.0")
         end if
      Endif

      cls

      Dim temp_spring as String * 10
      If WithFM = 1 Then   temp_spring =   str(flowRate)

      lcdat 1 , 5 , "   Result At  "
      lcdat 2 , 5 , " Maximum Speed "

      If WithPT = 1 Then
         lcdat 4 , 1,"   "; pressure_display_str; units_str
      Endif

      If WithFM = 1 Then
         lcdat 5, 1, "    "; Format( temp_spring, "00.0"); "CMH"
      Endif

      Lcdat 7 , 5 , "  Press Enter"
      Lcdat 8 , 5 , "  To Continue"

      'pb_enter = 2

      do
         waitms 100
         if pb_enter = 1 then
            pb_enter = 2
            exit do
         end if
      loop

      cls

      pb_enter = 2
      pb_dsp_fun = 2

       ' check here for pressure at minimum speed
      Cls
      Lcdat 1 , 5 , "System Needs To"
      Lcdat 2 , 5 , " Collect  Data "
      Lcdat 3 , 5 , " At Min Speed  "
      Lcdat 4 , 1 , "               "
      Lcdat 6 , 1 , " Close All Taps"
      Lcdat 8 , 5 , "  Press ENTER  "

      Do
         If Pb_enter = 1 Then
            Pb_enter = 2
            Exit Do
         End If

         If pb_dsp_fun = 1 then
            pb_dsp_fun = 2
            goto resume_point2
         end if
         Waitms 100
      Loop

      Cls
      Lcdat 2 , 5 , " The Pump Will"
      Lcdat 3 , 1 , "     Run At    "
      Lcdat 4 , 5 , " Minimum Speed"
      Lcdat 5 , 1 , " For 30 Seconds "
      Lcdat 7 , 5 , "  Press ENTER  "

      Do
         If Pb_enter = 1 Then
            Pb_enter = 2
            Exit Do
         End If

         If Pb_dsp_fun = 1 then
            Pb_dsp_fun = 2
            goto resume_point2
         End If

         Waitms 100
      Loop

      cls
      Lcdat 2 , 5 , "  Running At  "
      Lcdat 3 , 1 , "   Min Speed  "
      'Lcdat 4 , 1 , "  Pressure And"
      'Lcdat 5 , 1 , "  Current Draw"
      Lcdat 7 , 5 , "  Please Wait  "

      setup_setpoint = 2
      operation_mode = 10
      initialised = 1
      waittime = 0
      press_measure = 0
      wait 25

      If WithFM =1 Then flowRate_1 = FnGetFlowRate()
'Dim flowRate as  long
'     'read analogue 2 input which indicates flowrate
'Get_FlowRate:
'      Call Clear_buff()
'      Function_number = Analog2Value
'      Call Build_read_string()
'      comstring = ""
'      B = 0
'      Do
'         Call Receive_data()
'         If B > 25 Then Goto Get_FlowRate
'      Loop Until A = Chr(10)
'      If Len(comstring) = 15 Then
'         D = Mid(comstring , 8 , 4)
'         lcdat 6, 1,  D
'         flowRate = Hexval(d)
'         'If Hexval(d) < 900 Then
'         '   test_current = val(d)
'         'Else
'         '   Goto Get_current_1
'         'End If
'      End If

       'lcdat 6, 1,  comstring

      If WithPT = 1 Then
            do
               'Function_number = Analog2Value
               'Call Build_read_string() 'comparing to reading current with maximum speed, read it before the loop
               waittime = waittime + 1
               If press_measure < actual_pressure then press_measure = actual_pressure
               if waittime >= 30 then exit do
            loop
      Endif

       If WithFM = 1 Then   temp_spring =   str(flowRate_1)

      setup_setpoint = 0
      rly1 = 0   '

      wait 15

      If WithPT = 1 Then

            pressure_display_str = str(press_measure)

            if user_units = 0 then
               pressure_display_str = Format(pressure_display_str , "#0.00")
            else
               pressure_display_str = Format(pressure_display_str , "#0.0")
            end if

      Endif

      cls

      lcdat 1 , 5 , "   Result At  "
      lcdat 2 , 5 , " Minimum Speed "


      If WithPT = 1 Then
         lcdat 4 , 1,"   "; pressure_display_str; units_str
      Endif

      If WithFM = 1 Then
         lcdat 5, 1, "    "; Format( temp_spring, "00.0"); "CMH"
      Endif


      Lcdat 7 , 5 , "  Press Enter"
      Lcdat 8 , 5 , "  To Continue"
      pb_enter = 2

      do
         waitms 100
         if pb_enter = 1 then
            pb_enter = 2
            exit do
         end if
      loop

   skip_measure:

      cls

'      if user_units = 0 then
'         press_measure = 588
'      else
'         press_measure = 850
'      end if
'      press_measure = press_measure / 10
'      press_measure = press_measure * 10
      If WithFM = 1 and WithPT = 1 Then
'        if user_units = 0 then
'           setpoint_1 = press_measureMax  * 9
'        else
'           setpoint_1 = press_measureMax   * 9
'        end if
        setpoint_1 = press_measureMax   * 9
        setpoint_1 = setpoint_1 /10
      Else
           setpoint_1 = press_measureMax + press_measure
           setpoint_1 =   setpoint_1/2
      Endif
'#ENDIF
      cls

      If WithPT = 1 Then
         'let user input the setpoint of pressure
         h = str(setpoint_1)

'         if user_units = 0 then
'            Func_des2 = "     (bar)    "
'         else
'            Func_des2 = "      (psi)    "
'         end if

         If WithFM = 1 Then
            Func_des1 = "     Switch     "
            Func_des2 = "    Pressure    "
            F_num = 10
            Call Set_value()
         Else
            Func_des1 = "     Target     "
            Func_des2 = "    Pressure    "
            F_num = 106
            Call Set_value()
         Endif
      Endif

      Call MyAssert(666)

      If WithFM = 1 Then
         'let user input the setpoint of flow rate
         Dim temp_long as long
         temp_long =  flowRate + flowRate_1
         temp_long = temp_long/2
         h = str(temp_long)
         'h = "500"  'todo : change it according to testing running result

         Func_des1 = "    FlowRate"
         Func_des2 = "   Setpoint"
         'this function number is for Flow Rate Setpoint
         F_num = 106
         Call Set_value()
         Call MyAssert(FlowRateSetpoint)
      Endif

'      'deploy the user's setpoint of flow rate into VSD
'      Function_number =  PID_setpoint
'      temp =  temp * 10
'      'h = hex(temp)
'      'Call Format_value()
'      Function_value =   hex(temp)
'      'Function_value = "0064"


'      LCDAT  1 , 1 , Function_value; " "; Function_number; " "; Str(temp)
'      pb_enter = 2
'      do
'         wait 1
'         if pb_enter = 1 then
'            pb_enter = 2
'            exit do
'         end if
'      loop


'      Call Build_write_string()
'      Writeeeprom FlowRateSetpoint , 57



      If skip = 1 then
         skip = 0
         goto current_set
      End If

'      If WithPT = 1 Then
'         if user_units = 0 then
'            setpoint_2 = setpoint_1 - 100
'         else
'            setpoint_2 = setpoint_1 - 150
'         end if
'         Writeeeprom setpoint_2 , 41
'      Endif

      'FlowRateSetpoint1 = FlowRateSetpoint
      'FlowRateSetpoint2 = FlowRateSetpoint
     'If WithFM = 1 Then
         Writeeeprom FlowRateSetpoint , 59
         Writeeeprom FlowRateSetpoint , 61
     'Endif



      Accel_time = 300
      Function_number = accel1
      Function_value = hex(accel_time)
      Call Build_write_string()
      Writeeeprom Accel_time , 15

      Decel_time = 70
      Function_number = decel1
      Function_value = hex(decel_time)
      Call Build_write_string()
      Writeeeprom Decel_time , 17

      Cls

      Lcdat 3 , 5 , "  Configuring  "
      Lcdat 5 , 5 , "     Drive"
      For B = 1 To 120
         Lcdat 7 , B , "."
         Waitms 100
         B = B + 7
      Next B

      Cls
      Lcdat 3 , 1 , "   Finalizing"
      Lcdat 5 , 5 , "    Install"
      For B = 1 To 120
         Lcdat 7 , B , "."
         Waitms 100
         B = B + 7
      Next B
      Cls

      F_num = 1
      Configured = 20
      Resume_point = 0
      Prime_time = 60
      Prime_Loss = 0
      no_water_time = 10
      max_hours = 20
      standby_time = 20
      CloseLoopDelayTime = 20
      resume_time = 18
      low_suction_current = 0
      RunDryFlowRate = Fsensor_bias + 50 '10% eeprom write
      BurstPipeEnable = 0
      BurstPipeTime = 60
      BurstPipeFlowRate = BurstPipeFRlimit * 9
      BurstPipeFlowRate = BurstPipeFlowRate / 10  '90%
      'Call MyAssert(BurstPipeFlowRate)
      BlockedPipeEnable = 0
      LowPressEnable = 0
      LowPressTime = 60
      disp_op = 0

      if user_units = 0 then
         LS_pressure = 100
         standby_offset = 50
         resume_offset = 50
      else
         LS_pressure = 145
         standby_offset = 70
         resume_offset = 70
      end if

      Writeeeprom low_suction_current , 33
      Writeeeprom Configured , 0
      Writeeeprom Resume_point , 22
      Writeeeprom Prime_time , 36
      Writeeeprom Prime_Loss , 37
      Writeeeprom Setpoint_1 , 39
      'Writeeeprom Setpoint_2 , 41
      Writeeeprom standby_offset , 43
      Writeeeprom resume_offset , 45
      Writeeeprom max_hours , 47
      Writeeeprom CloseLoopDelayTime , 32
      Writeeeprom resume_time , 34
      Writeeeprom LS_pressure , 53
      Writeeeprom no_water_time , 29
      Writeeeprom standby_time, 64
      Writeeeprom RunDryFlowRate, 65 '65&66
      Writeeeprom BurstPipeEnable, 67
      Writeeeprom BurstPipeTime, 68
      Writeeeprom BurstPipeFlowRate, 69
      Writeeeprom BlockedPipeEnable, 71
      Writeeeprom LowPressEnable, 72
      Writeeeprom LowPressTime, 73
      waitms 100

      Lcdat 2 , 1 , "Parameter Setup "
      Lcdat 3 , 1 , "    Complete.   "
      Lcdat 6 , 1 , " Cycle Power To "
      Lcdat 7 , 1 , "Finalise Install"

      Do
         Wait 1
      Loop

      Pb_reset = 2
      Pb_enter = 2
      Pb_up_arrow = 2
      Pb_down_arrow = 2
      Pb_mode = 2
      Pb_run_stop = 2

   Return
End Sub
'=============================================================================
'Read Shiftregister Inputs
Sub Read165()
   Set Rd165
   Shiftin Miso , Sck , Ic13 , 0 , 8 , 100
   Shiftin Miso , Sck , Ic14 , 0 , 8 , 100
   Reset Rd165
'Change to positive logic
   Ic13 = Ic13 Xor 255
   'Ic13 = Ic13 And &HDF            ' exclude input F while used as a count input
   Ic14 = Ic14 Xor 255

'only change the status of the switch bits if they have performed a change of
'state
   If Ic13.6 = 1 And Pb_mode = 0 Then
      Pb_mode = 1
      Beeper = 6
   End If

   If Ic13.6 = 0 And Pb_mode > 1 Then Pb_mode = 0

   If Ic13.7 = 1 And Pb_up_arrow = 0 Then
      Pb_up_arrow = 1
      Beeper = 6
   End If

   If Ic13.7 = 0 And Pb_up_arrow > 1 Then Pb_up_arrow = 0

   If Ic14.0 = 1 And Pb_dsp_fun = 0 Then
      Pb_dsp_fun = 1
      Beeper = 6
   End If

   If Ic14.0 = 0 And Pb_dsp_fun > 1 Then Pb_dsp_fun = 0

   If Ic14.1 = 1 then
      run_stop = run_stop + 1
      if run_stop > 200 then
         run_stop = 200
      end if
   else
      run_stop = 0
   end if

   If run_stop >= 30 And Pb_run_stop = 0 Then     ' 1/2 second debounce to avoid accidental start/stop
      Pb_run_stop = 1
      Beeper = 6
      If rundry = 1 then ' reset run stop button
         pb_run_stop = 2
      End If
   End If

   If run_stop = 0 And Pb_run_stop > 1 Then Pb_run_stop = 0

   If Ic14.2 = 1 And Pb_down_arrow = 0 Then
      Pb_down_arrow = 1
      Beeper = 6
   End If

   If Ic14.2 = 0 And Pb_down_arrow > 1 Then Pb_down_arrow = 0

   If Ic14.3 = 1 And Pb_reset = 0 Then
      Pb_reset = 1
      'reset_attempts = 0
      Beeper = 6
   End If

   If Ic14.3 = 0 And Pb_reset > 1 Then Pb_reset = 0

   If Ic14.4 = 1 And Pb_enter = 0 Then
      Pb_enter = 1
      Beeper = 6
   End If

   If Ic14.4 = 0 And Pb_enter > 1 Then Pb_enter = 0

   Return
End Sub

'=============================================================================
' Interrupt service routine runs in the background
' every 16.32mS with a 16MHz Crystal
Declare Function PressSensorResolve(byval sensor_size  as Word, byval sensor_trim as Word, byval Pressure as Word) as Dword



Tim0_isr:

   Enable Interrupts
   Dim I As Byte
   Tcnt0 = &H00
   If Beeper > 0 Then
'      Reset Buzzer
      Decr Beeper
   Else
'      Set Buzzer
   End If
   'Flash LED0 approx 1sec on 1 sec off
   If Ticks > 0 Then
      Decr Ticks
   Else
      Ticks = Nbrticks
      Toggle Statusled
      For I = 1 To Nbrtimers
      'Decrement the seconds (nominal)  timers
         If Tim(i) > 0 Then
            Decr Tim(i)
         End If
      Next
   End If

   Call Read165()
   Call readadc()

   If Initialised = 1 Then
      If pb_run_stop = 1 then
         pb_run_stop = 2

         If remote_switch = 0 then
         'do not allow the local button to activate a local(soft) stop
         else
            if soft_stop = 0 then
               soft_stop = 1
            else
               soft_stop = 0
               first_cycle = 1  ' prevents pump starting up when pressure is alreay high
            end if
         end if
      End If

      If Ic13.4 = 1 then
         if remote_signal < 10 then
            remote_signal = remote_signal + 1 ' remote switch to be on for a short while before recognised as a true switch
         else
            remote_signal = 10
            remote_switch = 1
         end if
      else
         if remote_signal > 0 then
            remote_signal = remote_signal - 1   ' remote switch to be off for a short while before recognised as a true switch
         else
            remote_signal = 0
            remote_switch = 0
         end if
      end if

      If Running <> 1 then
         If operation_mode = 0 then
            If soft_stop = 0 and remote_switch = 1 then running = 1
         end if

         on_standby = 0
         tim(3) = standby_time    'used for pressure high/blocked pipe protection
         tim(4) = Prime_time        'used for run dry protection
         tim(7) = resume_time    'used for start delay
         tim(16) = BurstPipeTime                      'used for burst pipe protection
         going_to_standby = 0       'reset all variables when not running
         resuming = 0
         tim(5) = 60
         running_minutes = 0
         running_hours = 0
      else
         'If reset_timer <> 1 then tim(11) = no_water_time*60   'this timer will only time when in a faulted state
                                                                'it is used to automatically try to reset loss of
'         If tim(11) <= 0 then                                  'water(pressure) supply faults
'            If reset_attempts < 3 then
'               pb_reset = 1                           'controller will make 3 reset attempts
'               reset_attempts = reset_attempts + 1
'            End If
'         End If

         If Ic13.1 = 0 then
            If ext_interlock < 100 then
               ext_interlock = ext_interlock + 1     ' external interlock signal to be lost for about 1.6s
            else                                    'before recognised as trully lost
               ext_interlock = 100
               running = 0
               rly1 = 0
              ' Call MyAssert(5022)
            End If
         else
            ext_interlock = 0
         End If

'         If real_amps < low_suction_current and low_suction_current > 0 and flowswitch < 30 then
'          timer 8 is now used for start_stop counter  If tim(8) <= 0 then
'               tim(8) = 0
'               If low_suction < 120 then
'                  low_suction = low_suction + 1
'               else
'                  low_suction = 120
'                  running = 0
'                  rly1 = 0
'               End If
'            End If
'
'         else
'            tim(8) = accel_time/10
'            low_suction = 0
'         End If

         If tim(9) <= 0 then tim(9) = 0

         If operation_mode = 0 and rly1 = 1 and LS_pressure > 0 and actual_pressure < LS_pressure and error_on = 0 and WithPT = 1 and LowPressEnable = 1 then

'               If low_pressure < 254 then         ' pressure doesn't build up above the low pressure setting after
'                  low_pressure = low_pressure + 1  ' pump has been running for its accl time setting then it's
'               else                               ' lost pressure probably due to no water at suction
'                  low_pressure = 254
'                  running = 0
'                  rly1 = 0
'                  'assert(5054)
'               End Ifh

            If Rly2 = 1 Then
               If tim(11) <= 0 then
                     tim(11) = 0
                     Error_on = 1
                     Rly1 = 0
                     low_pressure = 255
               Endif
            Else
               tim(11) = LowPressTime
            Endif
         else
            tim(9) = accel_time/10
            tim(11) = LowPressTime 'timer 11 is used for low pressure protection
         End If

         #IF   FM_Rundry

         If RundryPre = 0 then
            tim(4) = Prime_time
         End If

         #ELSE

         If Ic13.2 = 1 then     ' when sensing fluid
            tim(4) = Prime_time  ' keep timer loaded
         End If                 ' else allow timer to operate

         #ENDIF

         #IF FM_BurstPipe
         If BurstPipePre = 0 or Rly1 = 0 then
            tim(16) = BurstPipeTime
         Endif

         If tim(16) <= 0 then
            tim(16) = 0
            Error_on = 1
'            soft_stop = 1
            Rly1 = 0
            BurstPipe = 1
         End If
         #ENDIF

         If Prime_Loss = 0 or IC14.3 = 1 then
            tim(4) = Prime_time  ' don't let timer run if Protection is turned off
         End If

         If rly1 = 0 then       ' or pump is not actually running ( in Filling mode)
            tim(4) = Prime_time
         End If

         If tim(4) <= 0 then
            tim(4) = 0
            Error_on = 1
'            soft_stop = 1
            Rly1 = 0
            RunDry = 1
         End If

         If tim(5) = 0 then
            tim(5) = 60
            running_minutes = running_minutes + 1
         End If                                  'accumulate the continuous running time of the pump

         If running_minutes >= 60 then
            running_minutes = 0
            running_hours = running_hours + 1
            If running_hours => 254 then
               running_hours = 0
            End If
         Else
         End If

         'in the new code below, if error_reg = 98 or 100 or 101, the time counting will be stopped, so this part of code is no more necessary. Also the time do not need to be cleared.
'         if on_standby = 1 then 'keeper timer loaded and clear accumulator variables
'            tim(5) = 60
'            running_minutes = 0
'            running_hours = 0
'         end if

         If operation_mode = 0 then

#IF PT_PressureHigh

           ' If on_standby = 1
           If error_reg = 100 or error_reg = 101 or error_reg = 98 or error_reg = 95 Then
               tim(7) = Resume_Time
               tim(5) = 60 'stop increasing running time
           elseIf anaval_l <= pressure1 And on_standby = 1 Then
                tim(7) = Resume_Time
                tim(5) = 60 'stop increasing running time
           Endif

           'If tim(7) <=0 and on_standby = 0  then
           If tim(7) <= 0 Then
               tim(7) = 0
               Rly1 = 1
               If BlockedPipeEnable = 2 Then on_standby = 0
           End If
#ELSE
           If on_standby = 1 then
               If anaval_l <= resume_pressure1 then
                  tim(7) = resume_time
                  resuming = 0
               else
                  resuming = 1    ' let resume timer count down, the pressure is under threshold
               End If
            else
               tim(7) = resume_time
               resuming = 0
            End If
            If tim(7) <=0 then
               tim(7) = 0
               rly1 = 1
               on_standby = 0
            End If
#ENDIF

'effluent pump controller does not have the function of "standby mode", if the pressrue exceeds up limit, a faulty error will occur
'because current code is based on water flo, the name of flags keeps the same as before, but the meaning will be changed
'going_to_standby = PressureHighErrorPre
'on_standby = PressureHigh Error
'This is modified, the High Pressure Protection (blocked pipe protection) adds one mode "standby", when the pressure drops below setpoint, it will restart the pump
            If anaval_l >= standby_pressure1 or WithPT = 0 or tim(7) > 0 or BlockedPipeEnable = 0 then
               tim(3) = standby_time
               going_to_standby = 0
            elseif WithPT = 1 and BlockedPipeEnable >= 1 then
               going_to_standby = 1    ' let standby pressure count down
            End if

            If first_cycle = 1 then  'zero this timer so the pump doesn't start running if pressure is already present
               tim(7) =  resume_time
               tim(3) =  standby_time
               first_cycle = 0
            end if

            if tim(3) <= 0 then
               tim(3) = 0
               on_standby = 1       ' flag to say controller is in standby
               'running_hours = 0
               'running_minutes = 0
               rly1 = 0
            End If
         End If
      End If

      If running <> 1 or on_standby = 1 then tim(10) = accel_time/10
      If rly1 = 0 then
         rly2 = 0
      elseif  running = 1 and operation_mode = 0 then
         If tim(10) <= 0 or anaval_l <= desired_ain then
            rly2 = 1    'rly 2 switches the drive to use its second set of accel and decel
         Endif
      end if

      'if iC13.3 = 1 then Setpoint = setpoint_2 else Setpoint = setpoint_1 'input D is setpoint select
      If WithFM = 0 and WithPT = 1 Then
         if ic13.3 = 0 and ic13.5 = 0 and FRateSwitch <> 1 then
            FRateSwitch = 1
            Setpoint =  FlowRateSetpoint
         elseif  ic13.3 = 1 and FRateSwitch <> 2 then
            FRateSwitch = 2
            Setpoint =  FlowRateSetpoint1
         elseif ic13.5 = 1 and   FRateSwitch <> 3 then
            FRateSwitch = 3
            Setpoint =  FlowRateSetpoint2
         Endif
      Elseif WithFm = 1 and WithPT = 1 Then
         Setpoint =  setpoint_1
      Else
         Setpoint = 0
      Endif



      standby_pressure = setpoint + standby_offset


      Pressure1= PressSensorResolve(sensor_size, sensor_trim, Setpoint)
      standby_pressure1 = PressSensorResolve(sensor_size, sensor_trim, standby_pressure)


      If Transducer_fault >= 120 Then
         Transducer_fault = 120           'wait about 2 seconds before recognising a transducer lost state as true
         Error_reg = 99
         Running = 0
         Rly1 = 0
        ' Call MyAssert(5226)
      End If

      If Initialised = 1 and Anaval_l = 0 and operation_mode = 0 and fault_bypass = 0 and WithPt = 1 Then
         Transducer_fault = Transducer_fault + 1  ' when adc value = 0 then assume sensor connection is
      else                                       ' lost. its a 10bar sensor and limited to 8 bar max setpoint
         transducer_fault = 0
      End If

      If desired_ain <> Pressure1 then
         desired_ain = Pressure1            ' only update target value when target changes
      End If
'      If WithFM = 1 and WithPT = 1 Then
'         If desired_ain <> Switch_Pressure1 then  desired_ain = Switch_Pressure1            ' only update target value when target changes
'      Else
'          If desired_ain <> Pressure1 then  desired_ain = Pressure1            ' only update target value when target changes
'      Endif

'      switch_pressure2  =  switch_pressure1 + Pressure1
'      switch_pressure2  =  switch_pressure2/2

'      If desired_ain <> switch_pressure2 then
'         desired_ain = switch_pressure2            ' only update target value when target changes
'      End If

      If anaval_l > sensor_trim then       'keep ADC value withing working range of sensor
         anaval_l = sensor_trim
      End If

      Level_display = sensor_trim - Anaval_l
      Level_display = Level_display * sensor_size
      Level_display = Level_display / sensor_trim        'convert ADC value to actual pressure value
      Error_inst = Anaval_l - Desired_ain                'to display on main screen
      Actual_pressure = level_display

      If Soft_stop = 1 or remote_signal <= 0 or Transducer_fault >= 120 Then   'or Run_dry_fault >= 20
         Running = 0
         Rly1 = 0
      End If

      'constant pressure
      If Inst_dac < sensor_trim Then
         If Error_inst >= 150 Then Integral2 = Integral2 + 30_i_gain
         If Error_inst >= 100 And Error_inst < 150 Then Integral2 = Integral2 + 25_i_gain
         If Error_inst >= 50 And Error_inst < 100 Then Integral2 = Integral2 + 20_i_gain
         If Error_inst >= 10 And Error_inst < 50 Then Integral2 = Integral2 + 5_i_gain
         If Error_inst >= 2 And Error_inst < 10 Then Integral2 = Integral2 + I_gain
      End If

   'only accumulate the negative integral if there is still some output from the
   'dac otherwise leave it where it is
      If Inst_dac > 0 Then
         If Error_inst <= -2 And Error_inst > -10 Then Integral2 = Integral2 - I_gain
         If Error_inst <= -10 And Error_inst > -50 Then Integral2 = Integral2 - 10_i_gain
         If Error_inst <= -50 And Error_inst > -100 Then Integral2 = Integral2 - 20_i_gain
         If Error_inst <= -100 And Error_inst > -150 Then Integral2 = Integral2 - 25_i_gain
         If Error_inst <= -150 Then Integral2 = Integral2 - 30_i_gain
      End If

      If Integral2 <= -10150 Then
         Integral2 = -10150
      End If

      If Integral2 >= 10150 Then
         Integral2 = 10150
      End If

      Integral = Integral2 / 10

      If Integral > 1015 Then
         Integral = 1015
      End If

      If Integral < -1015 Then
         Integral = -1015
      End If

      Error_inst = Error_inst * P_gain
      Inst_dac = Error_inst
      Inst_dac = Inst_dac + Integral

      If Inst_dac > 1015 Then
         Inst_dac = 1015
      End If
                               '
      If Inst_dac < 0 Then
         Inst_dac = 0
      End If

      If operation_mode = 1 then        'flow switch mode
         If remote_switch = 1 and soft_stop = 0 then
            If Ic13.0 = 1 then
               If flowswitch < 30 then           'in flow switch mode run stop based on input A of WLD
                  flowswitch = flowswitch + 1
               else
                  inst_dac = 1023
                  rly1 = 1
                  on_standby = 0
                  going_to_standby = 0
                  resuming = 0
                  tim(3) = standby_time
                  tim(7) = resume_time
                  transducer_fault = 0
               End If
            Else
               rly1 = 0
               flowswitch = 0
            End If
         End If
      End If

      If operation_mode = 2 then        'local mode
         If soft_stop = 0 then
            running = 1
            rly1 = 1
         else
            running = 0
            rly1 = 0
            'Call MyAssert(5342)
         end if
      end if

      tim(1) = Accel_time / 10

      If time_exceeded = 0 then

         If soft_stop = 0 and remote_signal >= 30 and error_on = 0  Then
            Running = 1
            Anaout = 0
            if anaval_l < standby_pressure1 then
            'Call MyAssert(111)
               Rly1 = 1
               first_cycle = 1
               on_standby = 0
            end if
         End If
      else
         pb_run_stop = 2
      End If

      If operation_mode = 2 then
         rly2 = 1
         inst_dac = 1023
         'Call MyAssert(5561)
      end if

      If operation_mode = 10 then       'during setup run at full speed for current and pressure check
         if setup_setpoint = 1 then
            Inst_dac = 1023
            rly1 = 1
         End If

         if setup_setpoint = 2 then     'during set up run at min speed for pressure check
            Inst_dac = 0
            rly1 = 1
         End If
      End If

'put the code here to activate relay 4 for 2 seconds if the reset button
   'is pressed
      If Reset_flag = 1 Then
         Reset_flag = 0
         Tim(6) = 2
         Rly4 = 1
      End If

      If Tim(6) = 0 Then
         Rly4 = 0
      End If
   End If



   If rly1 = 1 then
      if cycle_count = 0 then 'set a flag once when pump starts
         cycle_count = 1
      end if
   else
      if cycle_count = 1 then
         cycle_count = 0          'reset the flag once when pump stops and
         start_stops = start_stops + 1 'increment counter
         if start_stops >= 65000 then
            start_stops = 65000
         end if
         hour(1) = start_stops
      end if
   end if

   if tim(8) <= 0 then
      tim(8) = 3600    ' seconds in an hour

      For index_a = 24 To 2 Step -1
         hour(index_a) = hour(index_a - 1) ' shift totals to next hour
      Next

      hour(1) = 0
      start_stops = 0
   end if

   total_cycles = hour(1)

   For index_a = 2 To 24
      total_cycles = total_cycles + hour(index_a) ' totalise counts
   Next

   'when the pressure is under normal range, switched to flowrate control by VSD
   If WithFM = 1 and WithPT = 1 Then
          If Running = 1 and Operation_Mode = 0 Then
             If anaval_l >= pressure1 Then
               'If rly3=1 Then
                  rly3=0
                  on_switch = 0
                  going_to_switch = 0
               'Endif
                tim(15) =  5   '3s
             'Else
                'rly3=1
             else
                going_to_switch = 1
             Endif
          Else
              tim(15) = 5      '5s
          Endif

           If tim(15) <= 0  Then
             rly3=1
             tim(15) = 0
             going_to_switch = 0
             on_switch = 1
           Endif
    Else
         going_to_switch = 0
         on_switch = 0
         tim(15) = 5
         if  WithFM = 1 and  CloseLoopDelay = 0 Then
               rly3 = 0
         Else
               rly3 = 1
         Endif

    Endif

   'the code below is for close loop delay
   If Rly1 = 1 and FormerRly1 = 0 Then
      CloseLoopDelay = 1
   Elseif Rly1 = 0 Then
      CloseLoopDelay = 0
   Endif

   If Running = 1 and  Operation_Mode = 0  Then
       If CloseLoopDelay = 0  Then
          tim(17) = CloseLoopDelayTime
       Elseif Rly1 = 1 Then
          Rly3 = 1 'disable VSD internal PID
          inst_dac = 0 'give  VSD the reference of lowest speed
          tim(10) = accel_time/10       'load the timer for keeping
          'Call MyAssert(Running)
       Endif
    Else
       tim(17) = CloseLoopDelayTime
    Endif

   If  tim(17) <= 0 Then
      CloseLoopDelay = 0
   Endif

   FormerRly1 = Rly1 'record the status of relay 1 in the last clock tick

   'Analogue output is put at the end of clock tick
   Anaouth = High(inst_dac)
   Anaoutl = Low(inst_dac)


   Rly6 = error_on

   If error_on = 1 Then
      Rly5 = 0
   Else
      Rly5 = running
   Endif

Return


 Function PressSensorResolve(byval  sensor_size  as Word,  byval sensor_trim as Word,  byval Pressure as Word) as Dword
      Local PressureResult As Dword
      PressureResult = Pressure * sensor_trim
      PressureResult = PressureResult / sensor_size    'convert to the equal ADC value
      PressureResult = sensor_trim - PressureResult
      'Lcdat 3, 1, str(Pressure); " "; str(PressureResult)
      PressSensorResolve = PressureResult
 End Function

 Sub MyAssert(byval number as word)
        Lcdat    6, 1, str(number)
        do
            wait 1
            if pb_enter = 1 then
               pb_enter = 2
               exit do
            end if
         loop
 End Sub

 'function read flow rate after test running during initialization
   Function FnGetFlowRate() as Long

   local  temp_var as long

   temp_var = 0
Get_FlowRate:
     'read analogue 2 input which indicates flowrate
      Call Clear_buff()
      'Function_number = Analog2Value
      Function_number = "0C27" 'read the PID feedback rather than AI2
      Call Build_read_string()
      comstring = ""

      B = 0
      Do
         Call Receive_data()
         If B > 25 Then Goto Get_FlowRate
      Loop Until A = Chr(10)
      If Len(comstring) = 15 Then
         Call Clear_buff()
         D = Mid(comstring , 8 , 4)
 #IF TEST
         lcdat 6, 1,  D
#ENDIF
         temp_var   =  Hexval(d)   *  Fsensor_size
         temp_var =  temp_var  / 100
         FnGetFlowRate   =     temp_var   +  Fsensor_bias
         'FnGetFlowRate   =     temp_varch
      Else
         FnGetFlowRate = -1
      End If
   End Function

Sub ChangeFlowRateSP

    If WithFM = 1 Then
      if ic13.3 = 0 and ic13.5 = 0 and FRateSwitch <> 1 then
         'lcdat 6,1, "1"
            FRateSwitch = 1
'           Function_Number = PID_setpoint
           'temp =  FlowRateSetpoint * 10
           'Call MyAssert(FlowRateSetpoint)
           'Call MyAssert(Fsensor_size)
            temp_l = FlowRateSetpoint - Fsensor_bias
            temp_l = temp_l* 1000
            temp_l = temp_l / Fsensor_size
            temp = temp_l

            Function_Number = PID_setpoint
            Function_Value = hex(temp)
            call build_write_string()
            Waitms 100
        elseif  ic13.3 = 1 and FRateSwitch <> 2 then
           'lcdat 6,1, "2"
            FRateSwitch = 2
'            Function_Number = PID_setpoint
            'temp =  FlowRateSetpoint1 * 10

'            temp = temp_l / Fsensor_size
'            Function_Value = hex(temp)
'            call build_write_string()
'            Waitms 100


            temp_l = FlowRateSetpoint1  - Fsensor_bias
            temp_l = temp_l* 1000
            temp_l = temp_l / Fsensor_size
            temp = temp_l

            Function_Number = PID_setpoint
            Function_Value = hex(temp)
            call build_write_string()
            Waitms 100
        elseif ic13.5 = 1 and   FRateSwitch <> 3 then
          ' lcdat 6,1, "3"
           FRateSwitch = 3

'           temp = temp_l / Fsensor_size
'           Function_Value = hex(temp)
'           call build_write_string()
'           Waitms 100

          temp_l = FlowRateSetpoint2 - Fsensor_bias
          temp_l = temp_l* 1000
          temp_l = temp_l / Fsensor_size
          temp = temp_l

          Function_Number = PID_setpoint
          Function_Value = hex(temp)
          call build_write_string()
          Waitms 100
        Endif

   Endif
End Sub

'user configuration during initialization to see wether the controller has a flowmeter
Function  ChooseFM_PT(byval a as byte )  as byte

      local yes_no as byte
      yes_no = 0
      pb_enter = 2

      lcdat 1 , 1 , "  IS PUMP WITH  "


      lcdat 3 , 1 , "    Yes/No    "

      Lcdat 7 , 1 , "  ^/v Change "
      Lcdat 8 , 1 , "  Press Enter"

      do
         lcdat 5 , 1 , "                "
         If yes_no = 0 Then
            Lcdat 5 , 5 , "      NO      "
         Else
            Lcdat 5 , 5 , "      YES      "
         End If




         If pb_up_arrow = 1 or pb_down_arrow = 1 then
            pb_up_arrow = 2
            pb_down_arrow = 2
            if yes_no = 1 then
               yes_no = 0
            else
               yes_no = 1
            end if
         end if


         if pb_enter = 1 then
            pb_enter = 2
            Pb_dsp_fun = 2
            Pb_up_arrow = 2
            Pb_run_stop = 2
            Pb_down_arrow = 2
            Pb_reset = 2
            'writeeeprom WithFM , 56
            waitms 100
            exit do
         end if
         waitms 100

      loop
      cls

      ChooseFM_PT = yes_no

End Function

'All pictures are loaded here
Picdata:
   $bgf "waterflo4.bgf"